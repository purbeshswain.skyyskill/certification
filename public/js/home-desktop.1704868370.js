var $jscomp = $jscomp || {};
$jscomp.scope = {};
$jscomp.ASSUME_ES5 = !1;
$jscomp.ASSUME_NO_NATIVE_MAP = !1;
$jscomp.ASSUME_NO_NATIVE_SET = !1;
$jscomp.SIMPLE_FROUND_POLYFILL = !1;
$jscomp.ISOLATE_POLYFILLS = !1;
$jscomp.FORCE_POLYFILL_PROMISE = !1;
$jscomp.FORCE_POLYFILL_PROMISE_WHEN_NO_UNHANDLED_REJECTION = !1;
$jscomp.defineProperty = $jscomp.ASSUME_ES5 || "function" == typeof Object.defineProperties ? Object.defineProperty : function(b, c, f) {
    if (b == Array.prototype || b == Object.prototype) return b;
    b[c] = f.value;
    return b
};
$jscomp.getGlobal = function(b) {
    b = ["object" == typeof globalThis && globalThis, b, "object" == typeof window && window, "object" == typeof self && self, "object" == typeof global && global];
    for (var c = 0; c < b.length; ++c) {
        var f = b[c];
        if (f && f.Math == Math) return f
    }
    throw Error("Cannot find global object");
};
$jscomp.global = $jscomp.getGlobal(this);
$jscomp.IS_SYMBOL_NATIVE = "function" === typeof Symbol && "symbol" === typeof Symbol("x");
$jscomp.TRUST_ES6_POLYFILLS = !$jscomp.ISOLATE_POLYFILLS || $jscomp.IS_SYMBOL_NATIVE;
$jscomp.polyfills = {};
$jscomp.propertyToPolyfillSymbol = {};
$jscomp.POLYFILL_PREFIX = "$jscp$";
var $jscomp$lookupPolyfilledValue = function(b, c) {
    var f = $jscomp.propertyToPolyfillSymbol[c];
    if (null == f) return b[c];
    f = b[f];
    return void 0 !== f ? f : b[c]
};
$jscomp.polyfill = function(b, c, f, g) {
    c && ($jscomp.ISOLATE_POLYFILLS ? $jscomp.polyfillIsolated(b, c, f, g) : $jscomp.polyfillUnisolated(b, c, f, g))
};
$jscomp.polyfillUnisolated = function(b, c, f, g) {
    f = $jscomp.global;
    b = b.split(".");
    for (g = 0; g < b.length - 1; g++) {
        var k = b[g];
        if (!(k in f)) return;
        f = f[k]
    }
    b = b[b.length - 1];
    g = f[b];
    c = c(g);
    c != g && null != c && $jscomp.defineProperty(f, b, {
        configurable: !0,
        writable: !0,
        value: c
    })
};
$jscomp.polyfillIsolated = function(b, c, f, g) {
    var k = b.split(".");
    b = 1 === k.length;
    g = k[0];
    g = !b && g in $jscomp.polyfills ? $jscomp.polyfills : $jscomp.global;
    for (var u = 0; u < k.length - 1; u++) {
        var w = k[u];
        if (!(w in g)) return;
        g = g[w]
    }
    k = k[k.length - 1];
    f = $jscomp.IS_SYMBOL_NATIVE && "es6" === f ? g[k] : null;
    c = c(f);
    null != c && (b ? $jscomp.defineProperty($jscomp.polyfills, k, {
        configurable: !0,
        writable: !0,
        value: c
    }) : c !== f && (void 0 === $jscomp.propertyToPolyfillSymbol[k] && (f = 1E9 * Math.random() >>> 0, $jscomp.propertyToPolyfillSymbol[k] = $jscomp.IS_SYMBOL_NATIVE ?
        $jscomp.global.Symbol(k) : $jscomp.POLYFILL_PREFIX + f + "$" + k), $jscomp.defineProperty(g, $jscomp.propertyToPolyfillSymbol[k], {
        configurable: !0,
        writable: !0,
        value: c
    })))
};
$jscomp.underscoreProtoCanBeSet = function() {
    var b = {
            a: !0
        },
        c = {};
    try {
        return c.__proto__ = b, c.a
    } catch (f) {}
    return !1
};
$jscomp.setPrototypeOf = $jscomp.TRUST_ES6_POLYFILLS && "function" == typeof Object.setPrototypeOf ? Object.setPrototypeOf : $jscomp.underscoreProtoCanBeSet() ? function(b, c) {
    b.__proto__ = c;
    if (b.__proto__ !== c) throw new TypeError(b + " is not extensible");
    return b
} : null;
$jscomp.arrayIteratorImpl = function(b) {
    var c = 0;
    return function() {
        return c < b.length ? {
            done: !1,
            value: b[c++]
        } : {
            done: !0
        }
    }
};
$jscomp.arrayIterator = function(b) {
    return {
        next: $jscomp.arrayIteratorImpl(b)
    }
};
$jscomp.makeIterator = function(b) {
    var c = "undefined" != typeof Symbol && Symbol.iterator && b[Symbol.iterator];
    return c ? c.call(b) : $jscomp.arrayIterator(b)
};
$jscomp.generator = {};
$jscomp.generator.ensureIteratorResultIsObject_ = function(b) {
    if (!(b instanceof Object)) throw new TypeError("Iterator result " + b + " is not an object");
};
$jscomp.generator.Context = function() {
    this.isRunning_ = !1;
    this.yieldAllIterator_ = null;
    this.yieldResult = void 0;
    this.nextAddress = 1;
    this.finallyAddress_ = this.catchAddress_ = 0;
    this.finallyContexts_ = this.abruptCompletion_ = null
};
$jscomp.generator.Context.prototype.start_ = function() {
    if (this.isRunning_) throw new TypeError("Generator is already running");
    this.isRunning_ = !0
};
$jscomp.generator.Context.prototype.stop_ = function() {
    this.isRunning_ = !1
};
$jscomp.generator.Context.prototype.jumpToErrorHandler_ = function() {
    this.nextAddress = this.catchAddress_ || this.finallyAddress_
};
$jscomp.generator.Context.prototype.next_ = function(b) {
    this.yieldResult = b
};
$jscomp.generator.Context.prototype.throw_ = function(b) {
    this.abruptCompletion_ = {
        exception: b,
        isException: !0
    };
    this.jumpToErrorHandler_()
};
$jscomp.generator.Context.prototype.return = function(b) {
    this.abruptCompletion_ = {
        return: b
    };
    this.nextAddress = this.finallyAddress_
};
$jscomp.generator.Context.prototype.jumpThroughFinallyBlocks = function(b) {
    this.abruptCompletion_ = {
        jumpTo: b
    };
    this.nextAddress = this.finallyAddress_
};
$jscomp.generator.Context.prototype.yield = function(b, c) {
    this.nextAddress = c;
    return {
        value: b
    }
};
$jscomp.generator.Context.prototype.yieldAll = function(b, c) {
    b = $jscomp.makeIterator(b);
    var f = b.next();
    $jscomp.generator.ensureIteratorResultIsObject_(f);
    if (f.done) this.yieldResult = f.value, this.nextAddress = c;
    else return this.yieldAllIterator_ = b, this.yield(f.value, c)
};
$jscomp.generator.Context.prototype.jumpTo = function(b) {
    this.nextAddress = b
};
$jscomp.generator.Context.prototype.jumpToEnd = function() {
    this.nextAddress = 0
};
$jscomp.generator.Context.prototype.setCatchFinallyBlocks = function(b, c) {
    this.catchAddress_ = b;
    void 0 != c && (this.finallyAddress_ = c)
};
$jscomp.generator.Context.prototype.setFinallyBlock = function(b) {
    this.catchAddress_ = 0;
    this.finallyAddress_ = b || 0
};
$jscomp.generator.Context.prototype.leaveTryBlock = function(b, c) {
    this.nextAddress = b;
    this.catchAddress_ = c || 0
};
$jscomp.generator.Context.prototype.enterCatchBlock = function(b) {
    this.catchAddress_ = b || 0;
    b = this.abruptCompletion_.exception;
    this.abruptCompletion_ = null;
    return b
};
$jscomp.generator.Context.prototype.enterFinallyBlock = function(b, c, f) {
    f ? this.finallyContexts_[f] = this.abruptCompletion_ : this.finallyContexts_ = [this.abruptCompletion_];
    this.catchAddress_ = b || 0;
    this.finallyAddress_ = c || 0
};
$jscomp.generator.Context.prototype.leaveFinallyBlock = function(b, c) {
    c = this.finallyContexts_.splice(c || 0)[0];
    if (c = this.abruptCompletion_ = this.abruptCompletion_ || c) {
        if (c.isException) return this.jumpToErrorHandler_();
        void 0 != c.jumpTo && this.finallyAddress_ < c.jumpTo ? (this.nextAddress = c.jumpTo, this.abruptCompletion_ = null) : this.nextAddress = this.finallyAddress_
    } else this.nextAddress = b
};
$jscomp.generator.Context.prototype.forIn = function(b) {
    return new $jscomp.generator.Context.PropertyIterator(b)
};
$jscomp.generator.Context.PropertyIterator = function(b) {
    this.object_ = b;
    this.properties_ = [];
    for (var c in b) this.properties_.push(c);
    this.properties_.reverse()
};
$jscomp.generator.Context.PropertyIterator.prototype.getNext = function() {
    for (; 0 < this.properties_.length;) {
        var b = this.properties_.pop();
        if (b in this.object_) return b
    }
    return null
};
$jscomp.generator.Engine_ = function(b) {
    this.context_ = new $jscomp.generator.Context;
    this.program_ = b
};
$jscomp.generator.Engine_.prototype.next_ = function(b) {
    this.context_.start_();
    if (this.context_.yieldAllIterator_) return this.yieldAllStep_(this.context_.yieldAllIterator_.next, b, this.context_.next_);
    this.context_.next_(b);
    return this.nextStep_()
};
$jscomp.generator.Engine_.prototype.return_ = function(b) {
    this.context_.start_();
    var c = this.context_.yieldAllIterator_;
    if (c) return this.yieldAllStep_("return" in c ? c["return"] : function(f) {
        return {
            value: f,
            done: !0
        }
    }, b, this.context_.return);
    this.context_.return(b);
    return this.nextStep_()
};
$jscomp.generator.Engine_.prototype.throw_ = function(b) {
    this.context_.start_();
    if (this.context_.yieldAllIterator_) return this.yieldAllStep_(this.context_.yieldAllIterator_["throw"], b, this.context_.next_);
    this.context_.throw_(b);
    return this.nextStep_()
};
$jscomp.generator.Engine_.prototype.yieldAllStep_ = function(b, c, f) {
    try {
        var g = b.call(this.context_.yieldAllIterator_, c);
        $jscomp.generator.ensureIteratorResultIsObject_(g);
        if (!g.done) return this.context_.stop_(), g;
        var k = g.value
    } catch (u) {
        return this.context_.yieldAllIterator_ = null, this.context_.throw_(u), this.nextStep_()
    }
    this.context_.yieldAllIterator_ = null;
    f.call(this.context_, k);
    return this.nextStep_()
};
$jscomp.generator.Engine_.prototype.nextStep_ = function() {
    for (; this.context_.nextAddress;) try {
        var b = this.program_(this.context_);
        if (b) return this.context_.stop_(), {
            value: b.value,
            done: !1
        }
    } catch (c) {
        this.context_.yieldResult = void 0, this.context_.throw_(c)
    }
    this.context_.stop_();
    if (this.context_.abruptCompletion_) {
        b = this.context_.abruptCompletion_;
        this.context_.abruptCompletion_ = null;
        if (b.isException) throw b.exception;
        return {
            value: b.return,
            done: !0
        }
    }
    return {
        value: void 0,
        done: !0
    }
};
$jscomp.generator.Generator_ = function(b) {
    this.next = function(c) {
        return b.next_(c)
    };
    this.throw = function(c) {
        return b.throw_(c)
    };
    this.return = function(c) {
        return b.return_(c)
    };
    this[Symbol.iterator] = function() {
        return this
    }
};
$jscomp.generator.createGenerator = function(b, c) {
    c = new $jscomp.generator.Generator_(new $jscomp.generator.Engine_(c));
    $jscomp.setPrototypeOf && b.prototype && $jscomp.setPrototypeOf(c, b.prototype);
    return c
};
$jscomp.asyncExecutePromiseGenerator = function(b) {
    function c(g) {
        return b.next(g)
    }

    function f(g) {
        return b.throw(g)
    }
    return new Promise(function(g, k) {
        function u(w) {
            w.done ? g(w.value) : Promise.resolve(w.value).then(c, f).then(u, k)
        }
        u(b.next())
    })
};
$jscomp.asyncExecutePromiseGeneratorFunction = function(b) {
    return $jscomp.asyncExecutePromiseGenerator(b())
};
$jscomp.asyncExecutePromiseGeneratorProgram = function(b) {
    return $jscomp.asyncExecutePromiseGenerator(new $jscomp.generator.Generator_(new $jscomp.generator.Engine_(b)))
};
$jscomp.polyfill("Array.prototype.includes", function(b) {
    return b ? b : function(c, f) {
        var g = this;
        g instanceof String && (g = String(g));
        var k = g.length;
        f = f || 0;
        for (0 > f && (f = Math.max(f + k, 0)); f < k; f++) {
            var u = g[f];
            if (u === c || Object.is(u, c)) return !0
        }
        return !1
    }
}, "es7", "es3");
var toast_visible = !1,
    notification_visible = !1,
    timerInterval;

function success_notification(b, c = "") {
    create_notification(b, "general_notification", c)
}

function error_notification(b, c = "") {
    create_notification(b, "error", c)
}

function show_ribbon(b, c = null, f = null, g = null) {
    "timer" === c && f ? create_timer_ribbon(1E3 * f, b, g) : create_ribbon(b)
}

function create_timer_ribbon(b, c, f) {
    let g = b - (new Date).getTime();
    if (0 > g) c = `${c} <span id="countdown">EXPIRED!</span>`;
    else {
        let k = get_remaining_time(b);
        c = f ? `${c} <span id="countdown">${k}</span><span>. ${f}</span>` : `${c} <span id="countdown">${k}</span><span>. Enroll Now!</span>`
    }
    create_ribbon(c);
    0 <= g && update_timer(b)
}

function update_timer(b, c = null) {
    let f = $("#countdown"),
        g = get_remaining_time(b);
    f.text(g);
    timerInterval = setInterval(function() {
        let k = get_remaining_time(b);
        f.text(k);
        "00h: 00m: 00s" === k && (clearInterval(timerInterval), "function" === typeof c && c())
    }, 1E3)
}

function get_remaining_time(b) {
    b -= (new Date).getTime();
    return 0 > b ? "00h: 00m: 00s" : break_time_difference(b)
}

function break_time_difference(b) {
    let c = Math.floor(b / 864E5),
        f = Math.floor(b % 864E5 / 36E5),
        g = Math.floor(b % 36E5 / 6E4);
    b = Math.floor(b % 6E4 / 1E3);
    10 > f && (f = "0" + f);
    10 > g && (g = "0" + g);
    10 > b && (b = "0" + b);
    days_d = 0 >= c ? "" : c + "d: ";
    return days_d + f + "h: " + g + "m: " + b + "s"
}

function general_toast(b, c = "", f = "") {
    var g = b;
    "" != c && (g = "<span class='toast-message'>" + b + "</span><button id='" + f + "' class='" + f + " btn'>" + c + "</button>");
    create_toast("general_toast", g)
}

function error_toast(b, c = "", f = "") {
    var g = b;
    "" != c && (g = "<span class='toast-message'>" + b + "</span><button id='" + f + "' class='" + f + " btn'>" + c + "</button>");
    create_toast("error_toast", g)
}

function error_modal(b = "Something went wrong!", c = "", f = "Close", g = "", k = !1) {
    create_modal("error-modal", b, g, f, c, k)
}

function success_modal(b, c = "", f = "Close", g = null, k = "", u = !1, w = "") {
    k ? $("#modal_secondary_btn").show().text(k) : $("#modal_secondary_btn").hide();
    k = "success-modal";
    w && (k = `success-modal ${w}`);
    create_modal(k, b, g, f, c, u)
}

function alert_modal(b, c = "", f = "Ok", g = null, k = !1) {
    create_modal("alert-modal", b, g, f, c, k)
}

function confirmation_modal(b, c = "", f = "Yes", g, k = !1, u = null) {
    create_modal("confirmation-modal", b, null, f, c, k, u);
    g ? $("#modal_secondary_btn").show().text(g) : $("#modal_secondary_btn").hide()
}

function create_modal(b, c, f, g, k, u, w = null) {
    let z = $("#modal"),
        y = $("#modal_primary_btn");
    z.removeClass(["success-modal", "error-modal", "alert-modal", "confirmation-modal"]).addClass(b);
    y.text(g);
    $("#modal_text_message").html(c);
    k ? $("#modal_text_heading").show().html(k) : $("#modal_text_heading").hide();
    y.unbind();
    w && y.click(w);
    f ? y.removeClass("close_action").attr("href", f).attr("data-dismiss", "") : y.addClass("close_action").removeAttr("href").attr("data-dismiss", "modal");
    u ? z.removeData("bs.modal").modal({
        show: !0,
        backdrop: "static",
        keyboard: !1
    }) : z.removeData("bs.modal").modal({
        show: !0,
        backdrop: "true",
        keyboard: !0
    })
}

function create_toast(b, c) {
    let f = $("." + b);
    toast_visible && ($(".toast").hide(), clearTimeout(toast_visible));
    $("." + b + " .toast-body").html(c);
    f.css({
        opacity: 0,
        top: "-46px"
    }).show().animate({
        opacity: 1,
        top: "56px"
    }, 400);
    toast_visible = setTimeout(function() {
        hide_toast(f)
    }, 3E3)
}

function create_ribbon(b) {
    $("#notification-ribbon").css("position", "absolute");
    $("#general_ribbon").show();
    $("#general_ribbon_message").html(b)
}

function create_notification(b, c, f) {
    notification_visible && (clearInterval(notification_visible), notification_visible = !1);
    let g = $("#notification-ribbon");
    "none" !== $("#general_ribbon").css("display") && g.css("position", "absolute");
    let k = $("#close_notification_ribbon");
    g.removeClass(["error-notification", "has-cross-button"]);
    $("#notification-message").html(b);
    k.hide();
    "error" === c && g.addClass("error-notification");
    "with_button" === f ? (g.addClass("has-cross-button"), k.show(400).click(function() {
        g.slideUp(500);
        k.hide(500)
    })) : "with_timeout" === f && (notification_visible = setTimeout(function() {
        g.slideUp(500);
        notification_visible = !1
    }, 6E3));
    g.slideDown(500)
}

function hide_toast(b) {
    b.animate({
        opacity: 0,
        top: "-46px"
    }, 400, function() {
        b.hide();
        toast_visible = !1
    })
}
$(document).ready(function() {
    var b = $("#general_ribbon");
    if (b.length) {
        let c = b.data("ribbon-message"),
            f = b.data("ribbon-end_date"),
            g = b.data("ribbon-type");
        b = b.data("ribbon-cta-message");
        c && show_ribbon(c, g, f, b)
    }
});
let openNav = function() {},
    closeNav = function() {};
const openRecommendedMenu = function() {
    $(".recommended_menu").addClass("expanded");
    $(".recommended_menu_items").show()
};
$(document).ready(function() {
    const b = $("#mySidenav"),
        c = b.width(),
        f = $(".layer"),
        g = $(".dropdown-backdrop"),
        k = $("#content"),
        u = document.body,
        w = $("#open-nav"),
        z = $("#open-nav-mobile"),
        y = $("#login-list-item a");
    $(document).ready(function() {
        if ($("#timer1").length) {
            var q = 1E3 * $("#timer1").text();
            let A = q - (new Date).getTime();
            var t = `${get_remaining_time(q)}`;
            $("#timer").html(t);
            0 <= A && updateTimer(q)
        }
    });
    g.height($(document).height());
    f.click(function() {
        closeNav()
    });
    w.click(function() {
        $("#open-nav .arrow").hasClass("cross") ?
            closeNav() : ($("#open-nav .offer_hamburger_dot").css("visibility", "hidden"), $("#open-nav .arrow").removeClass("ic-24-menu").addClass("ic-24-cross cross"), openNav())
    });
    y.click(function() {
        closeNav()
    });
    z.click(function() {
        $("#open-nav-mobile .arrow").hasClass("cross") ? closeNav() : ($("#open-nav-mobile .arrow").removeClass("ic-24-menu").addClass("ic-24-cross cross"), openNav())
    });
    openNav = () => {
        b.css("left", 0);
        u.style.overflow = "hidden";
        f.show()
    };
    closeNav = () => {
        $("#open-nav .arrow").removeClass("ic-24-cross").addClass("ic-24-menu");
        $("#open-nav .offer_hamburger_dot").css("visibility", "visible");
        $("#open-nav-mobile .arrow").removeClass("ic-24-cross").addClass("ic-24-menu");
        $("#open-nav-mobile .arrow").removeClass("cross");
        $("#open-nav .arrow").removeClass("cross");
        $(".product-specific, .profile-container, .ham_menu_header, .ham_title, .jos-header, .jos-items").removeClass("d-none");
        $(".ham_submenu_items").removeClass("active");
        b.css("left", -c);
        u.style.overflow = "";
        f.hide();
        k.off("touchmove").off("touchend")
    };
    openRecommendedMenu();
    f.on("touchend", closeNav);
    k.on("touchstart", function(q) {
        20 >= (q.touches[0] || q.changedTouches[0]).pageX && ($(this).on("touchmove", function(t) {
            t = (t.touches[0] || t.changedTouches[0]).pageX - c;
            0 < t && (t = 0);
            b.css("left", t)
        }), $(this).on("touchend", function(t) {
            (t.touches[0] || t.changedTouches[0]).pageX >= c / 4 ? (f.show(), b.css("left", 0)) : closeNav()
        }))
    });
    $(function() {
        var q;
        $(".dropdown-hover").on("mouseenter", function() {
            clearTimeout(q);
            q = setTimeout(function(t) {
                $(".dropdown-menu", t).stop(!0, !0).show();
                $(t).toggleClass("open");
                $(".dropdown-hover").addClass("underlined");
                $(".dropdown-hover .arrow").removeClass("ic-24-chevron-down").addClass("ic-24-chevron-up");
                g.show()
            }, 45, this)
        }).on("mouseleave", function() {
            clearTimeout(q);
            q = setTimeout(function(t) {
                $(".dropdown-menu", t).stop(!0, !0).hide();
                $(t).toggleClass("open");
                $(".dropdown-hover").removeClass("underlined");
                $(".dropdown-hover .arrow").removeClass("ic-24-chevron-up").addClass("ic-24-chevron-down");
                g.hide()
            }, 45, this)
        })
    });
    $(function() {
        var q;
        $(".jos-dropdown-hover").on("mouseenter",
            function() {
                clearTimeout(q);
                q = setTimeout(function(t) {
                    $(".jos-dropdown-menu", t).stop(!0, !0).show();
                    $(t).toggleClass("open");
                    $(".jos-dropdown-hover").addClass("underlined");
                    $(".jos-dropdown-hover .arrow").removeClass("ic-24-chevron-down").addClass("ic-24-chevron-up");
                    g.show()
                }, 45, this)
            }).on("mouseleave", function() {
            clearTimeout(q);
            q = setTimeout(function(t) {
                $(".jos-dropdown-menu", t).stop(!0, !0).hide();
                $(t).toggleClass("open");
                $(".jos-dropdown-hover").removeClass("underlined");
                $(".jos-dropdown-hover .arrow").removeClass("ic-24-chevron-up").addClass("ic-24-chevron-down");
                g.hide()
            }, 45, this)
        })
    });
    $(function() {
        var q;
        $(".category-selector").on("mouseenter", function() {
            q = setTimeout(function(t) {
                $(".course-list").removeClass("show");
                $(".category-selector").removeClass("active-category");
                $("#courses_" + t).addClass("show");
                $("#" + t).addClass("active-category")
            }, 45, this.id)
        }).on("mouseleave", function() {
            clearTimeout(q)
        })
    });
    $(function() {
        $(".profile-hover").hover(function() {
            $(".profile-dropdown", this).stop(!0, !0).show();
            $(this).toggleClass("open");
            $(".profile-hover").addClass("underlined");
            $(".profile-hover .arrow").removeClass("ic-24-arrow-drop-down").addClass("ic-24-arrow-drop-up")
        }, function() {
            $(".profile-dropdown", this).stop(!0, !0).hide();
            $(this).toggleClass("open");
            $(".profile-hover").removeClass("underlined");
            $(".profile-hover .arrow").removeClass("ic-24-arrow-drop-up").addClass("ic-24-arrow-drop-down")
        })
    });
    $(function() {
        $(".ham_title").click(function() {
            let q = $(this).siblings(".ham_submenu_items");
            q.hasClass("active") ? q.removeClass("active") : (q.addClass("active"), $(".sidenav").scrollTop(0),
                $(".product-specific, .profile-container, .ham_menu_header, .ham_title, .jos-header, .jos-items").addClass("d-none"))
        });
        $(".submenu_ham_title").click(function() {
            let q = $(this).parents(".ham_submenu_items");
            q.hasClass("active") ? (q.removeClass("active"), $(".product-specific, .profile-container, .ham_menu_header, .ham_title, .jos-header, .jos-items").removeClass("d-none")) : q.addClass("active")
        })
    })
});

function get_remaining_time(b) {
    b -= (new Date).getTime();
    return 0 > b ? "00h: 00m: 00s" : break_time_difference(b)
}

function break_time_difference(b) {
    let c = Math.floor(b / 864E5),
        f = Math.floor(b % 864E5 / 36E5),
        g = Math.floor(b % 36E5 / 6E4);
    b = Math.floor(b % 6E4 / 1E3);
    10 > f && (f = "0" + f);
    10 > g && (g = "0" + g);
    10 > b && (b = "0" + b);
    days_d = 0 >= c ? "" : c + "d: ";
    return days_d + f + "h: " + g + "m: " + b + "s"
}

function updateTimer(b) {
    let c = $("#timer");
    timerInterval = setInterval(function() {
        let f = get_remaining_time(b);
        c.text(f);
        "00h: 00m: 00s" === f && clearInterval(timerInterval)
    }, 1E3)
}! function() {
    let b;
    ! function(a) {
        a.SUCCESS = "success";
        a.INFO = "info";
        a.ALERT = "alert";
        a.LOCKED = "locked";
        a.ERROR = "error"
    }(b || (b = {}));
    const c = {
            scrollClickVersion: "default",
            optionalDom: null,
            breakPoints: {
                scroll: {
                    startRes: 991,
                    endRes: 0
                },
                click: {
                    startRes: 5E3,
                    endRes: 992
                }
            },
            noOfCards: [{
                cards: 3,
                startRes: 5E3,
                endRes: 992
            }, {
                cards: 2,
                startRes: 991,
                endRes: 576
            }, {
                cards: 1,
                startRes: 575,
                endRes: 0
            }],
            customizedForTablet: !1
        },
        f = (a, d, h, e) => {
            h.split(" ").forEach(l => {
                a[d](l, e)
            })
        },
        g = (a, d, h) => f(a, "off", d, h);
    class k {
        constructor(a, d) {
            this.opts =
                Object.assign({}, c, d || {});
            this.el = "string" == typeof a ? $(a) : $("#custom-carousel");
            this.container = a;
            this.slider = this.el.find(".slider");
            this.sliderItems = this.slider.find(".slider-item");
            this.totalItems = this.sliderItems.length;
            this.itemWidth = this.noOfIndicators = this.noOfCards = this.sliderWidth = 0;
            this.deviceType = "";
            this.setCards();
            this.initCurrentState(this.slider);
            this.bind();
            this.init()
        }
        init() {
            this.animate()
        }
        setCards() {
            this.opts.noOfCards.forEach(a => {
                this.getWindowWidth() <= a.startRes && this.getWindowWidth() >=
                    a.endRes && (this.noOfCards = a.cards)
            })
        }
        initCurrentState(a) {
            const d = this.getWindowWidth(),
                {
                    breakPoints: h
                } = this.opts;
            if (h.click.startRes >= d && h.click.startRes <= d ? this.deviceType = "click" : this.deviceType = "scroll", this.slider = a, this.sliderItems = a.find(".slider-item"), this.totalItems = this.sliderItems.length, this.sliderWidth = a.outerWidth(), this.noOfIndicators = Math.ceil(this.totalItems / this.noOfCards), this.opts.customizedForTablet && 575 <= d && 991 >= d && 3 < this.totalItems) {
                var e;
                a = this.opts.optionalDom ? $(this.opts.optionalDom).width() :
                    null === (e = this.el) || void 0 === e ? void 0 : e.find(".custom-carousel").width();
                this.noOfIndicators = Math.ceil((288 * this.totalItems + 44 + 20 * (this.totalItems - 1) - a) / 288)
            }
            this.itemWidth = this.sliderItems.first().outerWidth(!0)
        }
        animate() {
            var a;
            const {
                el: d
            } = this;
            d.touch();
            this.initCurrentState(this.slider);
            parseInt(this.sliderItems.first().position().left);
            let h = null === (a = document.querySelector(this.container)) || void 0 === a ? void 0 : a.querySelector(".indicators");
            parseInt(this.sliderItems.last().position().left);
            for (a =
                0; a < this.noOfIndicators; a++) {
                var e = document.createElement("div");
                e.className = 0 === a ? "active indicator" : "indicator";
                null != h && h.appendChild(e);
                e = d.find(".indicator");
                var l = $(e[0]);
                e = $(d.find(".indicators"));
                l = (l && l.position() ? l.position().left : 0) + (l.outerWidth(!0) || 0) / 2 + (e.scrollLeft() || 0) - (e.width() || 0) / 2;
                e.animate({
                    scrollLeft: l
                })
            }
        }
        handleScroll({
            currentTarget: a
        }) {
            this.initCurrentState($(a));
            const {
                el: d,
                slider: h,
                sliderItems: e,
                sliderWidth: l,
                itemWidth: p
            } = this;
            a = -parseInt(e.first().position().left);
            a = Math.round(a /
                p);
            var m = h.parent().find(".indicator");
            let n = -1;
            for (let r = 0; r < m.length; r++) $(m[r]).hasClass("active") && (n = r);
            if (parseInt(e.last().offset().left) <= l / 2 && (a = this.noOfIndicators - 1), a !== n) $(m).removeClass("active"), m = $(m[a]), m.addClass("active"), a = $(d.find(".indicators")), m = (m && m.position() ? m.position().left : 0) + (m.outerWidth(!0) || 0) / 2 + (a.scrollLeft() || 0) - (a.width() || 0) / 2, a.animate({
                scrollLeft: m
            })
        }
        handleSwipeLeft({
            currentTarget: a
        }) {
            "click" === this.deviceType && $(a).find(".control.right").trigger("click")
        }
        handleSwipeRight({
            currentTarget: a
        }) {
            "click" ===
            this.deviceType && $(a).find(".control.left").trigger("click")
        }
        handleClick({
            currentTarget: a
        }) {
            let d = $(a).parents(".custom-carousel").find(".slider");
            if (this.initCurrentState(d), !$(a).hasClass("disabled")) {
                var h = $(a).parents(".navigation-container");
                if (!d.hasClass("in-transition") && "none" != h.css("display")) {
                    d.addClass("in-transition");
                    var e = this.sliderItems.first().outerWidth(!0),
                        l = -parseInt(this.sliderItems.first().position().left),
                        p = -1,
                        m = -1;
                    "default" === this.opts.scrollClickVersion ? (() => {
                        var n = Math.ceil(this.totalItems /
                                this.noOfCards),
                            r = d.parent().find(".indicator");
                        let v = -1;
                        for (let x = 0; x < r.length; x++) $(r[x]).hasClass("active") && (v = x);
                        if ($(a).hasClass("left") ? v == n - 1 ? e = this.totalItems * e - (e * this.noOfCards * (n - 2) + this.sliderWidth) : e *= this.noOfCards : v == n - 2 ? e = this.totalItems * e - (e * this.noOfCards * (n - 2) + this.sliderWidth) : e *= this.noOfCards, $(a).hasClass("left")) p = v - 1, m = l - e;
                        else {
                            if (v === n) return;
                            p = v + 1;
                            m = l + e
                        }
                        0 < p ? d.parent().find(".control.left").removeClass("disabled") : d.parent().find(".control.left").addClass("disabled");
                        p ==
                            n - 1 ? d.parent().find(".control.right").addClass("disabled") : d.parent().find(".control.right").removeClass("disabled");
                        this.sliderItems.animate({
                            left: -m
                        }, 600, function() {
                            d.removeClass("in-transition")
                        });
                        r = d.parent().find(".indicator");
                        $(r).removeClass("active");
                        r = $(r[p]);
                        r.addClass("active");
                        n = $(d.parent().find(".indicators"));
                        r = r.position().left + (r.outerWidth(!0) || 1) / 2 + (n.scrollLeft() || 0) - (n.width() || 1) / 2;
                        n.animate({
                            scrollLeft: r
                        })
                    })() : (() => {
                        let n = Math.round(this.sliderWidth / e),
                            r = Math.round(l / e) + 1;
                        if ($(a).hasClass("left")) p =
                            r - 1, m = l - e;
                        else {
                            if (r + (n - 1) === this.totalItems) return;
                            p = r + 1;
                            m = l + e
                        }
                        1 < p ? d.parent().find(".control.left").removeClass("disabled") : d.parent().find(".control.left").addClass("disabled");
                        p + (n - 1) >= this.totalItems ? d.parent().find(".control.right").addClass("disabled") : d.parent().find(".control.right").removeClass("disabled");
                        d.parent().find(".active-indicator").animate({
                            left: 16 * (p - 1) + "px"
                        }, 600);
                        this.sliderItems.animate({
                            left: -m
                        }, 600, function() {
                            d.removeClass("in-transition")
                        })
                    })()
                }
            }
        }
        update(a) {
            this.el && (this.destroy(),
                this.bind(), this.init())
        }
        destroy() {
            this.el && (this.unbind(), this.el.find(".indicators").empty())
        }
        unbind() {
            const {
                el: a
            } = this;
            g(a.find(".slider"), "scroll", this.handleScroll.bind(this));
            g(a, "swipeLeft", this.handleSwipeLeft.bind(this));
            g(a, "swipeRight", this.handleSwipeRight.bind(this));
            g(a.find(".control"), "click", this.handleClick.bind(this))
        }
        bind() {
            var {
                el: a
            } = this, d = a.find(".slider"), h = this.handleScroll.bind(this);
            f(d, "on", "scroll", h);
            d = this.handleSwipeLeft.bind(this);
            f(a, "on", "swipeLeft", d);
            d = this.handleSwipeRight.bind(this);
            f(a, "on", "swipeRight", d);
            a = a.find(".control");
            d = this.handleClick.bind(this);
            f(a, "on", "click", d)
        }
        getWindowWidth() {
            return window.screen.width
        }
    }
    class u {
        constructor() {}
        categoryClickHandler({
            currentTarget: a
        }) {
            let d = a.id,
                h = document.querySelector("button.categories-name.active"),
                e = document.querySelector(".category-card-wrapper.active"),
                l = Array.from(document.querySelectorAll(".category-card-wrapper")).filter(p => parseInt(p.id) == d)[0];
            l.classList.contains("inactive") && (h.classList.remove("active"), h.classList.add("inactive"),
                e.classList.remove("active"), e.classList.add("inactive"), a.classList.remove("inactive"), a.classList.add("active"), l.classList.remove("inactive"), l.classList.add("active"), a = document.getElementById("courses-section"), a = a.querySelector(".courses-category-wrapper"), a = a.offsetTop - 100, $("html,body").animate({
                    scrollTop: a
                }, "slow"))
        }
        viewMoreClickHandler({
            currentTarget: a
        }) {
            var d;
            let h = a.id;
            a = a.parentElement;
            a.querySelectorAll(".hide").forEach(e => {
                e.classList.remove("hide")
            });
            a.querySelectorAll(".show").forEach(e => {
                e.classList.remove("show")
            });
            null === (d = a.querySelector(`#${h}`)) || void 0 === d || d.remove()
        }
        addEventListener(a, d, h) {
            Array.from(a).forEach(e => {
                e.addEventListener(d, h)
            })
        }
        initEventListeners() {
            const a = document.getElementsByClassName("categories-name"),
                d = document.getElementsByClassName("view-more");
            [
                [a, this.categoryClickHandler.bind(this)],
                [d, this.viewMoreClickHandler.bind(this)]
            ].forEach(([h, e]) => {
                this.addEventListener(h, "click", e)
            })
        }
        createCoursesSlider() {
            let a = [],
                d = $(".collapse-title"),
                h = $(".course-carousel");
            for (let e = 0; e < h.length; e++) {
                let l = $(h[e]).attr("id");
                a[l] = new k("#" + l, {
                    optionalDom: "#accordion",
                    noOfCards: [{
                        cards: 2,
                        startRes: 5E3,
                        endRes: 992
                    }, {
                        cards: 1.5,
                        startRes: 991,
                        endRes: 576
                    }, {
                        cards: 1.2,
                        startRes: 575,
                        endRes: 501
                    }, {
                        cards: 1,
                        startRes: 500,
                        endRes: 0
                    }],
                    customizedForTablet: !0
                })
            }
            d.click(({
                currentTarget: e
            }) => {
                e = $(e).parent().attr("id").replace("collapse-item", "course-carousel");
                a[e].update()
            })
        }
        init() {
            this.initEventListeners();
            this.createCoursesSlider()
        }
    }
    class w {
        constructor() {
            this.dom = $("#collapsible-ribbon");
            this.switch = $(".toggle-ribbon");
            this.setVisibility();
            this.bind()
        }
        setVisibility() {
            this.visibility = !this.dom.hasClass("hide")
        }
        bind() {
            this.switch.on("click", this.toggle.bind(this))
        }
        toggle() {
            this.visibility ? this.dom.addClass("hide") : this.dom.removeClass("hide");
            this.setVisibility()
        }
    }
    class z {
        constructor() {}
        createEducatorSlider() {
            new k("#educator-carousel", {
                noOfCards: [{
                    cards: 3,
                    startRes: 5E3,
                    endRes: 992
                }, {
                    cards: 1.5,
                    startRes: 991,
                    endRes: 576
                }, {
                    cards: 1.2,
                    startRes: 575,
                    endRes: 501
                }, {
                    cards: 1,
                    startRes: 500,
                    endRes: 0
                }]
            })
        }
        init() {
            this.createEducatorSlider()
        }
    }
    class y {
        constructor() {}
        createEducatorSlider() {
            new k("#alumni-carousel", {
                scrollClickVersion: "latest",
                noOfCards: [{
                    cards: 1,
                    startRes: 5E3,
                    endRes: 992
                }, {
                    cards: 1.2,
                    startRes: 991,
                    endRes: 576
                }, {
                    cards: 1.2,
                    startRes: 575,
                    endRes: 501
                }, {
                    cards: 1,
                    startRes: 500,
                    endRes: 0
                }]
            })
        }
        init() {
            this.createEducatorSlider()
        }
    }
    class q {
        constructor() {
            this.scrollTriggers = $(".scroll-trigger");
            this.bind()
        }
        bind() {
            this.startTimer();
            this.scrollTriggers.on("click", this.smoothScroll.bind(this))
        }
        smoothScroll(a) {
            const d = $(a.currentTarget.dataset.scroll_to).offset().top,
                h = $("#navbar").height();
            a = $(a.currentTarget).offset().top > d ? d - h : d - 2 * h;
            $("html, body").animate({
                scrollTop: a
            }, 600)
        }
        startTimer() {
            const a = document.getElementById("timer-data");
            a && updateTimer(1E3 * a.dataset.end_time)
        }
    }
    class t {
        constructor() {
            this.courseList = [];
            this.pinnedCourses = [];
            this.trackingSourceSuffix = this.pinnedCourseHeading = "";
            this.filterCourse = [];
            this.specializations = [];
            this.regular = [];
            this.debounceTimer = this.enterKeyPressed = 0;
            this.cardsShown = this.dropdownshown = !1;
            this.searchResultQuery = "";
            this.showChips = !1;
            this.encryptedTrackingData = ""
        }
        getSearchData() {
            return $jscomp.asyncExecutePromiseGeneratorFunction(function*() {
                return new Promise((a, d) => {
                    $.ajax({
                        url: "/home/get_search_data",
                        type: "GET",
                        success: h => {
                            a(h)
                        },
                        error: h => {
                            d(h)
                        }
                    })
                })
            })
        }
        initSearchData() {
            const a = this;
            return $jscomp.asyncExecutePromiseGeneratorFunction(function*() {
                return new Promise((d, h) => {
                    a.getSearchData().then(e => {
                        const {
                            course_list: l,
                            pinned_courses: p,
                            search_recommendation_heading: m,
                            tracking_data: n
                        } = e, r = new Set;
                        a.courseList = [];
                        l.forEach(v => {
                            !1 === r.has(v.id) && (r.add(v.id), a.courseList.push(v))
                        });
                        a.pinnedCourses = p;
                        a.pinnedCourseHeading = m;
                        a.trackingSourceSuffix = a.pinnedCourseHeading.toLowerCase().includes("recommended") ? "-recommended" : a.pinnedCourseHeading.toLowerCase().includes("popular") ? "-most-popular" : "";
                        a.filterCourse = [];
                        a.enterKeyPressed = 0;
                        a.debounceTimer = 0;
                        a.dropdownshown = !1;
                        a.cardsShown = !1;
                        a.encryptedTrackingData = n;
                        d()
                    }).catch(e => {
                        h(`Something went wring ${e}`)
                    })
                })
            })
        }
        searchModal() {
            this.initSearchData().then(() => {
                $(() => {
                    $(".search-bar-trigger").on("click",
                        this.loadSearchModal.bind(this));
                    $(".search-bar-trigger").on("click", this.toggleSearchModal);
                    $("#search-bar").on("input", this.queryChange.bind(this));
                    $("#search-bar").on("keydown", this.handleSubmit.bind(this));
                    $(".ic-24-cross").on("click", this.handleCloseModal.bind(this));
                    $("#search_recommendation").on("mouseenter", ".suggestion", this.handleActiveSuggestion.bind(this));
                    $("#search_recommendation").on("click", ".suggestion", this.handleSearchStats.bind(this));
                    $(".recommended_trainings_chips").on("click",
                        ".search-tags", this.handleSearchChipStats.bind(this));
                    $(".recommended-trainings-container").on("click", ".search-card", this.handleSearchCardStats.bind(this));
                    const a = (new URLSearchParams(window.location.search)).get("search");
                    a && ($("#search-bar").val(a), this.queryChange({
                        target: {
                            value: a
                        }
                    }), this.handleSubmit({
                        which: 13
                    }), this.toggleSearchModal())
                })
            }).catch(() => {})
        }
        toggleSearchModal() {
            const a = $("#search_modal");
            let d;
            return a.hasClass("hide") ? (d = !0, $("body").addClass("modal-open"), $(".search-container").removeClass("hide"),
                $(".search-container").addClass("show"), a.removeClass("hide"), a.addClass("show")) : a.hasClass("show") && (d = !1, $("body").removeClass("modal-open"), $(".search-container").addClass("hide"), $(".search-container").removeClass("show"), a.removeClass("show"), a.addClass("hide")), $(".query-input").focus(), d
        }
        loadSearchModal() {
            document.querySelector("#search_modal") && ($(".search_query_heading").html(`${this.pinnedCourseHeading} (${this.pinnedCourses.length})`), $(".recommended_trainings_chips").empty(), this.showChips = !0, $.each(this.pinnedCourses, function(a, d) {
                $(".recommended_trainings_chips").append(`<a class="search-tags" id=${d.id}" data-url="/${d.redirect_url}?tracking_source=trainings-search-tags"><div class="chip">${d.name}</div></a>`)
            }))
        }
        handleCloseModal() {
            this.enterKeyPressed = 0;
            this.searchResultQuery = "";
            this.sendSearchTrackingStats(null, !0);
            this.filterCourse = [];
            this.specializations = [];
            this.regular = [];
            $("#search-bar").val("");
            const a = $("#search_modal");
            a.hasClass("show") && ($("body").removeClass("modal-open"),
                a.removeClass("show"), a.addClass("hide"));
            $(".search_recommendation").html("");
            $(".empty-search").remove();
            $(".search-pinned-heading").remove();
            $(".search_result_heading").remove();
            $(".recommended-trainings-container").removeClass("show_query_result_cards");
            $("#search-courses-card").remove();
            $("#search-pgc-card").remove();
            $("#course-type").remove();
            $("#pgc-type").remove();
            0 === $(".recommended_trainings_chips").length && ($(".recommended-trainings-container").append(`<div class="search_query_heading mt-xs">${this.pinnedCourseHeading}</div>`),
                $(".recommended-trainings-container").append('<div class="recommended_trainings_chips"></div>'))
        }
        cleanString(a) {
            return a.replace(/\W/g, d => `\\${d}`)
        }
        getDelimiter(a) {
            a = this.cleanString(a);
            return new RegExp(`(\\b${a})`, "gi")
        }
        handleActiveSuggestion(a) {
            $(".selected").removeClass("selected");
            a.target.classList.add("selected")
        }
        handleSearchStats(a) {
            const d = a.currentTarget.getAttribute("data-url");
            this.sendSearchTrackingStats(a.currentTarget.id, !1, "dropdown");
            window.location.href = d
        }
        handleSearchChipStats(a) {
            const d =
                a.currentTarget.getAttribute("data-url");
            this.sendSearchTrackingStats(a.currentTarget.id, !1, "tag");
            window.location.href = d
        }
        handleSearchCardStats(a) {
            const d = a.currentTarget.getAttribute("data-url");
            this.sendSearchTrackingStats(a.currentTarget.id, !1, "card");
            window.location.href = d
        }
        handleSubmit(a) {
            if (13 === a.which)
                if (clearInterval(this.debounceId), this.enterKeyPressed = 1, $(".search_recommendation").html(""), a = $("#recommended-trainings-container"), a.html(""), 0 !== this.filterCourse.length) this.dropdownshown =
                    this.showChips = !1, $(".empty-search").remove(), a.addClass("show_query_result_cards"), a.append(`<div class="search_result_heading">SEARCH RESULTS (${this.filterCourse.length})</div>`), 0 != this.regular.length && (a.append('<div id="course-type" class="course-type-heading">CERTIFICATION COURSES</div>'), a.append('<div id="search-courses-card" class="search-courses-card"></div>'), this.generate_course_cards(this.regular, "#search-courses-card")), 0 != this.specializations.length && (a.append('<div id="pgc-type" class="course-type-heading">PLACEMENT GUARANTEE COURSES</div>'),
                        a.append('<div id="search-pgc-card" class="search-courses-card"></div>'), this.generate_course_cards(this.specializations, "#search-pgc-card"));
                else {
                    this.dropdownshown = !1;
                    0 === $(".empty-search").length && $('<div class="empty-search">No results found...</div>').insertBefore(".recommended-trainings-container");
                    a.addClass("show_query_result_cards");
                    a.append(`<div class="search-pinned-heading">${this.pinnedCourseHeading}(${this.pinnedCourses.length})</div>`);
                    a.append('<div id="search-courses-card" class="search-courses-card"></div>');
                    const d = this;
                    $.each(this.pinnedCourses, function(h, e) {
                        $("#search-courses-card").append(`<a data-url="${e.redirect_url}?tracking_source=trainings-search-no-results ${d.trackingSourceSuffix.includes("recommended")?"-recommended":"-most-popular"}" class="search-card" id="${e.id}">\n                        <div id="course-card">\n                            <div class="course-container">\n                                <div class="course-image">\n                                    <img src="/cached_uploads/homepage/media/courses_section/card_images/${e.url}.png"/>\n                                </div>\n                                <div class="course-content">\n                                    <div class="content-wrapper">\n                                        <div class="duration">\n                                            <span> ${e.url.includes("specialization")?
`${Math.round(e.duration/7/4)} months`:`${Math.round(e.duration/7)} weeks`} </span>\n                                            ${e.is_hindi_training?"tally"===e.url?'<span class="hindi-lang-label"><img class="speaker" src="/cached_uploads/homepage/media/icon/language-hindi.png"/></span>':'<span class="hindi-lang-label"><img class="speaker" src="/cached_uploads/homepage/media/icon/language.png"/></span>':""}\n                                        </div>\n                                        <div class="name"> ${e.name} </div>\n                                        <div class="feedback"> ${e.average_rating?
`<div class="star"><img src="/static/images/icons/star.svg" data-alt="star"/><span>${e.average_rating}</span></div>`:""}\n                                            ${e.average_rating&&e.show_enrolled_users&&e.enrolled_users?'<div class="v-line"></div>':""}\n                                            ${e.show_enrolled_users&&e.enrolled_users?`<div class="learner">${e.enrolled_users.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")} learners</div>`:""}\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </a>`)
                    })
                }
        }
        debounce(a,
            d) {
            return (...h) => {
                this.debounceTimer && clearTimeout(this.debounceTimer);
                this.debounceTimer = setTimeout(() => a(...h), d)
            }
        }
        queryChange(a) {
            var d;
            a = null == a || null === (d = a.target) || void 0 === d ? void 0 : d.value;
            if (this.debounce(this.trackQuerySearch.bind(this), 500)(a), 0 != a.length) {
                this.showChips = !1;
                this.dropdownshown = !0;
                const h = this.getDelimiter(a);
                this.filterCourse = this.courseList.filter(e => null != e.name.match(h));
                d = $("#search_recommendation");
                if (0 !== this.filterCourse.length) {
                    d.removeClass("hide");
                    d.addClass("show");
                    d.html("");
                    const e = [];
                    if (this.specializations = this.filterCourse.filter(l => l.url.includes("specialization")), this.regular = this.filterCourse.filter(l => !l.url.includes("specialization")), 0 != this.regular.length) e.push('<span class="course-type-tag">CERTIFICATION COURSES</span>'), this.generate_search_results(this.regular, e, a, h);
                    0 != this.specializations.length && (e.push('<span class="course-type-tag">PlACEMENT GUARANTEE COURSES</span>'), this.generate_search_results(this.specializations, e, a, h));
                    d.append(e)
                } else $("#search_recommendation").html("")
            } else $("#search_recommendation").html("")
        }
        trackQuerySearch(a) {
            this.encryptedTrackingData &&
                $.post("https://tracking.internshala.com/", JSON.stringify({
                    deliveryStreamName: "trainings_searched_keywords",
                    data: {
                        encryptedData: this.encryptedTrackingData,
                        typed_keyword: a
                    }
                })).catch(d => {})
        }
        sendSearchTrackingStats(a = null, d = null, h = null) {
            this.encryptedTrackingData && "A" != $(document.activeElement).prop("tagName") && (d = {
                deliveryStreamName: "trainings_search_stats",
                data: {
                    encryptedData: this.encryptedTrackingData,
                    enter_key_pressed: this.enterKeyPressed,
                    recommendation_shown: this.pinnedCourseHeading.toLowerCase().includes("recommended") ?
                        "1" : "0",
                    course_ids_shown_from: this.showChips ? "tags" : this.dropdownshown ? "dropdown" : 0 == this.filterCourse.length ? "no_search_results" : "search_cards",
                    course_ids_shown: (0 !== this.filterCourse.length || 0 !== this.filterCourse.length ? this.filterCourse : this.pinnedCourses).map(e => e.id).join(","),
                    is_modal_closed: d ? "1" : "0"
                }
            }, h && (d.data.clicked_course_id_source = h), a && (d.data.clicked_course_id = a), $.post("https://tracking.internshala.com/", JSON.stringify(d)).catch(e => {}))
        }
        generate_course_cards(a, d) {
            $.each(a, function(h,
                e) {
                $(d).append(`<a data-url="/${e.redirect_url}?tracking_source=trainings-search-results" class="search-card" id="${e.id}">\n                        <div id="course-card">\n                            <div class="course-container">\n                                <div class="course-image">\n                                    <img src="/cached_uploads/homepage/media/courses_section/card_images/${e.url}.png"/>\n                                </div>\n                                <div class="course-content">\n                                    <div class="content-wrapper">\n                                        <div class="duration">\n                                            <span> ${e.url.includes("specialization")?
`${Math.round(e.duration/7/4)} months`:`${Math.round(e.duration/7)} weeks`} </span>\n                                            ${e.is_hindi_training?"tally"===e.url?'<span class="hindi-lang-label"><img class="speaker" src="/cached_uploads/homepage/media/icon/language-hindi.png"/></span>':'<span class="hindi-lang-label"><img class="speaker" src="/cached_uploads/homepage/media/icon/language.png"/></span>':""}\n                                        </div>\n                                        <div class="name"> ${e.name} </div>\n                                        <div class="feedback">\n                                            ${e.average_rating?
`<div class="star"><img src="/static/images/icons/star.svg" data-alt="star"/><span>${e.average_rating.toFixed(1)}</span></div>`:""}\n                                            ${e.average_rating&&e.show_enrolled_users&&e.enrolled_users?'<div class="v-line"></div>':""}\n                                            ${e.show_enrolled_users&&e.enrolled_users?`<div class="learner">${e.enrolled_users.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")} learners</div>`:""}\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </a>`)
            })
        }
        generate_search_results(a,
            d, h, e) {
            $.each(a, function(l, p) {
                l = p.name.split(e).map(n => n.toLowerCase().trim() === h.toLowerCase().trim() && "" != h ? `<span class="highlight">${n.replace(/,/g,"")}</span>` : n.replace(/,/g, "")).join("");
                const m = document.createElement("a");
                m.className = "suggestion";
                m.id = p.id;
                m.innerHTML = l;
                m.dataset.url = `/${p.redirect_url}/?tracking_source=trainings-search-dropdown`;
                d.push(m)
            })
        }
    }
    class A {
        constructor() {}
        lazyLoadingImgs(a = "img-responsive", d = "lazy-load") {
            function h(p, m) {
                let n = new Image;
                n.src = p.getAttribute("data-src");
                n.alt = p.getAttribute("data-alt");
                n.classList.add(a);
                n.addEventListener("load", function(r) {
                    p.insertAdjacentElement("beforebegin", r.target);
                    p.classList.remove(d);
                    p.remove();
                    !0
                }, {
                    once: !0
                });
                m && m.unobserve(p)
            }
            const e = document.querySelectorAll("." + d);
            if (!("IntersectionObserver" in window)) return void e.forEach(function(p) {
                setTimeout(function() {
                    h(p)
                }, 0)
            });
            const l = new IntersectionObserver(function(p, m) {
                p.forEach(function(n) {
                    n.isIntersecting && setTimeout(function() {
                        h(n.target, m)
                    }, 0)
                })
            }, {
                threshold: 0,
                rootMargin: "120px"
            });
            e.forEach(function(p) {
                l.observe(p)
            })
        }
        init() {
            this.lazyLoadingImgs()
        }
    }
    class B {
        constructor() {
            document.querySelectorAll(".top-companies-container .image-marquee").forEach(a => {
                $(a).find(".section-content").hover(({
                    currentTarget: d
                }) => {
                    $(d).find(".marquee-row").css("animation-play-state", "paused")
                }, ({
                    currentTarget: d
                }) => {
                    $(d).find(".marquee-row").css("animation-play-state", "running")
                })
            })
        }
    }
    class C {
        trackHomepageView() {
            var a = document.getElementById("homepage-data");
            a && (a = a.dataset, a.tracking_data && $.post("https://tracking.internshala.com/",
                JSON.stringify({
                    deliveryStreamName: "trainings_homepage_revamp_2022_tracking",
                    data: {
                        encryptedData: a.tracking_data,
                        is_personalised: a.is_personalised ? 1 : 0
                    }
                })).catch(d => {}))
        }
        trackVerticalScroll() {
            let a = $(window).scrollTop();
            const d = $("section.track");
            let h = "",
                e = [],
                l = null;
            const p = document.getElementById("homepage-data").dataset;
            d.each((m, n) => {
                e.push({
                    sectionName: n.id,
                    top: n.offsetTop,
                    bottom: n.offsetTop + n.offsetHeight + 2 * $("#navbar").height()
                })
            });
            $(document).on("scroll.trackVerticalScroll2s", () => {
                const m =
                    $(window).scrollTop(),
                    n = m > a ? "top_to_bottom" : "bottom_to_top";
                0 === e.length && $(document).off("scroll.trackVerticalScroll2s");
                const r = screen.availHeight / 2 + window.pageYOffset + $("#navbar").height();
                let v = e.filter(x => "top_to_bottom" === n ? x.top <= r && x.top >= window.pageYOffset : x.bottom >= r && x.top <= r);
                if (v = v[0] ? v[0].sectionName : "", "" != v && ("" === h || h !== v)) {
                    clearTimeout(l);
                    const x = m > a ? "top_to_bottom" : "bottom_to_top";
                    l = setTimeout(() => {
                        this.pushIntoDataLayer({
                            event: "scrolledVerticallyHomepage2s",
                            sectionScrolled: v + (p.is_personalised ?
                                "_personalised" : ""),
                            scrollMotionVertical: x
                        });
                        this.pushIntoDataLayer({
                            event: "ga4-scrolledVerticallyHomepage2s",
                            scroll: `${v+(p.is_personalised?"_personalised":"")}_${x}`
                        });
                        e = e.filter(D => D.sectionName != v)
                    }, 2E3)
                }
                h = v;
                a = m
            })
        }
        trackOnLoad() {
            const a = document.querySelector("section.track"),
                d = document.getElementById("homepage-data").dataset;
            this.isInViewportCustom(a) && setTimeout(() => {
                this.isInViewportCustom(a) && (this.pushIntoDataLayer({
                    event: "scrolledVerticallyHomepage2s",
                    sectionScrolled: a.id + (d.is_personalised ?
                        "_personalised" : ""),
                    scrollMotionVertical: "on_load"
                }), this.pushIntoDataLayer({
                    event: "ga4-scrolledVerticallyHomepage2s",
                    scroll: a.id + (d.is_personalised ? "_personalised" : "") + "on_load"
                }))
            }, 2E3)
        }
        trackCourseCategory() {
            var a;
            const d = null !== (a = document.querySelector(".category-card-wrapper.active")) && void 0 !== a ? a : document.getElementById("course-carousel-1");
            let h = !1;
            $(document).on("scroll.trackCategoryVisibility", () => {
                this.isInViewportCustom(d, 300) && !h && (h = !0, setTimeout(() => {
                    h = !1;
                    this.isInViewportCustom(d,
                        0) && ($(document).off("scroll.trackCategoryVisibility"), this.pushIntoDataLayer({
                        event: "categoryVisible",
                        categoryVisibleName: d.dataset.category.replace("_", " ")
                    }), this.pushIntoDataLayer({
                        event: "ga4-categoryVisible",
                        view: d.dataset.category.replace("_", " ")
                    }))
                }, 2E3))
            });
            (0 !== $(".categories-name.inactive").length ? $(".categories-name.inactive") : $(".collapse-title")).on("click.trackCategory", e => {
                this.pushIntoDataLayer({
                    event: "categoryVisible",
                    categoryVisibleName: e.target.innerText
                });
                this.pushIntoDataLayer({
                    event: "ga4-categoryVisible",
                    view: e.target.innerText
                });
                $(e.currentTarget).off("click.trackCategory")
            })
        }
        trackClick() {
            $(".track-click").on("click.trackClick", a => {
                a = $(a.currentTarget);
                a.off("click.trackClick");
                a = a.data();
                this.pushIntoDataLayer({
                    event: a.click_event_name,
                    clickedElementName: a.click_event_value
                });
                this.pushIntoDataLayer({
                    event: `ga4-${a.click_event_name}`,
                    click: a.click_event_value
                })
            })
        }
        pushIntoDataLayer(a) {
            void 0 !== window.dataLayer && window.dataLayer.push(a)
        }
        isInViewportCustom(a, d = 100) {
            if (a) {
                const h = a.offsetTop;
                a = h + a.offsetHeight;
                const e = $(window).scrollTop();
                return h > e && a > e && h + d < e + screen.height
            }
        }
    }
    class E {
        constructor() {
            var a;
            let d = $("#header"),
                h = $(".navbar").outerHeight() || 0,
                e = (null === (a = $("#top-companies-section").offset()) || void 0 === a ? void 0 : a.top) || 0;
            $(window).on("scroll.handleScroll", function() {
                ($(window).scrollTop() || 0) + h > e + 150 ? d.hasClass("sticky") && d.removeClass("sticky").addClass("fixed") : d.hasClass("fixed") && d.removeClass("fixed").addClass("sticky")
            })
        }
    }
    class F {
        constructor() {
            (this.videoDom = document.getElementById("video")) &&
            (this.playLottie(), this.bind())
        }
        bind() {
            this.playVideoOnVisibility = this.playVideoOnVisibility.bind(this);
            document.addEventListener("scroll", this.playVideoOnVisibility)
        }
        playVideoOnVisibility() {
            this.videoDom.getBoundingClientRect().top <= document.documentElement.clientHeight && (this.videoDom.play(), document.removeEventListener("scroll", this.playVideoOnVisibility))
        }
        playLottie() {
            0 < $(".lottie-wrapper").length && $(".lottie-wrapper").each(function() {
                const a = this.dataset;
                bodymovin.loadAnimation({
                    container: this,
                    path: a.src,
                    renderer: "svg",
                    loop: a.loop,
                    autoplay: !0
                })
            })
        }
    }
    class G {
        constructor() {
            this.footer = document.getElementById("footer");
            this.bottomNav = document.getElementById("bottom-navigation");
            this.bind()
        }
        bind() {
            this.bottomNav && (this.hideBottomNavigation = this.hideBottomNavigation.bind(this), document.addEventListener("scroll", this.hideBottomNavigation))
        }
        hideBottomNavigation(a) {
            a = this.footer.getBoundingClientRect().top <= document.documentElement.clientHeight;
            this.bottomNav.style.display = a ? "none" : "flex"
        }
    }(new class {
        constructor() {}
        initAllTasks() {
            (new u).init();
            new w;
            (new t).searchModal();
            (new z).init();
            (new y).init();
            new q;
            (new A).init();
            new B;
            new E;
            new F;
            new G;
            const a = new C;
            a.trackHomepageView();
            a.trackVerticalScroll();
            a.trackOnLoad();
            a.trackCourseCategory();
            a.trackClick()
        }
    }).initAllTasks()
}();
document.addEventListener("DOMContentLoaded", function() {
    var b = document.getElementById("page-tracking-param");
    if (b) {
        var c = b.getAttribute("data-page-tracking-param");
        b = b.getAttribute("data-course-url");
        ["sp", "hp"].includes(c) && $.ajax("/page_landing_tracker/track_user", {
            data: {
                page: c,
                course_url: b
            },
            type: "POST"
        })
    }
});