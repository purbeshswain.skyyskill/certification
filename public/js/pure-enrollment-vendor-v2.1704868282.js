/*
 jQuery v3.3.1 | (c) JS Foundation and other contributors | jquery.org/license  Bootstrap v4.4.1 (https://getbootstrap.com/)
 Copyright 2011-2019 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
 Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
*/
! function(L, k) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = L.document ? k(L, !0) : function(oa) {
        if (!oa.document) throw Error("jQuery requires a window with a document");
        return k(oa)
    } : k(L)
}("undefined" != typeof window ? window : this, function(L, k) {
    function oa(a, b, c) {
        var f, g = (b = b || X).createElement("script");
        if (g.text = a, c)
            for (f in pa) c[f] && (g[f] = c[f]);
        b.head.appendChild(g).parentNode.removeChild(g)
    }

    function Ja(a) {
        return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? t[d.call(a)] ||
            "object" : typeof a
    }

    function ta(a) {
        var b = !!a && "length" in a && a.length,
            c = Ja(a);
        return !H(a) && !ia(a) && ("array" === c || 0 === b || "number" == typeof b && 0 < b && b - 1 in a)
    }

    function va(a, b) {
        return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
    }

    function Y(a, b, c) {
        return H(b) ? e.grep(a, function(f, g) {
            return !!b.call(f, g, f) !== c
        }) : b.nodeType ? e.grep(a, function(f) {
            return f === b !== c
        }) : "string" != typeof b ? e.grep(a, function(f) {
            return -1 < w.call(b, f) !== c
        }) : e.filter(b, a, c)
    }

    function tb(a, b) {
        for (;
            (a = a[b]) && 1 !== a.nodeType;);
        return a
    }

    function z(a) {
        var b = {};
        return e.each(a.match(Ya) || [], function(c, f) {
            b[f] = !0
        }), b
    }

    function K(a) {
        return a
    }

    function F(a) {
        throw a;
    }

    function A(a, b, c, f) {
        var g;
        try {
            a && H(g = a.promise) ? g.call(a).done(b).fail(c) : a && H(g = a.then) ? g.call(a, b, c) : b.apply(void 0, [a].slice(f))
        } catch (l) {
            c.apply(void 0, [l])
        }
    }

    function N() {
        X.removeEventListener("DOMContentLoaded", N);
        L.removeEventListener("load", N);
        e.ready()
    }

    function fa(a, b) {
        return b.toUpperCase()
    }

    function ja(a) {
        return a.replace(Rc, "ms-").replace(Sc, fa)
    }

    function Ba() {
        this.expando =
            e.expando + Ba.uid++
    }

    function Ra(a, b, c) {
        var f;
        if (void 0 === c && 1 === a.nodeType)
            if (f = "data-" + b.replace(Tc, "-$&").toLowerCase(), "string" == typeof(c = a.getAttribute(f))) {
                try {
                    f = c, c = "true" === f || "false" !== f && ("null" === f ? null : f === +f + "" ? +f : Uc.test(f) ? JSON.parse(f) : f)
                } catch (g) {}
                Ka.set(a, b, c)
            } else c = void 0;
        return c
    }

    function bb(a, b, c, f) {
        var g, l, n = 20,
            r = f ? function() {
                return f.cur()
            } : function() {
                return e.css(a, b, "")
            },
            u = r(),
            C = c && c[3] || (e.cssNumber[b] ? "" : "px"),
            y = (e.cssNumber[b] || "px" !== C && +u) && Cb.exec(e.css(a, b));
        if (y && y[3] !==
            C) {
            u /= 2;
            C = C || y[3];
            for (y = +u || 1; n--;) e.style(a, b, y + C), 0 >= (1 - l) * (1 - (l = r() / u || .5)) && (n = 0), y /= l;
            y *= 2;
            e.style(a, b, y + C);
            c = c || []
        }
        return c && (y = +y || +u || 0, g = c[1] ? y + (c[1] + 1) * c[2] : +c[2], f && (f.unit = C, f.start = y, f.end = g)), g
    }

    function lb(a, b) {
        for (var c, f, g = [], l = 0, n = a.length; l < n; l++)
            if ((f = a[l]).style)
                if (c = f.style.display, b) {
                    if ("none" === c && (g[l] = V.get(f, "display") || null, g[l] || (f.style.display = "")), "" === f.style.display && Pb(f)) {
                        c = l;
                        var r = void 0;
                        var u = f.ownerDocument;
                        f = f.nodeName;
                        var C = vc[f];
                        u = C || (r = u.body.appendChild(u.createElement(f)),
                            C = e.css(r, "display"), r.parentNode.removeChild(r), "none" === C && (C = "block"), vc[f] = C, C);
                        g[c] = u
                    }
                } else "none" !== c && (g[l] = "none", V.set(f, "display", c));
        for (l = 0; l < n; l++) null != g[l] && (a[l].style.display = g[l]);
        return a
    }

    function xa(a, b) {
        var c;
        return c = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : [], void 0 === b || b && va(a, b) ? e.merge([a], c) : c
    }

    function Db(a, b) {
        for (var c = 0, f = a.length; c < f; c++) V.set(a[c], "globalEval", !b || V.get(b[c],
            "globalEval"))
    }

    function mb(a, b, c, f, g) {
        for (var l, n, r, u, C = b.createDocumentFragment(), y = [], I = 0, O = a.length; I < O; I++)
            if ((l = a[I]) || 0 === l)
                if ("object" === Ja(l)) e.merge(y, l.nodeType ? [l] : l);
                else if (Vc.test(l)) {
            n = n || C.appendChild(b.createElement("div"));
            r = (wc.exec(l) || ["", ""])[1].toLowerCase();
            r = Sa[r] || Sa._default;
            n.innerHTML = r[1] + e.htmlPrefilter(l) + r[2];
            for (r = r[0]; r--;) n = n.lastChild;
            e.merge(y, n.childNodes);
            (n = C.firstChild).textContent = ""
        } else y.push(b.createTextNode(l));
        C.textContent = "";
        for (I = 0; l = y[I++];)
            if (f &&
                -1 < e.inArray(l, f)) g && g.push(l);
            else if (u = e.contains(l.ownerDocument, l), n = xa(C.appendChild(l), "script"), u && Db(n), c)
            for (r = 0; l = n[r++];) xc.test(l.type || "") && c.push(l);
        return C
    }

    function zb() {
        return !0
    }

    function nb() {
        return !1
    }

    function Qb() {
        try {
            return X.activeElement
        } catch (a) {}
    }

    function Eb(a, b, c, f, g, l) {
        var n, r;
        if ("object" == typeof b) {
            "string" != typeof c && (f = f || c, c = void 0);
            for (r in b) Eb(a, r, c, f, b[r], l);
            return a
        }
        if (null == f && null == g ? (g = c, f = c = void 0) : null == g && ("string" == typeof c ? (g = f, f = void 0) : (g = f, f = c, c = void 0)), !1 === g) g = nb;
        else if (!g) return a;
        return 1 === l && (n = g, (g = function(u) {
            return e().off(u), n.apply(this, arguments)
        }).guid = n.guid || (n.guid = e.guid++)), a.each(function() {
            e.event.add(this, b, g, f, c)
        })
    }

    function Va(a, b) {
        return va(a, "table") && va(11 !== b.nodeType ? b : b.firstChild, "tr") ? e(a).children("tbody")[0] || a : a
    }

    function ic(a) {
        return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a
    }

    function Fb(a) {
        return "true/" === (a.type || "").slice(0, 5) ? a.type = a.type.slice(5) : a.removeAttribute("type"), a
    }

    function Rb(a, b) {
        var c,
            f, g, l, n, r;
        if (1 === b.nodeType) {
            if (V.hasData(a) && (c = V.access(a), f = V.set(b, c), r = c.events))
                for (g in delete f.handle, f.events = {}, r)
                    for (c = 0, f = r[g].length; c < f; c++) e.event.add(b, g, r[g][c]);
            Ka.hasData(a) && (l = Ka.access(a), n = e.extend({}, l), Ka.set(b, n))
        }
    }

    function Ta(a, b, c, f) {
        b = Gb.apply([], b);
        var g, l, n, r = 0,
            u = a.length,
            C = u - 1,
            y = b[0],
            I = H(y);
        if (I || 1 < u && "string" == typeof y && !G.checkClone && Wc.test(y)) return a.each(function(W) {
            var Q = a.eq(W);
            I && (b[0] = y.call(this, W, Q.html()));
            Ta(Q, b, c, f)
        });
        if (u && (g = mb(b, a[0].ownerDocument, !1, a, f), l = g.firstChild, 1 === g.childNodes.length && (g = l), l || f)) {
            for (n = (l = e.map(xa(g, "script"), ic)).length; r < u; r++) {
                var O = g;
                r !== C && (O = e.clone(O, !0, !0), n && e.merge(l, xa(O, "script")));
                c.call(a[r], O, r)
            }
            if (n)
                for (g = l[l.length - 1].ownerDocument, e.map(l, Fb), r = 0; r < n; r++) O = l[r], xc.test(O.type || "") && !V.access(O, "globalEval") && e.contains(g, O) && (O.src && "module" !== (O.type || "").toLowerCase() ? e._evalUrl && e._evalUrl(O.src) : oa(O.textContent.replace(Xc, ""), g, O))
        }
        return a
    }

    function Sb(a, b, c) {
        for (var f = b ? e.filter(b, a) : a, g =
                0; null != (b = f[g]); g++) c || 1 !== b.nodeType || e.cleanData(xa(b)), b.parentNode && (c && e.contains(b.ownerDocument, b) && Db(xa(b, "script")), b.parentNode.removeChild(b));
        return a
    }

    function ub(a, b, c) {
        var f, g, l, n, r = a.style;
        return (c = c || Tb(a)) && ("" !== (n = c.getPropertyValue(b) || c[b]) || e.contains(a.ownerDocument, a) || (n = e.style(a, b)), !G.pixelBoxStyles() && jc.test(n) && Yc.test(b) && (f = r.width, g = r.minWidth, l = r.maxWidth, r.minWidth = r.maxWidth = r.width = n, n = c.width, r.width = f, r.minWidth = g, r.maxWidth = l)), void 0 !== n ? n + "" : n
    }

    function Ub(a,
        b) {
        return {
            get: function() {
                if (!a()) return (this.get = b).apply(this, arguments);
                delete this.get
            }
        }
    }

    function Vb(a) {
        var b = e.cssProps[a];
        if (!b) {
            b = e.cssProps;
            a: {
                var c = a;
                if (!(c in yc)) {
                    for (var f = c[0].toUpperCase() + c.slice(1), g = zc.length; g--;)
                        if ((c = zc[g] + f) in yc) break a;
                    c = void 0
                }
            }
            b = b[a] = c || a
        }
        return b
    }

    function Wb(a, b, c) {
        return (a = Cb.exec(b)) ? Math.max(0, a[2] - (c || 0)) + (a[3] || "px") : b
    }

    function Hb(a, b, c, f, g, l) {
        var n = "width" === b ? 1 : 0,
            r = 0,
            u = 0;
        if (c === (f ? "border" : "content")) return 0;
        for (; 4 > n; n += 2) "margin" === c && (u += e.css(a,
            c + cb[n], !0, g)), f ? ("content" === c && (u -= e.css(a, "padding" + cb[n], !0, g)), "margin" !== c && (u -= e.css(a, "border" + cb[n] + "Width", !0, g))) : (u += e.css(a, "padding" + cb[n], !0, g), "padding" !== c ? u += e.css(a, "border" + cb[n] + "Width", !0, g) : r += e.css(a, "border" + cb[n] + "Width", !0, g));
        return !f && 0 <= l && (u += Math.max(0, Math.ceil(a["offset" + b[0].toUpperCase() + b.slice(1)] - l - u - r - .5))), u
    }

    function Xb(a, b, c) {
        var f = Tb(a),
            g = ub(a, b, f),
            l = "border-box" === e.css(a, "boxSizing", !1, f),
            n = l;
        if (jc.test(g)) {
            if (!c) return g;
            g = "auto"
        }
        return n = n && (G.boxSizingReliable() ||
            g === a.style[b]), ("auto" === g || !parseFloat(g) && "inline" === e.css(a, "display", !1, f)) && (g = a["offset" + b[0].toUpperCase() + b.slice(1)], n = !0), (g = parseFloat(g) || 0) + Hb(a, b, c || (l ? "border" : "content"), n, f, g) + "px"
    }

    function Fa(a, b, c, f, g) {
        return new Fa.prototype.init(a, b, c, f, g)
    }

    function Ib() {
        Yb && (!1 === X.hidden && L.requestAnimationFrame ? L.requestAnimationFrame(Ib) : L.setTimeout(Ib, e.fx.interval), e.fx.tick())
    }

    function Zb() {
        return L.setTimeout(function() {
            Ab = void 0
        }), Ab = Date.now()
    }

    function Ua(a, b) {
        var c, f = 0,
            g = {
                height: a
            };
        for (b = b ? 1 : 0; 4 > f; f += 2 - b) g["margin" + (c = cb[f])] = g["padding" + c] = a;
        return b && (g.opacity = g.width = a), g
    }

    function $b(a, b, c) {
        for (var f, g = (Na.tweeners[b] || []).concat(Na.tweeners["*"]), l = 0, n = g.length; l < n; l++)
            if (f = g[l].call(c, b, a)) return f
    }

    function kc(a, b) {
        var c, f, g, l, n;
        for (c in a)
            if (f = ja(c), g = b[f], l = a[c], Array.isArray(l) && (g = l[1], l = a[c] = l[0]), c !== f && (a[f] = l, delete a[c]), (n = e.cssHooks[f]) && "expand" in n)
                for (c in l = n.expand(l), delete a[f], l) c in a || (a[c] = l[c], b[c] = g);
            else b[f] = g
    }

    function Na(a, b, c) {
        var f, g = 0,
            l = Na.prefilters.length,
            n = e.Deferred().always(function() {
                delete r.elem
            }),
            r = function() {
                if (f) return !1;
                var C = Ab || Zb();
                C = Math.max(0, u.startTime + u.duration - C);
                for (var y = 1 - (C / u.duration || 0), I = 0, O = u.tweens.length; I < O; I++) u.tweens[I].run(y);
                return n.notifyWith(a, [u, y, C]), 1 > y && O ? C : (O || n.notifyWith(a, [u, 1, 0]), n.resolveWith(a, [u]), !1)
            },
            u = n.promise({
                elem: a,
                props: e.extend({}, b),
                opts: e.extend(!0, {
                    specialEasing: {},
                    easing: e.easing._default
                }, c),
                originalProperties: b,
                originalOptions: c,
                startTime: Ab || Zb(),
                duration: c.duration,
                tweens: [],
                createTween: function(C,
                    y) {
                    C = e.Tween(a, u.opts, C, y, u.opts.specialEasing[C] || u.opts.easing);
                    return u.tweens.push(C), C
                },
                stop: function(C) {
                    var y = 0,
                        I = C ? u.tweens.length : 0;
                    if (f) return this;
                    for (f = !0; y < I; y++) u.tweens[y].run(1);
                    return C ? (n.notifyWith(a, [u, 1, 0]), n.resolveWith(a, [u, C])) : n.rejectWith(a, [u, C]), this
                }
            });
        c = u.props;
        for (kc(c, u.opts.specialEasing); g < l; g++)
            if (b = Na.prefilters[g].call(u, a, c, u.opts)) return H(b.stop) && (e._queueHooks(u.elem, u.opts.queue).stop = b.stop.bind(b)), b;
        return e.map(c, $b, u), H(u.opts.start) && u.opts.start.call(a,
            u), u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always), e.fx.timer(e.extend(r, {
            elem: a,
            anim: u,
            queue: u.opts.queue
        })), u
    }

    function db(a) {
        return (a.match(Ya) || []).join(" ")
    }

    function eb(a) {
        return a.getAttribute && a.getAttribute("class") || ""
    }

    function ob(a) {
        return Array.isArray(a) ? a : "string" == typeof a ? a.match(Ya) || [] : []
    }

    function Jb(a, b, c, f) {
        var g;
        if (Array.isArray(b)) e.each(b, function(l, n) {
            c || Zc.test(a) ? f(a, n) : Jb(a + "[" + ("object" == typeof n && null != n ? l : "") + "]", n,
                c, f)
        });
        else if (c || "object" !== Ja(b)) f(a, b);
        else
            for (g in b) Jb(a + "[" + g + "]", b[g], c, f)
    }

    function Kb(a) {
        return function(b, c) {
            "string" != typeof b && (c = b, b = "*");
            var f = 0,
                g = b.toLowerCase().match(Ya) || [];
            if (H(c))
                for (; b = g[f++];) "+" === b[0] ? (b = b.slice(1) || "*", (a[b] = a[b] || []).unshift(c)) : (a[b] = a[b] || []).push(c)
        }
    }

    function ac(a, b, c, f) {
        function g(r) {
            var u;
            return l[r] = !0, e.each(a[r] || [], function(C, y) {
                C = y(b, c, f);
                return "string" != typeof C || n || l[C] ? n ? !(u = C) : void 0 : (b.dataTypes.unshift(C), g(C), !1)
            }), u
        }
        var l = {},
            n = a === lc;
        return g(b.dataTypes[0]) ||
            !l["*"] && g("*")
    }

    function fb(a, b) {
        var c, f, g = e.ajaxSettings.flatOptions || {};
        for (c in b) void 0 !== b[c] && ((g[c] ? a : f || (f = {}))[c] = b[c]);
        return f && e.extend(!0, a, f), a
    }
    var gb = [],
        X = L.document,
        mc = Object.getPrototypeOf,
        hb = gb.slice,
        Gb = gb.concat,
        pb = gb.push,
        w = gb.indexOf,
        t = {},
        d = t.toString,
        h = t.hasOwnProperty,
        p = h.toString,
        D = p.call(Object),
        G = {},
        H = function(a) {
            return "function" == typeof a && "number" != typeof a.nodeType
        },
        ia = function(a) {
            return null != a && a === a.window
        },
        pa = {
            type: !0,
            src: !0,
            noModule: !0
        },
        e = function(a, b) {
            return new e.fn.init(a,
                b)
        },
        vb = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    e.fn = e.prototype = {
        jquery: "3.3.1",
        constructor: e,
        length: 0,
        toArray: function() {
            return hb.call(this)
        },
        get: function(a) {
            return null == a ? hb.call(this) : 0 > a ? this[a + this.length] : this[a]
        },
        pushStack: function(a) {
            a = e.merge(this.constructor(), a);
            return a.prevObject = this, a
        },
        each: function(a) {
            return e.each(this, a)
        },
        map: function(a) {
            return this.pushStack(e.map(this, function(b, c) {
                return a.call(b, c, b)
            }))
        },
        slice: function() {
            return this.pushStack(hb.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(a) {
            var b = this.length;
            a = +a + (0 > a ? b : 0);
            return this.pushStack(0 <= a && a < b ? [this[a]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: pb,
        sort: gb.sort,
        splice: gb.splice
    };
    e.extend = e.fn.extend = function() {
        var a, b, c, f, g, l = arguments[0] || {},
            n = 1,
            r = arguments.length,
            u = !1;
        "boolean" == typeof l && (u = l, l = arguments[n] || {}, n++);
        "object" == typeof l || H(l) || (l = {});
        for (n === r && (l = this, n--); n < r; n++)
            if (null != (a = arguments[n]))
                for (b in a) {
                    var C = l[b];
                    l !== (c = a[b]) && (u &&
                        c && (e.isPlainObject(c) || (f = Array.isArray(c))) ? (f ? (f = !1, g = C && Array.isArray(C) ? C : []) : g = C && e.isPlainObject(C) ? C : {}, l[b] = e.extend(u, g, c)) : void 0 !== c && (l[b] = c))
                }
        return l
    };
    e.extend({
        expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(a) {
            throw Error(a);
        },
        noop: function() {},
        isPlainObject: function(a) {
            var b, c;
            return !(!a || "[object Object]" !== d.call(a)) && (!(b = mc(a)) || "function" == typeof(c = h.call(b, "constructor") && b.constructor) && p.call(c) === D)
        },
        isEmptyObject: function(a) {
            for (var b in a) return !1;
            return !0
        },
        globalEval: function(a) {
            oa(a)
        },
        each: function(a, b) {
            var c, f = 0;
            if (ta(a))
                for (c = a.length; f < c && !1 !== b.call(a[f], f, a[f]); f++);
            else
                for (f in a)
                    if (!1 === b.call(a[f], f, a[f])) break;
            return a
        },
        trim: function(a) {
            return null == a ? "" : (a + "").replace(vb, "")
        },
        makeArray: function(a, b) {
            b = b || [];
            return null != a && (ta(Object(a)) ? e.merge(b, "string" == typeof a ? [a] : a) : pb.call(b, a)), b
        },
        inArray: function(a, b, c) {
            return null == b ? -1 : w.call(b, a, c)
        },
        merge: function(a, b) {
            for (var c = +b.length, f = 0, g = a.length; f < c; f++) a[g++] = b[f];
            return a.length =
                g, a
        },
        grep: function(a, b, c) {
            var f = [],
                g = 0,
                l = a.length;
            for (c = !c; g < l; g++) !b(a[g], g) !== c && f.push(a[g]);
            return f
        },
        map: function(a, b, c) {
            var f, g, l = 0,
                n = [];
            if (ta(a))
                for (f = a.length; l < f; l++) null != (g = b(a[l], l, c)) && n.push(g);
            else
                for (l in a) null != (g = b(a[l], l, c)) && n.push(g);
            return Gb.apply([], n)
        },
        guid: 1,
        support: G
    });
    "function" == typeof Symbol && (e.fn[Symbol.iterator] = gb[Symbol.iterator]);
    e.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(a, b) {
        t["[object " + b + "]"] = b.toLowerCase()
    });
    var Ma = function(a) {
        function b(m, q, v, x) {
            var B, E, M, P, J = q && q.ownerDocument,
                R = q ? q.nodeType : 9;
            if (v = v || [], "string" != typeof m || !m || 1 !== R && 9 !== R && 11 !== R) return v;
            if (!x && ((q ? q.ownerDocument || q : ka) !== ba && wb(q), q = q || ba, qa)) {
                if (11 !== R && (P = $c.exec(m)))
                    if (B = P[1])
                        if (9 === R) {
                            if (!(E = q.getElementById(B))) return v;
                            if (E.id === B) return v.push(E), v
                        } else {
                            if (J && (E = J.getElementById(B)) && Ga(q, E) && E.id === B) return v.push(E), v
                        }
                else {
                    if (P[2]) return qb.apply(v, q.getElementsByTagName(m)), v;
                    if ((B = P[3]) && ra.getElementsByClassName && q.getElementsByClassName) return qb.apply(v,
                        q.getElementsByClassName(B)), v
                }
                if (!(!ra.qsa || Oa[m + " "] || ca && ca.test(m))) {
                    if (1 !== R) {
                        J = q;
                        var S = m
                    } else if ("object" !== q.nodeName.toLowerCase()) {
                        (M = q.getAttribute("id")) ? M = M.replace(Ac, Bc): q.setAttribute("id", M = ea);
                        for (B = (E = bc(m)).length; B--;) E[B] = "#" + M + " " + W(E[B]);
                        S = E.join(",");
                        J = nc.test(m) && I(q.parentNode) || q
                    }
                    if (S) try {
                        return qb.apply(v, J.querySelectorAll(S)), v
                    } catch (ma) {} finally {
                        M === ea && q.removeAttribute("id")
                    }
                }
            }
            return wa(m.replace(cc, "$1"), q, v, x)
        }

        function c() {
            function m(v, x) {
                return q.push(v + " ") > U.cacheLength &&
                    delete m[q.shift()], m[v + " "] = x
            }
            var q = [];
            return m
        }

        function f(m) {
            return m[ea] = !0, m
        }

        function g(m) {
            var q = ba.createElement("fieldset");
            try {
                return !!m(q)
            } catch (v) {
                return !1
            } finally {
                q.parentNode && q.parentNode.removeChild(q)
            }
        }

        function l(m, q) {
            m = m.split("|");
            for (var v = m.length; v--;) U.attrHandle[m[v]] = q
        }

        function n(m, q) {
            var v = q && m,
                x = v && 1 === m.nodeType && 1 === q.nodeType && m.sourceIndex - q.sourceIndex;
            if (x) return x;
            if (v)
                for (; v = v.nextSibling;)
                    if (v === q) return -1;
            return m ? 1 : -1
        }

        function r(m) {
            return function(q) {
                return "input" ===
                    q.nodeName.toLowerCase() && q.type === m
            }
        }

        function u(m) {
            return function(q) {
                var v = q.nodeName.toLowerCase();
                return ("input" === v || "button" === v) && q.type === m
            }
        }

        function C(m) {
            return function(q) {
                return "form" in q ? q.parentNode && !1 === q.disabled ? "label" in q ? "label" in q.parentNode ? q.parentNode.disabled === m : q.disabled === m : q.isDisabled === m || q.isDisabled !== !m && ad(q) === m : q.disabled === m : "label" in q && q.disabled === m
            }
        }

        function y(m) {
            return f(function(q) {
                return q = +q, f(function(v, x) {
                    for (var B, E = m([], v.length, q), M = E.length; M--;) v[B =
                        E[M]] && (v[B] = !(x[B] = v[B]))
                })
            })
        }

        function I(m) {
            return m && "undefined" != typeof m.getElementsByTagName && m
        }

        function O() {}

        function W(m) {
            for (var q = 0, v = m.length, x = ""; q < v; q++) x += m[q].value;
            return x
        }

        function Q(m, q, v) {
            var x = q.dir,
                B = q.next,
                E = B || x,
                M = v && "parentNode" === E,
                P = Wa++;
            return q.first ? function(J, R, S) {
                for (; J = J[x];)
                    if (1 === J.nodeType || M) return m(J, R, S);
                return !1
            } : function(J, R, S) {
                var ma, na, T, la = [Pa, P];
                if (S)
                    for (; J = J[x];) {
                        if ((1 === J.nodeType || M) && m(J, R, S)) return !0
                    } else
                        for (; J = J[x];)
                            if (1 === J.nodeType || M)
                                if (T = J[ea] ||
                                    (J[ea] = {}), na = T[J.uniqueID] || (T[J.uniqueID] = {}), B && B === J.nodeName.toLowerCase()) J = J[x] || J;
                                else {
                                    if ((ma = na[E]) && ma[0] === Pa && ma[1] === P) return la[2] = ma[2];
                                    if (na[E] = la, la[2] = m(J, R, S)) return !0
                                }
                return !1
            }
        }

        function da(m) {
            return 1 < m.length ? function(q, v, x) {
                for (var B = m.length; B--;)
                    if (!m[B](q, v, x)) return !1;
                return !0
            } : m[0]
        }

        function ya(m, q, v, x, B) {
            for (var E, M = [], P = 0, J = m.length, R = null != q; P < J; P++)(E = m[P]) && (v && !v(E, x, B) || (M.push(E), R && q.push(P)));
            return M
        }

        function Xa(m, q, v, x, B, E) {
            return x && !x[ea] && (x = Xa(x)), B && !B[ea] &&
                (B = Xa(B, E)), f(function(M, P, J, R) {
                    var S, ma = [],
                        na = [],
                        T = P.length,
                        la;
                    if (!(la = M)) {
                        la = q || "*";
                        for (var ha = J.nodeType ? [J] : J, rb = [], Za = 0, Ca = ha.length; Za < Ca; Za++) b(la, ha[Za], rb);
                        la = rb
                    }
                    la = !m || !M && q ? la : ya(la, ma, m, J, R);
                    ha = v ? B || (M ? m : T || x) ? [] : P : la;
                    if (v && v(la, ha, J, R), x) {
                        var Ha = ya(ha, na);
                        x(Ha, [], J, R);
                        for (J = Ha.length; J--;)(S = Ha[J]) && (ha[na[J]] = !(la[na[J]] = S))
                    }
                    if (M) {
                        if (B || m) {
                            if (B) {
                                Ha = [];
                                for (J = ha.length; J--;)(S = ha[J]) && Ha.push(la[J] = S);
                                B(null, ha = [], Ha, R)
                            }
                            for (J = ha.length; J--;)(S = ha[J]) && -1 < (Ha = B ? xb(M, S) : ma[J]) && (M[Ha] = !(P[Ha] =
                                S))
                        }
                    } else ha = ya(ha === P ? ha.splice(T, ha.length) : ha), B ? B(null, P, ha, R) : qb.apply(P, ha)
                })
        }

        function za(m) {
            var q, v, x = m.length,
                B = U.relative[m[0].type];
            var E = B || U.relative[" "];
            for (var M = B ? 1 : 0, P = Q(function(S) {
                    return S === q
                }, E, !0), J = Q(function(S) {
                    return -1 < xb(q, S)
                }, E, !0), R = [function(S, ma, na) {
                    S = !B && (na || ma !== sa) || ((q = ma).nodeType ? P(S, ma, na) : J(S, ma, na));
                    return q = null, S
                }]; M < x; M++)
                if (E = U.relative[m[M].type]) R = [Q(da(R), E)];
                else {
                    if ((E = U.filter[m[M].type].apply(null, m[M].matches))[ea]) {
                        for (v = ++M; v < x && !U.relative[m[v].type]; v++);
                        return Xa(1 < M && da(R), 1 < M && W(m.slice(0, M - 1).concat({
                            value: " " === m[M - 2].type ? "*" : ""
                        })).replace(cc, "$1"), E, M < v && za(m.slice(M, v)), v < x && za(m = m.slice(v)), v < x && W(m))
                    }
                    R.push(E)
                }
            return da(R)
        }

        function Z(m, q) {
            var v = 0 < q.length,
                x = 0 < m.length,
                B = function(E, M, P, J, R) {
                    var S, ma, na = 0,
                        T = "0",
                        la = E && [],
                        ha = [],
                        rb = sa,
                        Za = E || x && U.find.TAG("*", R),
                        Ca = Pa += null == rb ? 1 : Math.random() || .1,
                        Ha = Za.length;
                    for (R && (sa = M === ba || M || R); T !== Ha && null != (S = Za[T]); T++) {
                        if (x && S) {
                            var oc = 0;
                            for (M || S.ownerDocument === ba || (wb(S), P = !qa); ma = m[oc++];)
                                if (ma(S,
                                        M || ba, P)) {
                                    J.push(S);
                                    break
                                }
                            R && (Pa = Ca)
                        }
                        v && ((S = !ma && S) && na--, E && la.push(S))
                    }
                    if (na += T, v && T !== na) {
                        for (oc = 0; ma = q[oc++];) ma(la, ha, M, P);
                        if (E) {
                            if (0 < na)
                                for (; T--;) la[T] || ha[T] || (ha[T] = bd.call(J));
                            ha = ya(ha)
                        }
                        qb.apply(J, ha);
                        R && !E && 0 < ha.length && 1 < na + q.length && b.uniqueSort(J)
                    }
                    return R && (Pa = Ca, sa = rb), la
                };
            return v ? f(B) : B
        }
        var ua, U, aa, wa, sa, Da, $a, ba, Ea, qa, ca, La, Aa, Ga, ea = "sizzle" + 1 * new Date,
            ka = a.document,
            Pa = 0,
            Wa = 0,
            Ia = c(),
            sb = c(),
            Oa = c(),
            ab = function(m, q) {
                return m === q && ($a = !0), 0
            },
            pc = {}.hasOwnProperty,
            yb = [],
            bd = yb.pop,
            cd = yb.push,
            qb = yb.push,
            Cc = yb.slice,
            xb = function(m, q) {
                for (var v = 0, x = m.length; v < x; v++)
                    if (m[v] === q) return v;
                return -1
            },
            dd = RegExp("[\\x20\\t\\r\\n\\f]+", "g"),
            cc = RegExp("^[\\x20\\t\\r\\n\\f]+|((?:^|[^\\\\])(?:\\\\.)*)[\\x20\\t\\r\\n\\f]+$", "g"),
            ed = RegExp("^[\\x20\\t\\r\\n\\f]*,[\\x20\\t\\r\\n\\f]*"),
            fd = RegExp("^[\\x20\\t\\r\\n\\f]*([>+~]|[\\x20\\t\\r\\n\\f])[\\x20\\t\\r\\n\\f]*"),
            gd = RegExp("=[\\x20\\t\\r\\n\\f]*([^\\]'\"]*?)[\\x20\\t\\r\\n\\f]*\\]", "g"),
            hd = RegExp(":((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\])*)|.*)\\)|)"),
            id = RegExp("^(?:\\\\.|[\\w-]|[^\x00-\\xa0])+$"),
            dc = {
                ID: RegExp("^#((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)"),
                CLASS: RegExp("^\\.((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)"),
                TAG: RegExp("^((?:\\\\.|[\\w-]|[^\x00-\\xa0])+|[*])"),
                ATTR: RegExp("^\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\]"),
                PSEUDO: RegExp("^:((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\])*)|.*)\\)|)"),
                CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\([\\x20\\t\\r\\n\\f]*(even|odd|(([+-]|)(\\d*)n|)[\\x20\\t\\r\\n\\f]*(?:([+-]|)[\\x20\\t\\r\\n\\f]*(\\d+)|))[\\x20\\t\\r\\n\\f]*\\)|)", "i"),
                bool: RegExp("^(?:checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped)$", "i"),
                needsContext: RegExp("^[\\x20\\t\\r\\n\\f]*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\([\\x20\\t\\r\\n\\f]*((?:-\\d)?\\d*)[\\x20\\t\\r\\n\\f]*\\)|)(?=[^-]|$)",
                    "i")
            },
            jd = /^(?:input|select|textarea|button)$/i,
            kd = /^h\d$/i,
            Lb = /^[^{]+\{\s*\[native \w/,
            $c = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            nc = /[+~]/,
            ib = RegExp("\\\\([\\da-f]{1,6}[\\x20\\t\\r\\n\\f]?|([\\x20\\t\\r\\n\\f])|.)", "ig"),
            jb = function(m, q, v) {
                m = "0x" + q - 65536;
                return m !== m || v ? q : 0 > m ? String.fromCharCode(m + 65536) : String.fromCharCode(m >> 10 | 55296, 1023 & m | 56320)
            },
            Ac = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            Bc = function(m, q) {
                return q ? "\x00" === m ? "\ufffd" : m.slice(0, -1) + "\\" + m.charCodeAt(m.length - 1).toString(16) +
                    " " : "\\" + m
            },
            Dc = function() {
                wb()
            },
            ad = Q(function(m) {
                return !0 === m.disabled && ("form" in m || "label" in m)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            qb.apply(yb = Cc.call(ka.childNodes), ka.childNodes), yb[ka.childNodes.length].nodeType
        } catch (m) {
            qb = {
                apply: yb.length ? function(q, v) {
                    cd.apply(q, Cc.call(v))
                } : function(q, v) {
                    for (var x = q.length, B = 0; q[x++] = v[B++];);
                    q.length = x - 1
                }
            }
        }
        var ra = b.support = {};
        var ld = b.isXML = function(m) {
            m = m && (m.ownerDocument || m).documentElement;
            return !!m && "HTML" !== m.nodeName
        };
        var wb = b.setDocument = function(m) {
            var q,
                v;
            m = m ? m.ownerDocument || m : ka;
            return m !== ba && 9 === m.nodeType && m.documentElement ? (ba = m, Ea = ba.documentElement, qa = !ld(ba), ka !== ba && (v = ba.defaultView) && v.top !== v && (v.addEventListener ? v.addEventListener("unload", Dc, !1) : v.attachEvent && v.attachEvent("onunload", Dc)), ra.attributes = g(function(x) {
                    return x.className = "i", !x.getAttribute("className")
                }), ra.getElementsByTagName = g(function(x) {
                    return x.appendChild(ba.createComment("")), !x.getElementsByTagName("*").length
                }), ra.getElementsByClassName = Lb.test(ba.getElementsByClassName),
                ra.getById = g(function(x) {
                    return Ea.appendChild(x).id = ea, !ba.getElementsByName || !ba.getElementsByName(ea).length
                }), ra.getById ? (U.filter.ID = function(x) {
                    var B = x.replace(ib, jb);
                    return function(E) {
                        return E.getAttribute("id") === B
                    }
                }, U.find.ID = function(x, B) {
                    if ("undefined" != typeof B.getElementById && qa) return (x = B.getElementById(x)) ? [x] : []
                }) : (U.filter.ID = function(x) {
                    var B = x.replace(ib, jb);
                    return function(E) {
                        return (E = "undefined" != typeof E.getAttributeNode && E.getAttributeNode("id")) && E.value === B
                    }
                }, U.find.ID = function(x,
                    B) {
                    if ("undefined" != typeof B.getElementById && qa) {
                        var E, M = B.getElementById(x);
                        if (M) {
                            if ((E = M.getAttributeNode("id")) && E.value === x) return [M];
                            var P = B.getElementsByName(x);
                            for (B = 0; M = P[B++];)
                                if ((E = M.getAttributeNode("id")) && E.value === x) return [M]
                        }
                        return []
                    }
                }), U.find.TAG = ra.getElementsByTagName ? function(x, B) {
                    return "undefined" != typeof B.getElementsByTagName ? B.getElementsByTagName(x) : ra.qsa ? B.querySelectorAll(x) : void 0
                } : function(x, B) {
                    var E = [],
                        M = 0;
                    B = B.getElementsByTagName(x);
                    if ("*" === x) {
                        for (; x = B[M++];) 1 === x.nodeType &&
                            E.push(x);
                        return E
                    }
                    return B
                }, U.find.CLASS = ra.getElementsByClassName && function(x, B) {
                    if ("undefined" != typeof B.getElementsByClassName && qa) return B.getElementsByClassName(x)
                }, La = [], ca = [], (ra.qsa = Lb.test(ba.querySelectorAll)) && (g(function(x) {
                    Ea.appendChild(x).innerHTML = "<a id='" + ea + "'></a><select id='" + ea + "-\r\\' msallowcapture=''><option selected=''></option></select>";
                    x.querySelectorAll("[msallowcapture^='']").length && ca.push("[*^$]=[\\x20\\t\\r\\n\\f]*(?:''|\"\")");
                    x.querySelectorAll("[selected]").length ||
                        ca.push("\\[[\\x20\\t\\r\\n\\f]*(?:value|checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped)");
                    x.querySelectorAll("[id~=" + ea + "-]").length || ca.push("~=");
                    x.querySelectorAll(":checked").length || ca.push(":checked");
                    x.querySelectorAll("a#" + ea + "+*").length || ca.push(".#.+[+~]")
                }), g(function(x) {
                    x.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                    var B = ba.createElement("input");
                    B.setAttribute("type",
                        "hidden");
                    x.appendChild(B).setAttribute("name", "D");
                    x.querySelectorAll("[name=d]").length && ca.push("name[\\x20\\t\\r\\n\\f]*[*^$|!~]?=");
                    2 !== x.querySelectorAll(":enabled").length && ca.push(":enabled", ":disabled");
                    Ea.appendChild(x).disabled = !0;
                    2 !== x.querySelectorAll(":disabled").length && ca.push(":enabled", ":disabled");
                    x.querySelectorAll("*,:x");
                    ca.push(",.*:")
                })), (ra.matchesSelector = Lb.test(Aa = Ea.matches || Ea.webkitMatchesSelector || Ea.mozMatchesSelector || Ea.oMatchesSelector || Ea.msMatchesSelector)) &&
                g(function(x) {
                    ra.disconnectedMatch = Aa.call(x, "*");
                    Aa.call(x, "[s!='']:x");
                    La.push("!=", ":((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?=)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\])*)|.*)\\)|)")
                }), ca = ca.length && new RegExp(ca.join("|")), La = La.length && new RegExp(La.join("|")),
                q = Lb.test(Ea.compareDocumentPosition), Ga = q || Lb.test(Ea.contains) ? function(x, B) {
                    var E = 9 === x.nodeType ? x.documentElement : x;
                    B = B && B.parentNode;
                    return x === B || !(!B || 1 !== B.nodeType || !(E.contains ? E.contains(B) : x.compareDocumentPosition && 16 & x.compareDocumentPosition(B)))
                } : function(x, B) {
                    if (B)
                        for (; B = B.parentNode;)
                            if (B === x) return !0;
                    return !1
                }, ab = q ? function(x, B) {
                    if (x === B) return $a = !0, 0;
                    var E = !x.compareDocumentPosition - !B.compareDocumentPosition;
                    return E || (1 & (E = (x.ownerDocument || x) === (B.ownerDocument || B) ? x.compareDocumentPosition(B) :
                        1) || !ra.sortDetached && B.compareDocumentPosition(x) === E ? x === ba || x.ownerDocument === ka && Ga(ka, x) ? -1 : B === ba || B.ownerDocument === ka && Ga(ka, B) ? 1 : Da ? xb(Da, x) - xb(Da, B) : 0 : 4 & E ? -1 : 1)
                } : function(x, B) {
                    if (x === B) return $a = !0, 0;
                    var E = 0,
                        M = x.parentNode,
                        P = B.parentNode,
                        J = [x],
                        R = [B];
                    if (!M || !P) return x === ba ? -1 : B === ba ? 1 : M ? -1 : P ? 1 : Da ? xb(Da, x) - xb(Da, B) : 0;
                    if (M === P) return n(x, B);
                    for (; x = x.parentNode;) J.unshift(x);
                    for (x = B; x = x.parentNode;) R.unshift(x);
                    for (; J[E] === R[E];) E++;
                    return E ? n(J[E], R[E]) : J[E] === ka ? -1 : R[E] === ka ? 1 : 0
                }, ba) : ba
        };
        b.matches = function(m, q) {
            return b(m, null, null, q)
        };
        b.matchesSelector = function(m, q) {
            if ((m.ownerDocument || m) !== ba && wb(m), q = q.replace(gd, "='$1']"), !(!ra.matchesSelector || !qa || Oa[q + " "] || La && La.test(q) || ca && ca.test(q))) try {
                var v = Aa.call(m, q);
                if (v || ra.disconnectedMatch || m.document && 11 !== m.document.nodeType) return v
            } catch (x) {}
            return 0 < b(q, ba, null, [m]).length
        };
        b.contains = function(m, q) {
            return (m.ownerDocument || m) !== ba && wb(m), Ga(m, q)
        };
        b.attr = function(m, q) {
            (m.ownerDocument || m) !== ba && wb(m);
            var v = U.attrHandle[q.toLowerCase()];
            v = v && pc.call(U.attrHandle, q.toLowerCase()) ? v(m, q, !qa) : void 0;
            return void 0 !== v ? v : ra.attributes || !qa ? m.getAttribute(q) : (v = m.getAttributeNode(q)) && v.specified ? v.value : null
        };
        b.escape = function(m) {
            return (m + "").replace(Ac, Bc)
        };
        b.error = function(m) {
            throw Error("Syntax error, unrecognized expression: " + m);
        };
        b.uniqueSort = function(m) {
            var q, v = [],
                x = 0,
                B = 0;
            if ($a = !ra.detectDuplicates, Da = !ra.sortStable && m.slice(0), m.sort(ab), $a) {
                for (; q = m[B++];) q === m[B] && (x = v.push(B));
                for (; x--;) m.splice(v[x], 1)
            }
            return Da = null, m
        };
        var qc =
            b.getText = function(m) {
                var q, v = "",
                    x = 0;
                if (q = m.nodeType)
                    if (1 === q || 9 === q || 11 === q) {
                        if ("string" == typeof m.textContent) return m.textContent;
                        for (m = m.firstChild; m; m = m.nextSibling) v += qc(m)
                    } else {
                        if (3 === q || 4 === q) return m.nodeValue
                    }
                else
                    for (; q = m[x++];) v += qc(q);
                return v
            };
        (U = b.selectors = {
            cacheLength: 50,
            createPseudo: f,
            match: dc,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(m) {
                    return m[1] =
                        m[1].replace(ib, jb), m[3] = (m[3] || m[4] || m[5] || "").replace(ib, jb), "~=" === m[2] && (m[3] = " " + m[3] + " "), m.slice(0, 4)
                },
                CHILD: function(m) {
                    return m[1] = m[1].toLowerCase(), "nth" === m[1].slice(0, 3) ? (m[3] || b.error(m[0]), m[4] = +(m[4] ? m[5] + (m[6] || 1) : 2 * ("even" === m[3] || "odd" === m[3])), m[5] = +(m[7] + m[8] || "odd" === m[3])) : m[3] && b.error(m[0]), m
                },
                PSEUDO: function(m) {
                    var q, v = !m[6] && m[2];
                    return dc.CHILD.test(m[0]) ? null : (m[3] ? m[2] = m[4] || m[5] || "" : v && hd.test(v) && (q = bc(v, !0)) && (q = v.indexOf(")", v.length - q) - v.length) && (m[0] = m[0].slice(0,
                        q), m[2] = v.slice(0, q)), m.slice(0, 3))
                }
            },
            filter: {
                TAG: function(m) {
                    var q = m.replace(ib, jb).toLowerCase();
                    return "*" === m ? function() {
                        return !0
                    } : function(v) {
                        return v.nodeName && v.nodeName.toLowerCase() === q
                    }
                },
                CLASS: function(m) {
                    var q = Ia[m + " "];
                    return q || (q = new RegExp("(^|[\\x20\\t\\r\\n\\f])" + m + "([\\x20\\t\\r\\n\\f]|$)"), Ia(m, function(v) {
                        return q.test("string" == typeof v.className && v.className || "undefined" != typeof v.getAttribute && v.getAttribute("class") || "")
                    }))
                },
                ATTR: function(m, q, v) {
                    return function(x) {
                        x = b.attr(x,
                            m);
                        return null == x ? "!=" === q : !q || (x += "", "=" === q ? x === v : "!=" === q ? x !== v : "^=" === q ? v && 0 === x.indexOf(v) : "*=" === q ? v && -1 < x.indexOf(v) : "$=" === q ? v && x.slice(-v.length) === v : "~=" === q ? -1 < (" " + x.replace(dd, " ") + " ").indexOf(v) : "|=" === q && (x === v || x.slice(0, v.length + 1) === v + "-"))
                    }
                },
                CHILD: function(m, q, v, x, B) {
                    var E = "nth" !== m.slice(0, 3),
                        M = "last" !== m.slice(-4),
                        P = "of-type" === q;
                    return 1 === x && 0 === B ? function(J) {
                        return !!J.parentNode
                    } : function(J, R, S) {
                        var ma, na, T, la;
                        R = E !== M ? "nextSibling" : "previousSibling";
                        var ha = J.parentNode,
                            rb =
                            P && J.nodeName.toLowerCase(),
                            Za = !S && !P,
                            Ca = !1;
                        if (ha) {
                            if (E) {
                                for (; R;) {
                                    for (T = J; T = T[R];)
                                        if (P ? T.nodeName.toLowerCase() === rb : 1 === T.nodeType) return !1;
                                    var Ha = R = "only" === m && !Ha && "nextSibling"
                                }
                                return !0
                            }
                            if (Ha = [M ? ha.firstChild : ha.lastChild], M && Za)
                                for (Ca = (la = (ma = (S = (na = (T = ha)[ea] || (T[ea] = {}))[T.uniqueID] || (na[T.uniqueID] = {}))[m] || [])[0] === Pa && ma[1]) && ma[2], T = la && ha.childNodes[la]; T = ++la && T && T[R] || (Ca = la = 0) || Ha.pop();) {
                                    if (1 === T.nodeType && ++Ca && T === J) {
                                        S[m] = [Pa, la, Ca];
                                        break
                                    }
                                } else if (Za && (Ca = la = (ma = ((na = (T = J)[ea] ||
                                        (T[ea] = {}))[T.uniqueID] || (na[T.uniqueID] = {}))[m] || [])[0] === Pa && ma[1]), !1 === Ca)
                                    for (;
                                        (T = ++la && T && T[R] || (Ca = la = 0) || Ha.pop()) && ((P ? T.nodeName.toLowerCase() !== rb : 1 !== T.nodeType) || !++Ca || (Za && ((S = (na = T[ea] || (T[ea] = {}))[T.uniqueID] || (na[T.uniqueID] = {}))[m] = [Pa, Ca]), T !== J)););
                            return (Ca -= B) === x || 0 == Ca % x && 0 <= Ca / x
                        }
                    }
                },
                PSEUDO: function(m, q) {
                    var v, x = U.pseudos[m] || U.setFilters[m.toLowerCase()] || b.error("unsupported pseudo: " + m);
                    return x[ea] ? x(q) : 1 < x.length ? (v = [m, m, "", q], U.setFilters.hasOwnProperty(m.toLowerCase()) ?
                        f(function(B, E) {
                            for (var M, P = x(B, q), J = P.length; J--;) B[M = xb(B, P[J])] = !(E[M] = P[J])
                        }) : function(B) {
                            return x(B, 0, v)
                        }) : x
                }
            },
            pseudos: {
                not: f(function(m) {
                    var q = [],
                        v = [],
                        x = aa(m.replace(cc, "$1"));
                    return x[ea] ? f(function(B, E, M, P) {
                        var J;
                        M = x(B, null, P, []);
                        for (P = B.length; P--;)(J = M[P]) && (B[P] = !(E[P] = J))
                    }) : function(B, E, M) {
                        return q[0] = B, x(q, null, M, v), q[0] = null, !v.pop()
                    }
                }),
                has: f(function(m) {
                    return function(q) {
                        return 0 < b(m, q).length
                    }
                }),
                contains: f(function(m) {
                    return m = m.replace(ib, jb),
                        function(q) {
                            return -1 < (q.textContent ||
                                q.innerText || qc(q)).indexOf(m)
                        }
                }),
                lang: f(function(m) {
                    return id.test(m || "") || b.error("unsupported lang: " + m), m = m.replace(ib, jb).toLowerCase(),
                        function(q) {
                            var v;
                            do
                                if (v = qa ? q.lang : q.getAttribute("xml:lang") || q.getAttribute("lang")) return (v = v.toLowerCase()) === m || 0 === v.indexOf(m + "-"); while ((q = q.parentNode) && 1 === q.nodeType);
                            return !1
                        }
                }),
                target: function(m) {
                    var q = a.location && a.location.hash;
                    return q && q.slice(1) === m.id
                },
                root: function(m) {
                    return m === Ea
                },
                focus: function(m) {
                    return m === ba.activeElement && (!ba.hasFocus ||
                        ba.hasFocus()) && !!(m.type || m.href || ~m.tabIndex)
                },
                enabled: C(!1),
                disabled: C(!0),
                checked: function(m) {
                    var q = m.nodeName.toLowerCase();
                    return "input" === q && !!m.checked || "option" === q && !!m.selected
                },
                selected: function(m) {
                    return m.parentNode && m.parentNode.selectedIndex, !0 === m.selected
                },
                empty: function(m) {
                    for (m = m.firstChild; m; m = m.nextSibling)
                        if (6 > m.nodeType) return !1;
                    return !0
                },
                parent: function(m) {
                    return !U.pseudos.empty(m)
                },
                header: function(m) {
                    return kd.test(m.nodeName)
                },
                input: function(m) {
                    return jd.test(m.nodeName)
                },
                button: function(m) {
                    var q = m.nodeName.toLowerCase();
                    return "input" === q && "button" === m.type || "button" === q
                },
                text: function(m) {
                    var q;
                    return "input" === m.nodeName.toLowerCase() && "text" === m.type && (null == (q = m.getAttribute("type")) || "text" === q.toLowerCase())
                },
                first: y(function() {
                    return [0]
                }),
                last: y(function(m, q) {
                    return [q - 1]
                }),
                eq: y(function(m, q, v) {
                    return [0 > v ? v + q : v]
                }),
                even: y(function(m, q) {
                    for (var v = 0; v < q; v += 2) m.push(v);
                    return m
                }),
                odd: y(function(m, q) {
                    for (var v = 1; v < q; v += 2) m.push(v);
                    return m
                }),
                lt: y(function(m, q, v) {
                    for (q =
                        0 > v ? v + q : v; 0 <= --q;) m.push(q);
                    return m
                }),
                gt: y(function(m, q, v) {
                    for (v = 0 > v ? v + q : v; ++v < q;) m.push(v);
                    return m
                })
            }
        }).pseudos.nth = U.pseudos.eq;
        for (ua in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) U.pseudos[ua] = r(ua);
        for (ua in {
                submit: !0,
                reset: !0
            }) U.pseudos[ua] = u(ua);
        O.prototype = U.filters = U.pseudos;
        U.setFilters = new O;
        var bc = b.tokenize = function(m, q) {
            var v, x, B, E, M;
            if (E = sb[m + " "]) return q ? 0 : E.slice(0);
            E = m;
            var P = [];
            for (M = U.preFilter; E;) {
                J && !(v = ed.exec(E)) || (v && (E = E.slice(v[0].length) || E), P.push(x = []));
                var J = !1;
                (v = fd.exec(E)) && (J = v.shift(), x.push({
                    value: J,
                    type: v[0].replace(cc, " ")
                }), E = E.slice(J.length));
                for (B in U.filter) !(v = dc[B].exec(E)) || M[B] && !(v = M[B](v)) || (J = v.shift(), x.push({
                    value: J,
                    type: B,
                    matches: v
                }), E = E.slice(J.length));
                if (!J) break
            }
            return q ? E.length : E ? b.error(m) : sb(m, P).slice(0)
        };
        return aa = b.compile = function(m, q) {
            var v, x = [],
                B = [],
                E = Oa[m + " "];
            if (!E) {
                q || (q = bc(m));
                for (v = q.length; v--;)(E = za(q[v]))[ea] ? x.push(E) : B.push(E);
                (E = Oa(m, Z(B, x))).selector = m
            }
            return E
        }, wa = b.select = function(m, q, v, x) {
            var B, E, M,
                P, J, R = "function" == typeof m && m,
                S = !x && bc(m = R.selector || m);
            if (v = v || [], 1 === S.length) {
                if (2 < (E = S[0] = S[0].slice(0)).length && "ID" === (M = E[0]).type && 9 === q.nodeType && qa && U.relative[E[1].type]) {
                    if (!(q = (U.find.ID(M.matches[0].replace(ib, jb), q) || [])[0])) return v;
                    R && (q = q.parentNode);
                    m = m.slice(E.shift().value.length)
                }
                for (B = dc.needsContext.test(m) ? 0 : E.length; B-- && (M = E[B], !U.relative[P = M.type]);)
                    if ((J = U.find[P]) && (x = J(M.matches[0].replace(ib, jb), nc.test(E[0].type) && I(q.parentNode) || q))) {
                        if (E.splice(B, 1), !(m = x.length &&
                                W(E))) return qb.apply(v, x), v;
                        break
                    }
            }
            return (R || aa(m, S))(x, q, !qa, v, !q || nc.test(m) && I(q.parentNode) || q), v
        }, ra.sortStable = ea.split("").sort(ab).join("") === ea, ra.detectDuplicates = !!$a, wb(), ra.sortDetached = g(function(m) {
            return 1 & m.compareDocumentPosition(ba.createElement("fieldset"))
        }), g(function(m) {
            return m.innerHTML = "<a href='#'></a>", "#" === m.firstChild.getAttribute("href")
        }) || l("type|href|height|width", function(m, q, v) {
            if (!v) return m.getAttribute(q, "type" === q.toLowerCase() ? 1 : 2)
        }), ra.attributes && g(function(m) {
            return m.innerHTML =
                "<input/>", m.firstChild.setAttribute("value", ""), "" === m.firstChild.getAttribute("value")
        }) || l("value", function(m, q, v) {
            if (!v && "input" === m.nodeName.toLowerCase()) return m.defaultValue
        }), g(function(m) {
            return null == m.getAttribute("disabled")
        }) || l("checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", function(m, q, v) {
            var x;
            if (!v) return !0 === m[q] ? q.toLowerCase() : (x = m.getAttributeNode(q)) && x.specified ? x.value : null
        }), b
    }(L);
    e.find = Ma;
    e.expr =
        Ma.selectors;
    e.expr[":"] = e.expr.pseudos;
    e.uniqueSort = e.unique = Ma.uniqueSort;
    e.text = Ma.getText;
    e.isXMLDoc = Ma.isXML;
    e.contains = Ma.contains;
    e.escapeSelector = Ma.escape;
    var Qa = function(a, b, c) {
            for (var f = [], g = void 0 !== c;
                (a = a[b]) && 9 !== a.nodeType;)
                if (1 === a.nodeType) {
                    if (g && e(a).is(c)) break;
                    f.push(a)
                }
            return f
        },
        Bb = function(a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c
        },
        ec = e.expr.match.needsContext,
        fc = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
    e.filter = function(a,
        b, c) {
        var f = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === f.nodeType ? e.find.matchesSelector(f, a) ? [f] : [] : e.find.matches(a, e.grep(b, function(g) {
            return 1 === g.nodeType
        }))
    };
    e.fn.extend({
        find: function(a) {
            var b, c = this.length,
                f = this;
            if ("string" != typeof a) return this.pushStack(e(a).filter(function() {
                for (b = 0; b < c; b++)
                    if (e.contains(f[b], this)) return !0
            }));
            var g = this.pushStack([]);
            for (b = 0; b < c; b++) e.find(a, f[b], g);
            return 1 < c ? e.uniqueSort(g) : g
        },
        filter: function(a) {
            return this.pushStack(Y(this, a || [], !1))
        },
        not: function(a) {
            return this.pushStack(Y(this,
                a || [], !0))
        },
        is: function(a) {
            return !!Y(this, "string" == typeof a && ec.test(a) ? e(a) : a || [], !1).length
        }
    });
    var md = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (e.fn.init = function(a, b, c) {
        var f, g;
        if (!a) return this;
        if (c = c || nd, "string" == typeof a) {
            if (!(f = "<" === a[0] && ">" === a[a.length - 1] && 3 <= a.length ? [null, a, null] : md.exec(a)) || !f[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
            if (f[1]) {
                if (b = b instanceof e ? b[0] : b, e.merge(this, e.parseHTML(f[1], b && b.nodeType ? b.ownerDocument || b : X, !0)), fc.test(f[1]) && e.isPlainObject(b))
                    for (f in b) H(this[f]) ?
                        this[f](b[f]) : this.attr(f, b[f]);
                return this
            }
            return (g = X.getElementById(f[2])) && (this[0] = g, this.length = 1), this
        }
        return a.nodeType ? (this[0] = a, this.length = 1, this) : H(a) ? void 0 !== c.ready ? c.ready(a) : a(e) : e.makeArray(a, this)
    }).prototype = e.fn;
    var nd = e(X);
    var od = /^(?:parents|prev(?:Until|All))/,
        pd = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    e.fn.extend({
        has: function(a) {
            var b = e(a, this),
                c = b.length;
            return this.filter(function() {
                for (var f = 0; f < c; f++)
                    if (e.contains(this, b[f])) return !0
            })
        },
        closest: function(a, b) {
            var c,
                f = 0,
                g = this.length,
                l = [],
                n = "string" != typeof a && e(a);
            if (!ec.test(a))
                for (; f < g; f++)
                    for (c = this[f]; c && c !== b; c = c.parentNode)
                        if (11 > c.nodeType && (n ? -1 < n.index(c) : 1 === c.nodeType && e.find.matchesSelector(c, a))) {
                            l.push(c);
                            break
                        }
            return this.pushStack(1 < l.length ? e.uniqueSort(l) : l)
        },
        index: function(a) {
            return a ? "string" == typeof a ? w.call(e(a), this[0]) : w.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(a, b) {
            return this.pushStack(e.uniqueSort(e.merge(this.get(), e(a, b))))
        },
        addBack: function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    });
    e.each({
        parent: function(a) {
            return (a = a.parentNode) && 11 !== a.nodeType ? a : null
        },
        parents: function(a) {
            return Qa(a, "parentNode")
        },
        parentsUntil: function(a, b, c) {
            return Qa(a, "parentNode", c)
        },
        next: function(a) {
            return tb(a, "nextSibling")
        },
        prev: function(a) {
            return tb(a, "previousSibling")
        },
        nextAll: function(a) {
            return Qa(a, "nextSibling")
        },
        prevAll: function(a) {
            return Qa(a, "previousSibling")
        },
        nextUntil: function(a, b, c) {
            return Qa(a,
                "nextSibling", c)
        },
        prevUntil: function(a, b, c) {
            return Qa(a, "previousSibling", c)
        },
        siblings: function(a) {
            return Bb((a.parentNode || {}).firstChild, a)
        },
        children: function(a) {
            return Bb(a.firstChild)
        },
        contents: function(a) {
            return va(a, "iframe") ? a.contentDocument : (va(a, "template") && (a = a.content || a), e.merge([], a.childNodes))
        }
    }, function(a, b) {
        e.fn[a] = function(c, f) {
            var g = e.map(this, b, c);
            return "Until" !== a.slice(-5) && (f = c), f && "string" == typeof f && (g = e.filter(f, g)), 1 < this.length && (pd[a] || e.uniqueSort(g), od.test(a) && g.reverse()),
                this.pushStack(g)
        }
    });
    var Ya = /[^\x20\t\r\n\f]+/g;
    e.Callbacks = function(a) {
        a = "string" == typeof a ? z(a) : e.extend({}, a);
        var b, c, f, g, l = [],
            n = [],
            r = -1,
            u = function() {
                g = g || a.once;
                for (f = b = !0; n.length; r = -1)
                    for (c = n.shift(); ++r < l.length;) !1 === l[r].apply(c[0], c[1]) && a.stopOnFalse && (r = l.length, c = !1);
                a.memory || (c = !1);
                b = !1;
                g && (l = c ? [] : "")
            },
            C = {
                add: function() {
                    return l && (c && !b && (r = l.length - 1, n.push(c)), function O(I) {
                            e.each(I, function(W, Q) {
                                H(Q) ? a.unique && C.has(Q) || l.push(Q) : Q && Q.length && "string" !== Ja(Q) && O(Q)
                            })
                        }(arguments),
                        c && !b && u()), this
                },
                remove: function() {
                    return e.each(arguments, function(y, I) {
                        for (var O; - 1 < (O = e.inArray(I, l, O));) l.splice(O, 1), O <= r && r--
                    }), this
                },
                has: function(y) {
                    return y ? -1 < e.inArray(y, l) : 0 < l.length
                },
                empty: function() {
                    return l && (l = []), this
                },
                disable: function() {
                    return g = n = [], l = c = "", this
                },
                disabled: function() {
                    return !l
                },
                lock: function() {
                    return g = n = [], c || b || (l = c = ""), this
                },
                locked: function() {
                    return !!g
                },
                fireWith: function(y, I) {
                    return g || (I = [y, (I = I || []).slice ? I.slice() : I], n.push(I), b || u()), this
                },
                fire: function() {
                    return C.fireWith(this,
                        arguments), this
                },
                fired: function() {
                    return !!f
                }
            };
        return C
    };
    e.extend({
        Deferred: function(a) {
            var b = [
                    ["notify", "progress", e.Callbacks("memory"), e.Callbacks("memory"), 2],
                    ["resolve", "done", e.Callbacks("once memory"), e.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", e.Callbacks("once memory"), e.Callbacks("once memory"), 1, "rejected"]
                ],
                c = "pending",
                f = {
                    state: function() {
                        return c
                    },
                    always: function() {
                        return g.done(arguments).fail(arguments), this
                    },
                    "catch": function(l) {
                        return f.then(null, l)
                    },
                    pipe: function() {
                        var l =
                            arguments;
                        return e.Deferred(function(n) {
                            e.each(b, function(r, u) {
                                var C = H(l[u[4]]) && l[u[4]];
                                g[u[1]](function() {
                                    var y = C && C.apply(this, arguments);
                                    y && H(y.promise) ? y.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[u[0] + "With"](this, C ? [y] : arguments)
                                })
                            });
                            l = null
                        }).promise()
                    },
                    then: function(l, n, r) {
                        function u(y, I, O, W) {
                            return function() {
                                var Q = this,
                                    da = arguments,
                                    ya = function() {
                                        var za;
                                        if (!(y < C)) {
                                            if ((za = O.apply(Q, da)) === I.promise()) throw new TypeError("Thenable self-resolution");
                                            var Z = za && ("object" == typeof za ||
                                                "function" == typeof za) && za.then;
                                            H(Z) ? W ? Z.call(za, u(C, I, K, W), u(C, I, F, W)) : (C++, Z.call(za, u(C, I, K, W), u(C, I, F, W), u(C, I, K, I.notifyWith))) : (O !== K && (Q = void 0, da = [za]), (W || I.resolveWith)(Q, da))
                                        }
                                    },
                                    Xa = W ? ya : function() {
                                        try {
                                            ya()
                                        } catch (za) {
                                            e.Deferred.exceptionHook && e.Deferred.exceptionHook(za, Xa.stackTrace), y + 1 >= C && (O !== F && (Q = void 0, da = [za]), I.rejectWith(Q, da))
                                        }
                                    };
                                y ? Xa() : (e.Deferred.getStackHook && (Xa.stackTrace = e.Deferred.getStackHook()), L.setTimeout(Xa))
                            }
                        }
                        var C = 0;
                        return e.Deferred(function(y) {
                            b[0][3].add(u(0, y,
                                H(r) ? r : K, y.notifyWith));
                            b[1][3].add(u(0, y, H(l) ? l : K));
                            b[2][3].add(u(0, y, H(n) ? n : F))
                        }).promise()
                    },
                    promise: function(l) {
                        return null != l ? e.extend(l, f) : f
                    }
                },
                g = {};
            return e.each(b, function(l, n) {
                var r = n[2],
                    u = n[5];
                f[n[1]] = r.add;
                u && r.add(function() {
                    c = u
                }, b[3 - l][2].disable, b[3 - l][3].disable, b[0][2].lock, b[0][3].lock);
                r.add(n[3].fire);
                g[n[0]] = function() {
                    return g[n[0] + "With"](this === g ? void 0 : this, arguments), this
                };
                g[n[0] + "With"] = r.fireWith
            }), f.promise(g), a && a.call(g, g), g
        },
        when: function(a) {
            var b = arguments.length,
                c = b,
                f = Array(c),
                g = hb.call(arguments),
                l = e.Deferred(),
                n = function(r) {
                    return function(u) {
                        f[r] = this;
                        g[r] = 1 < arguments.length ? hb.call(arguments) : u;
                        --b || l.resolveWith(f, g)
                    }
                };
            if (1 >= b && (A(a, l.done(n(c)).resolve, l.reject, !b), "pending" === l.state() || H(g[c] && g[c].then))) return l.then();
            for (; c--;) A(g[c], n(c), l.reject);
            return l.promise()
        }
    });
    var qd = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    e.Deferred.exceptionHook = function(a, b) {
        L.console && L.console.warn && a && qd.test(a.name) && L.console.warn("jQuery.Deferred exception: " +
            a.message, a.stack, b)
    };
    e.readyException = function(a) {
        L.setTimeout(function() {
            throw a;
        })
    };
    var rc = e.Deferred();
    e.fn.ready = function(a) {
        return rc.then(a)["catch"](function(b) {
            e.readyException(b)
        }), this
    };
    e.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(a) {
            (!0 === a ? --e.readyWait : e.isReady) || (e.isReady = !0, !0 !== a && 0 < --e.readyWait || rc.resolveWith(X, [e]))
        }
    });
    e.ready.then = rc.then;
    "complete" === X.readyState || "loading" !== X.readyState && !X.documentElement.doScroll ? L.setTimeout(e.ready) : (X.addEventListener("DOMContentLoaded",
        N), L.addEventListener("load", N));
    var kb = function(a, b, c, f, g, l, n) {
            var r = 0,
                u = a.length,
                C = null == c;
            if ("object" === Ja(c))
                for (r in g = !0, c) kb(a, b, r, c[r], !0, l, n);
            else if (void 0 !== f && (g = !0, H(f) || (n = !0), C && (n ? (b.call(a, f), b = null) : (C = b, b = function(y, I, O) {
                    return C.call(e(y), O)
                })), b))
                for (; r < u; r++) b(a[r], c, n ? f : f.call(a[r], r, b(a[r], c)));
            return g ? a : C ? b.call(a) : u ? b(a[0], c) : l
        },
        Rc = /^-ms-/,
        Sc = /-([a-z])/g,
        gc = function(a) {
            return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType
        };
    Ba.uid = 1;
    Ba.prototype = {
        cache: function(a) {
            var b = a[this.expando];
            return b || (b = {}, gc(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, {
                value: b,
                configurable: !0
            }))), b
        },
        set: function(a, b, c) {
            var f;
            a = this.cache(a);
            if ("string" == typeof b) a[ja(b)] = c;
            else
                for (f in b) a[ja(f)] = b[f];
            return a
        },
        get: function(a, b) {
            return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][ja(b)]
        },
        access: function(a, b, c) {
            return void 0 === b || b && "string" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b)
        },
        remove: function(a, b) {
            var c = a[this.expando];
            if (void 0 !==
                c) {
                if (void 0 !== b) {
                    var f = (b = Array.isArray(b) ? b.map(ja) : (b = ja(b)) in c ? [b] : b.match(Ya) || []).length;
                    for (; f--;) delete c[b[f]]
                }(void 0 === b || e.isEmptyObject(c)) && (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando])
            }
        },
        hasData: function(a) {
            a = a[this.expando];
            return void 0 !== a && !e.isEmptyObject(a)
        }
    };
    var V = new Ba,
        Ka = new Ba,
        Uc = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Tc = /[A-Z]/g;
    e.extend({
        hasData: function(a) {
            return Ka.hasData(a) || V.hasData(a)
        },
        data: function(a, b, c) {
            return Ka.access(a, b, c)
        },
        removeData: function(a, b) {
            Ka.remove(a,
                b)
        },
        _data: function(a, b, c) {
            return V.access(a, b, c)
        },
        _removeData: function(a, b) {
            V.remove(a, b)
        }
    });
    e.fn.extend({
        data: function(a, b) {
            var c, f, g, l = this[0],
                n = l && l.attributes;
            if (void 0 === a) {
                if (this.length && (g = Ka.get(l), 1 === l.nodeType && !V.get(l, "hasDataAttrs"))) {
                    for (c = n.length; c--;) n[c] && 0 === (f = n[c].name).indexOf("data-") && (f = ja(f.slice(5)), Ra(l, f, g[f]));
                    V.set(l, "hasDataAttrs", !0)
                }
                return g
            }
            return "object" == typeof a ? this.each(function() {
                Ka.set(this, a)
            }) : kb(this, function(r) {
                var u;
                if (l && void 0 === r) {
                    if (void 0 !== (u =
                            Ka.get(l, a)) || void 0 !== (u = Ra(l, a))) return u
                } else this.each(function() {
                    Ka.set(this, a, r)
                })
            }, null, b, 1 < arguments.length, null, !0)
        },
        removeData: function(a) {
            return this.each(function() {
                Ka.remove(this, a)
            })
        }
    });
    e.extend({
        queue: function(a, b, c) {
            var f;
            if (a) return b = (b || "fx") + "queue", f = V.get(a, b), c && (!f || Array.isArray(c) ? f = V.access(a, b, e.makeArray(c)) : f.push(c)), f || []
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = e.queue(a, b),
                f = c.length,
                g = c.shift(),
                l = e._queueHooks(a, b),
                n = function() {
                    e.dequeue(a, b)
                };
            "inprogress" === g && (g =
                c.shift(), f--);
            g && ("fx" === b && c.unshift("inprogress"), delete l.stop, g.call(a, n, l));
            !f && l && l.empty.fire()
        },
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return V.get(a, c) || V.access(a, c, {
                empty: e.Callbacks("once memory").add(function() {
                    V.remove(a, [b + "queue", c])
                })
            })
        }
    });
    e.fn.extend({
        queue: function(a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? e.queue(this[0], a) : void 0 === b ? this : this.each(function() {
                var f = e.queue(this, a, b);
                e._queueHooks(this, a);
                "fx" === a && "inprogress" !== f[0] &&
                    e.dequeue(this, a)
            })
        },
        dequeue: function(a) {
            return this.each(function() {
                e.dequeue(this, a)
            })
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", [])
        },
        promise: function(a, b) {
            var c, f = 1,
                g = e.Deferred(),
                l = this,
                n = this.length,
                r = function() {
                    --f || g.resolveWith(l, [l])
                };
            "string" != typeof a && (b = a, a = void 0);
            for (a = a || "fx"; n--;)(c = V.get(l[n], a + "queueHooks")) && c.empty && (f++, c.empty.add(r));
            return r(), g.promise(b)
        }
    });
    var Ec = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Cb = new RegExp("^(?:([+-])=|)(" + Ec + ")([a-z%]*)$", "i"),
        cb = ["Top", "Right", "Bottom", "Left"],
        Pb = function(a, b) {
            return "none" === (a = b || a).style.display || "" === a.style.display && e.contains(a.ownerDocument, a) && "none" === e.css(a, "display")
        },
        Fc = function(a, b, c, f) {
            var g, l = {};
            for (g in b) l[g] = a.style[g], a.style[g] = b[g];
            c = c.apply(a, f || []);
            for (g in b) a.style[g] = l[g];
            return c
        },
        vc = {};
    e.fn.extend({
        show: function() {
            return lb(this, !0)
        },
        hide: function() {
            return lb(this)
        },
        toggle: function(a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function() {
                Pb(this) ? e(this).show() :
                    e(this).hide()
            })
        }
    });
    var Gc = /^(?:checkbox|radio)$/i,
        wc = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        xc = /^$|^module$|\/(?:java|ecma)script/i,
        Sa = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Sa.optgroup = Sa.option;
    Sa.tbody = Sa.tfoot = Sa.colgroup = Sa.caption = Sa.thead;
    Sa.th = Sa.td;
    var Vc = /<|&#?\w+;/;
    ! function() {
        var a =
            X.createDocumentFragment().appendChild(X.createElement("div")),
            b = X.createElement("input");
        b.setAttribute("type", "radio");
        b.setAttribute("checked", "checked");
        b.setAttribute("name", "t");
        a.appendChild(b);
        G.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked;
        a.innerHTML = "<textarea>x</textarea>";
        G.noCloneChecked = !!a.cloneNode(!0).lastChild.defaultValue
    }();
    var hc = X.documentElement,
        rd = /^key/,
        sd = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Hc = /^([^.]*)(?:\.(.+)|)/;
    e.event = {
        global: {},
        add: function(a,
            b, c, f, g) {
            var l, n, r, u, C, y, I, O, W;
            if (C = V.get(a))
                for (c.handler && (c = (l = c).handler, g = l.selector), g && e.find.matchesSelector(hc, g), c.guid || (c.guid = e.guid++), (u = C.events) || (u = C.events = {}), (n = C.handle) || (n = C.handle = function(da) {
                        return "undefined" != typeof e && e.event.triggered !== da.type ? e.event.dispatch.apply(a, arguments) : void 0
                    }), C = (b = (b || "").match(Ya) || [""]).length; C--;) {
                    var Q = W = (r = Hc.exec(b[C]) || [])[1];
                    r = (r[2] || "").split(".").sort();
                    Q && (I = e.event.special[Q] || {}, Q = (g ? I.delegateType : I.bindType) || Q, I = e.event.special[Q] || {}, y = e.extend({
                        type: Q,
                        origType: W,
                        data: f,
                        handler: c,
                        guid: c.guid,
                        selector: g,
                        needsContext: g && e.expr.match.needsContext.test(g),
                        namespace: r.join(".")
                    }, l), (O = u[Q]) || ((O = u[Q] = []).delegateCount = 0, I.setup && !1 !== I.setup.call(a, f, r, n) || a.addEventListener && a.addEventListener(Q, n)), I.add && (I.add.call(a, y), y.handler.guid || (y.handler.guid = c.guid)), g ? O.splice(O.delegateCount++, 0, y) : O.push(y), e.event.global[Q] = !0)
                }
        },
        remove: function(a, b, c, f, g) {
            var l, n, r, u, C, y, I, O, W = V.hasData(a) && V.get(a);
            if (W && (u = W.events)) {
                for (C =
                    (b = (b || "").match(Ya) || [""]).length; C--;)
                    if (r = Hc.exec(b[C]) || [], y = O = r[1], I = (r[2] || "").split(".").sort(), y) {
                        var Q = e.event.special[y] || {};
                        var da = u[y = (f ? Q.delegateType : Q.bindType) || y] || [];
                        r = r[2] && new RegExp("(^|\\.)" + I.join("\\.(?:.*\\.|)") + "(\\.|$)");
                        for (n = l = da.length; l--;) {
                            var ya = da[l];
                            !g && O !== ya.origType || c && c.guid !== ya.guid || r && !r.test(ya.namespace) || f && f !== ya.selector && ("**" !== f || !ya.selector) || (da.splice(l, 1), ya.selector && da.delegateCount--, Q.remove && Q.remove.call(a, ya))
                        }
                        n && !da.length && (Q.teardown &&
                            !1 !== Q.teardown.call(a, I, W.handle) || e.removeEvent(a, y, W.handle), delete u[y])
                    } else
                        for (y in u) e.event.remove(a, y + b[C], c, f, !0);
                e.isEmptyObject(u) && V.remove(a, "handle events")
            }
        },
        dispatch: function(a) {
            var b = e.event.fix(a),
                c, f, g, l, n = Array(arguments.length);
            var r = (V.get(this, "events") || {})[b.type] || [];
            var u = e.event.special[b.type] || {};
            n[0] = b;
            for (c = 1; c < arguments.length; c++) n[c] = arguments[c];
            if (b.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, b)) {
                var C = e.event.handlers.call(this, b, r);
                for (c =
                    0;
                    (g = C[c++]) && !b.isPropagationStopped();)
                    for (b.currentTarget = g.elem, r = 0;
                        (l = g.handlers[r++]) && !b.isImmediatePropagationStopped();) b.rnamespace && !b.rnamespace.test(l.namespace) || (b.handleObj = l, b.data = l.data, void 0 !== (f = ((e.event.special[l.origType] || {}).handle || l.handler).apply(g.elem, n)) && !1 === (b.result = f) && (b.preventDefault(), b.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, b), b.result
            }
        },
        handlers: function(a, b) {
            var c, f, g, l = [],
                n = b.delegateCount,
                r = a.target;
            if (n && r.nodeType && !("click" ===
                    a.type && 1 <= a.button))
                for (; r !== this; r = r.parentNode || this)
                    if (1 === r.nodeType && ("click" !== a.type || !0 !== r.disabled)) {
                        var u = [];
                        var C = {};
                        for (c = 0; c < n; c++) void 0 === C[g = (f = b[c]).selector + " "] && (C[g] = f.needsContext ? -1 < e(g, this).index(r) : e.find(g, this, null, [r]).length), C[g] && u.push(f);
                        u.length && l.push({
                            elem: r,
                            handlers: u
                        })
                    }
            return r = this, n < b.length && l.push({
                elem: r,
                handlers: b.slice(n)
            }), l
        },
        addProp: function(a, b) {
            Object.defineProperty(e.Event.prototype, a, {
                enumerable: !0,
                configurable: !0,
                get: H(b) ? function() {
                    if (this.originalEvent) return b(this.originalEvent)
                } : function() {
                    if (this.originalEvent) return this.originalEvent[a]
                },
                set: function(c) {
                    Object.defineProperty(this, a, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: c
                    })
                }
            })
        },
        fix: function(a) {
            return a[e.expando] ? a : new e.Event(a)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== Qb() && this.focus) return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === Qb() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click &&
                        va(this, "input")) return this.click(), !1
                },
                _default: function(a) {
                    return va(a.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
                }
            }
        }
    };
    e.removeEvent = function(a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c)
    };
    e.Event = function(a, b) {
        if (!(this instanceof e.Event)) return new e.Event(a, b);
        a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && !1 === a.returnValue ?
            zb : nb, this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a;
        b && e.extend(this, b);
        this.timeStamp = a && a.timeStamp || Date.now();
        this[e.expando] = !0
    };
    e.Event.prototype = {
        constructor: e.Event,
        isDefaultPrevented: nb,
        isPropagationStopped: nb,
        isImmediatePropagationStopped: nb,
        isSimulated: !1,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = zb;
            a && !this.isSimulated && a.preventDefault()
        },
        stopPropagation: function() {
            var a =
                this.originalEvent;
            this.isPropagationStopped = zb;
            a && !this.isSimulated && a.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = zb;
            a && !this.isSimulated && a.stopImmediatePropagation();
            this.stopPropagation()
        }
    };
    e.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(a) {
            var b = a.button;
            return null == a.which && rd.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && sd.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which
        }
    }, e.event.addProp);
    e.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        e.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(c) {
                var f, g = c.relatedTarget,
                    l = c.handleObj;
                return g && (g === this || e.contains(this, g)) || (c.type = l.origType, f = l.handler.apply(this, arguments), c.type = b), f
            }
        }
    });
    e.fn.extend({
        on: function(a, b, c, f) {
            return Eb(this, a, b, c, f)
        },
        one: function(a, b, c, f) {
            return Eb(this, a, b, c, f, 1)
        },
        off: function(a, b, c) {
            var f, g;
            if (a && a.preventDefault && a.handleObj) return f = a.handleObj, e(a.delegateTarget).off(f.namespace ? f.origType + "." + f.namespace : f.origType, f.selector, f.handler), this;
            if ("object" == typeof a) {
                for (g in a) this.off(g, b, a[g]);
                return this
            }
            return !1 !== b && "function" !=
                typeof b || (c = b, b = void 0), !1 === c && (c = nb), this.each(function() {
                    e.event.remove(this, a, c, b)
                })
        }
    });
    var td = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        ud = /<script|<style|<link/i,
        Wc = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Xc = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    e.extend({
        htmlPrefilter: function(a) {
            return a.replace(td, "<$1></$2>")
        },
        clone: function(a, b, c) {
            var f, g, l = a.cloneNode(!0),
                n = e.contains(a.ownerDocument, a);
            if (!(G.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType ||
                    e.isXMLDoc(a))) {
                var r = xa(l);
                var u = 0;
                for (f = (g = xa(a)).length; u < f; u++) {
                    var C = g[u],
                        y = r[u],
                        I = y.nodeName.toLowerCase();
                    "input" === I && Gc.test(C.type) ? y.checked = C.checked : "input" !== I && "textarea" !== I || (y.defaultValue = C.defaultValue)
                }
            }
            if (b)
                if (c)
                    for (g = g || xa(a), r = r || xa(l), u = 0, f = g.length; u < f; u++) Rb(g[u], r[u]);
                else Rb(a, l);
            return 0 < (r = xa(l, "script")).length && Db(r, !n && xa(a, "script")), l
        },
        cleanData: function(a) {
            for (var b, c, f, g = e.event.special, l = 0; void 0 !== (c = a[l]); l++)
                if (gc(c)) {
                    if (b = c[V.expando]) {
                        if (b.events)
                            for (f in b.events) g[f] ?
                                e.event.remove(c, f) : e.removeEvent(c, f, b.handle);
                        c[V.expando] = void 0
                    }
                    c[Ka.expando] && (c[Ka.expando] = void 0)
                }
        }
    });
    e.fn.extend({
        detach: function(a) {
            return Sb(this, a, !0)
        },
        remove: function(a) {
            return Sb(this, a)
        },
        text: function(a) {
            return kb(this, function(b) {
                return void 0 === b ? e.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = b)
                })
            }, null, a, arguments.length)
        },
        append: function() {
            return Ta(this, arguments, function(a) {
                1 !== this.nodeType && 11 !== this.nodeType &&
                    9 !== this.nodeType || Va(this, a).appendChild(a)
            })
        },
        prepend: function() {
            return Ta(this, arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Va(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        },
        before: function() {
            return Ta(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        },
        after: function() {
            return Ta(this, arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        },
        empty: function() {
            for (var a, b = 0; null != (a = this[b]); b++) 1 ===
                a.nodeType && (e.cleanData(xa(a, !1)), a.textContent = "");
            return this
        },
        clone: function(a, b) {
            return a = null != a && a, b = null == b ? a : b, this.map(function() {
                return e.clone(this, a, b)
            })
        },
        html: function(a) {
            return kb(this, function(b) {
                var c = this[0] || {},
                    f = 0,
                    g = this.length;
                if (void 0 === b && 1 === c.nodeType) return c.innerHTML;
                if ("string" == typeof b && !ud.test(b) && !Sa[(wc.exec(b) || ["", ""])[1].toLowerCase()]) {
                    b = e.htmlPrefilter(b);
                    try {
                        for (; f < g; f++) 1 === (c = this[f] || {}).nodeType && (e.cleanData(xa(c, !1)), c.innerHTML = b);
                        c = 0
                    } catch (l) {}
                }
                c &&
                    this.empty().append(b)
            }, null, a, arguments.length)
        },
        replaceWith: function() {
            var a = [];
            return Ta(this, arguments, function(b) {
                var c = this.parentNode;
                0 > e.inArray(this, a) && (e.cleanData(xa(this)), c && c.replaceChild(b, this))
            }, a)
        }
    });
    e.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        e.fn[a] = function(c) {
            for (var f = [], g = e(c), l = g.length - 1, n = 0; n <= l; n++) c = n === l ? this : this.clone(!0), e(g[n])[b](c), pb.apply(f, c.get());
            return this.pushStack(f)
        }
    });
    var jc = new RegExp("^(" + Ec + ")(?!px)[a-z%]+$", "i"),
        Tb = function(a) {
            var b = a.ownerDocument.defaultView;
            return b && b.opener || (b = L), b.getComputedStyle(a)
        },
        Yc = new RegExp(cb.join("|"), "i");
    ! function() {
        function a() {
            if (r) {
                n.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0";
                r.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%";
                hc.appendChild(n).appendChild(r);
                var u = L.getComputedStyle(r);
                b = "1%" !== u.top;
                l = 12 === Math.round(parseFloat(u.marginLeft));
                r.style.right = "60%";
                g = 36 === Math.round(parseFloat(u.right));
                c = 36 === Math.round(parseFloat(u.width));
                r.style.position = "absolute";
                f = 36 === r.offsetWidth || "absolute";
                hc.removeChild(n);
                r = null
            }
        }
        var b, c, f, g, l, n = X.createElement("div"),
            r = X.createElement("div");
        r.style && (r.style.backgroundClip = "content-box", r.cloneNode(!0).style.backgroundClip = "", G.clearCloneStyle = "content-box" === r.style.backgroundClip, e.extend(G, {
            boxSizingReliable: function() {
                return a(),
                    c
            },
            pixelBoxStyles: function() {
                return a(), g
            },
            pixelPosition: function() {
                return a(), b
            },
            reliableMarginLeft: function() {
                return a(), l
            },
            scrollboxSize: function() {
                return a(), f
            }
        }))
    }();
    var vd = /^(none|table(?!-c[ea]).+)/,
        Ic = /^--/,
        wd = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Jc = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        zc = ["Webkit", "Moz", "ms"],
        yc = X.createElement("div").style;
    e.extend({
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) return a = ub(a, "opacity"), "" === a ? "1" : a
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {},
        style: function(a, b, c, f) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var g, l, n, r = ja(b),
                    u = Ic.test(b),
                    C = a.style;
                if (u || (b = Vb(r)), n = e.cssHooks[b] || e.cssHooks[r], void 0 === c) return n && "get" in n && void 0 !== (g = n.get(a, !1, f)) ? g : C[b];
                "string" == (l = typeof c) && (g = Cb.exec(c)) && g[1] && (c = bb(a, b, g), l = "number");
                null != c && c === c && ("number" === l && (c += g && g[3] || (e.cssNumber[r] ?
                    "" : "px")), G.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (C[b] = "inherit"), n && "set" in n && void 0 === (c = n.set(a, c, f)) || (u ? C.setProperty(b, c) : C[b] = c))
            }
        },
        css: function(a, b, c, f) {
            var g, l, n, r = ja(b);
            return Ic.test(b) || (b = Vb(r)), (n = e.cssHooks[b] || e.cssHooks[r]) && "get" in n && (g = n.get(a, !0, c)), void 0 === g && (g = ub(a, b, f)), "normal" === g && b in Jc && (g = Jc[b]), "" === c || c ? (l = parseFloat(g), !0 === c || isFinite(l) ? l || 0 : g) : g
        }
    });
    e.each(["height", "width"], function(a, b) {
        e.cssHooks[b] = {
            get: function(c, f, g) {
                if (f) return !vd.test(e.css(c,
                    "display")) || c.getClientRects().length && c.getBoundingClientRect().width ? Xb(c, b, g) : Fc(c, wd, function() {
                    return Xb(c, b, g)
                })
            },
            set: function(c, f, g) {
                var l, n = Tb(c),
                    r = "border-box" === e.css(c, "boxSizing", !1, n);
                g = g && Hb(c, b, g, r, n);
                return r && G.scrollboxSize() === n.position && (g -= Math.ceil(c["offset" + b[0].toUpperCase() + b.slice(1)] - parseFloat(n[b]) - Hb(c, b, "border", !1, n) - .5)), g && (l = Cb.exec(f)) && "px" !== (l[3] || "px") && (c.style[b] = f, f = e.css(c, b)), Wb(c, f, g)
            }
        }
    });
    e.cssHooks.marginLeft = Ub(G.reliableMarginLeft, function(a, b) {
        if (b) return (parseFloat(ub(a,
            "marginLeft")) || a.getBoundingClientRect().left - Fc(a, {
            marginLeft: 0
        }, function() {
            return a.getBoundingClientRect().left
        })) + "px"
    });
    e.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        e.cssHooks[a + b] = {
            expand: function(c) {
                var f = 0,
                    g = {};
                for (c = "string" == typeof c ? c.split(" ") : [c]; 4 > f; f++) g[a + cb[f] + b] = c[f] || c[f - 2] || c[0];
                return g
            }
        };
        "margin" !== a && (e.cssHooks[a + b].set = Wb)
    });
    e.fn.extend({
        css: function(a, b) {
            return kb(this, function(c, f, g) {
                var l, n = {},
                    r = 0;
                if (Array.isArray(f)) {
                    g = Tb(c);
                    for (l = f.length; r < l; r++) n[f[r]] =
                        e.css(c, f[r], !1, g);
                    return n
                }
                return void 0 !== g ? e.style(c, f, g) : e.css(c, f)
            }, a, b, 1 < arguments.length)
        }
    });
    e.Tween = Fa;
    Fa.prototype = {
        constructor: Fa,
        init: function(a, b, c, f, g, l) {
            this.elem = a;
            this.prop = c;
            this.easing = g || e.easing._default;
            this.options = b;
            this.start = this.now = this.cur();
            this.end = f;
            this.unit = l || (e.cssNumber[c] ? "" : "px")
        },
        cur: function() {
            var a = Fa.propHooks[this.prop];
            return a && a.get ? a.get(this) : Fa.propHooks._default.get(this)
        },
        run: function(a) {
            var b, c = Fa.propHooks[this.prop];
            return this.options.duration ?
                this.pos = b = e.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : Fa.propHooks._default.set(this), this
        }
    };
    Fa.prototype.init.prototype = Fa.prototype;
    Fa.propHooks = {
        _default: {
            get: function(a) {
                var b;
                return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = e.css(a.elem, a.prop, "")) && "auto" !== b ? b : 0
            },
            set: function(a) {
                e.fx.step[a.prop] ?
                    e.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[e.cssProps[a.prop]] && !e.cssHooks[a.prop] ? a.elem[a.prop] = a.now : e.style(a.elem, a.prop, a.now + a.unit)
            }
        }
    };
    Fa.propHooks.scrollTop = Fa.propHooks.scrollLeft = {
        set: function(a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    };
    e.easing = {
        linear: function(a) {
            return a
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2
        },
        _default: "swing"
    };
    e.fx = Fa.prototype.init;
    e.fx.step = {};
    var Ab, Yb, xd = /^(?:toggle|show|hide)$/,
        yd = /queueHooks$/;
    e.Animation = e.extend(Na, {
        tweeners: {
            "*": [function(a, b) {
                var c = this.createTween(a, b);
                return bb(c.elem, a, Cb.exec(b), c), c
            }]
        },
        tweener: function(a, b) {
            H(a) ? (b = a, a = ["*"]) : a = a.match(Ya);
            for (var c, f = 0, g = a.length; f < g; f++) c = a[f], Na.tweeners[c] = Na.tweeners[c] || [], Na.tweeners[c].unshift(b)
        },
        prefilters: [function(a, b, c) {
            var f, g, l, n, r, u, C, y = "width" in b || "height" in b,
                I = this,
                O = {},
                W = a.style,
                Q = a.nodeType && Pb(a),
                da = V.get(a, "fxshow");
            c.queue || (null == (n = e._queueHooks(a, "fx")).unqueued && (n.unqueued = 0, r = n.empty.fire, n.empty.fire = function() {
                n.unqueued ||
                    r()
            }), n.unqueued++, I.always(function() {
                I.always(function() {
                    n.unqueued--;
                    e.queue(a, "fx").length || n.empty.fire()
                })
            }));
            for (f in b)
                if (g = b[f], xd.test(g)) {
                    if (delete b[f], l = l || "toggle" === g, g === (Q ? "hide" : "show")) {
                        if ("show" !== g || !da || void 0 === da[f]) continue;
                        Q = !0
                    }
                    O[f] = da && da[f] || e.style(a, f)
                }
            if ((b = !e.isEmptyObject(b)) || !e.isEmptyObject(O))
                for (f in y && 1 === a.nodeType && (c.overflow = [W.overflow, W.overflowX, W.overflowY], null == (u = da && da.display) && (u = V.get(a, "display")), "none" === (C = e.css(a, "display")) && (u ? C = u : (lb([a], !0), u = a.style.display || u, C = e.css(a, "display"), lb([a]))), ("inline" === C || "inline-block" === C && null != u) && "none" === e.css(a, "float") && (b || (I.done(function() {
                        W.display = u
                    }), null == u && (C = W.display, u = "none" === C ? "" : C)), W.display = "inline-block")), c.overflow && (W.overflow = "hidden", I.always(function() {
                        W.overflow = c.overflow[0];
                        W.overflowX = c.overflow[1];
                        W.overflowY = c.overflow[2]
                    })), b = !1, O) b || (da ? "hidden" in da && (Q = da.hidden) : da = V.access(a, "fxshow", {
                    display: u
                }), l && (da.hidden = !Q), Q && lb([a], !0), I.done(function() {
                    Q || lb([a]);
                    V.remove(a, "fxshow");
                    for (f in O) e.style(a, f, O[f])
                })), b = $b(Q ? da[f] : 0, f, I), f in da || (da[f] = b.start, Q && (b.end = b.start, b.start = 0))
        }],
        prefilter: function(a, b) {
            b ? Na.prefilters.unshift(a) : Na.prefilters.push(a)
        }
    });
    e.speed = function(a, b, c) {
        var f = a && "object" == typeof a ? e.extend({}, a) : {
            complete: c || !c && b || H(a) && a,
            duration: a,
            easing: c && b || b && !H(b) && b
        };
        return e.fx.off ? f.duration = 0 : "number" != typeof f.duration && (f.duration in e.fx.speeds ? f.duration = e.fx.speeds[f.duration] : f.duration = e.fx.speeds._default), null != f.queue &&
            !0 !== f.queue || (f.queue = "fx"), f.old = f.complete, f.complete = function() {
                H(f.old) && f.old.call(this);
                f.queue && e.dequeue(this, f.queue)
            }, f
    };
    e.fn.extend({
        fadeTo: function(a, b, c, f) {
            return this.filter(Pb).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, f)
        },
        animate: function(a, b, c, f) {
            var g = e.isEmptyObject(a),
                l = e.speed(b, c, f);
            b = function() {
                var n = Na(this, e.extend({}, a), l);
                (g || V.get(this, "finish")) && n.stop(!0)
            };
            return b.finish = b, g || !1 === l.queue ? this.each(b) : this.queue(l.queue, b)
        },
        stop: function(a, b, c) {
            var f = function(g) {
                var l =
                    g.stop;
                delete g.stop;
                l(c)
            };
            return "string" != typeof a && (c = b, b = a, a = void 0), b && !1 !== a && this.queue(a || "fx", []), this.each(function() {
                var g = !0,
                    l = null != a && a + "queueHooks",
                    n = e.timers,
                    r = V.get(this);
                if (l) r[l] && r[l].stop && f(r[l]);
                else
                    for (l in r) r[l] && r[l].stop && yd.test(l) && f(r[l]);
                for (l = n.length; l--;) n[l].elem !== this || null != a && n[l].queue !== a || (n[l].anim.stop(c), g = !1, n.splice(l, 1));
                !g && c || e.dequeue(this, a)
            })
        },
        finish: function(a) {
            return !1 !== a && (a = a || "fx"), this.each(function() {
                var b = V.get(this),
                    c = b[a + "queue"];
                var f =
                    b[a + "queueHooks"];
                var g = e.timers,
                    l = c ? c.length : 0;
                b.finish = !0;
                e.queue(this, a, []);
                f && f.stop && f.stop.call(this, !0);
                for (f = g.length; f--;) g[f].elem === this && g[f].queue === a && (g[f].anim.stop(!0), g.splice(f, 1));
                for (f = 0; f < l; f++) c[f] && c[f].finish && c[f].finish.call(this);
                delete b.finish
            })
        }
    });
    e.each(["toggle", "show", "hide"], function(a, b) {
        var c = e.fn[b];
        e.fn[b] = function(f, g, l) {
            return null == f || "boolean" == typeof f ? c.apply(this, arguments) : this.animate(Ua(b, !0), f, g, l)
        }
    });
    e.each({
        slideDown: Ua("show"),
        slideUp: Ua("hide"),
        slideToggle: Ua("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(a, b) {
        e.fn[a] = function(c, f, g) {
            return this.animate(b, c, f, g)
        }
    });
    e.timers = [];
    e.fx.tick = function() {
        var a, b = 0,
            c = e.timers;
        for (Ab = Date.now(); b < c.length; b++)(a = c[b])() || c[b] !== a || c.splice(b--, 1);
        c.length || e.fx.stop();
        Ab = void 0
    };
    e.fx.timer = function(a) {
        e.timers.push(a);
        e.fx.start()
    };
    e.fx.interval = 13;
    e.fx.start = function() {
        Yb || (Yb = !0, Ib())
    };
    e.fx.stop = function() {
        Yb = null
    };
    e.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    };
    e.fn.delay = function(a, b) {
        return a = e.fx ? e.fx.speeds[a] || a : a, b = b || "fx", this.queue(b, function(c, f) {
            var g = L.setTimeout(c, a);
            f.stop = function() {
                L.clearTimeout(g)
            }
        })
    };
    (function() {
        var a = X.createElement("input"),
            b = X.createElement("select").appendChild(X.createElement("option"));
        a.type = "checkbox";
        G.checkOn = "" !== a.value;
        G.optSelected = b.selected;
        (a = X.createElement("input")).value = "t";
        a.type = "radio";
        G.radioValue = "t" === a.value
    })();
    var Mb = e.expr.attrHandle;
    e.fn.extend({
        attr: function(a, b) {
            return kb(this,
                e.attr, a, b, 1 < arguments.length)
        },
        removeAttr: function(a) {
            return this.each(function() {
                e.removeAttr(this, a)
            })
        }
    });
    e.extend({
        attr: function(a, b, c) {
            var f, g, l = a.nodeType;
            if (3 !== l && 8 !== l && 2 !== l) return "undefined" == typeof a.getAttribute ? e.prop(a, b, c) : (1 === l && e.isXMLDoc(a) || (g = e.attrHooks[b.toLowerCase()] || (e.expr.match.bool.test(b) ? zd : void 0)), void 0 !== c ? null === c ? void e.removeAttr(a, b) : g && "set" in g && void 0 !== (f = g.set(a, c, b)) ? f : (a.setAttribute(b, c + ""), c) : g && "get" in g && null !== (f = g.get(a, b)) ? f : null == (f = e.find.attr(a,
                b)) ? void 0 : f)
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!G.radioValue && "radio" === b && va(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        },
        removeAttr: function(a, b) {
            var c = 0,
                f = b && b.match(Ya);
            if (f && 1 === a.nodeType)
                for (; b = f[c++];) a.removeAttribute(b)
        }
    });
    var zd = {
        set: function(a, b, c) {
            return !1 === b ? e.removeAttr(a, c) : a.setAttribute(c, c), c
        }
    };
    e.each(e.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = Mb[b] || e.find.attr;
        Mb[b] = function(f, g, l) {
            var n, r, u = g.toLowerCase();
            return l || (r =
                Mb[u], Mb[u] = n, n = null != c(f, g, l) ? u : null, Mb[u] = r), n
        }
    });
    var Ad = /^(?:input|select|textarea|button)$/i,
        Bd = /^(?:a|area)$/i;
    e.fn.extend({
        prop: function(a, b) {
            return kb(this, e.prop, a, b, 1 < arguments.length)
        },
        removeProp: function(a) {
            return this.each(function() {
                delete this[e.propFix[a] || a]
            })
        }
    });
    e.extend({
        prop: function(a, b, c) {
            var f, g, l = a.nodeType;
            if (3 !== l && 8 !== l && 2 !== l) return 1 === l && e.isXMLDoc(a) || (b = e.propFix[b] || b, g = e.propHooks[b]), void 0 !== c ? g && "set" in g && void 0 !== (f = g.set(a, c, b)) ? f : a[b] = c : g && "get" in g && null !==
                (f = g.get(a, b)) ? f : a[b]
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    var b = e.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : Ad.test(a.nodeName) || Bd.test(a.nodeName) && a.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    });
    G.optSelected || (e.propHooks.selected = {
        get: function(a) {
            a = a.parentNode;
            return a && a.parentNode && a.parentNode.selectedIndex, null
        },
        set: function(a) {
            a = a.parentNode;
            a && (a.selectedIndex, a.parentNode && a.parentNode.selectedIndex)
        }
    });
    e.each("tabIndex readOnly maxLength cellSpacing cellPadding rowSpan colSpan useMap frameBorder contentEditable".split(" "),
        function() {
            e.propFix[this.toLowerCase()] = this
        });
    e.fn.extend({
        addClass: function(a) {
            var b, c, f, g, l, n, r, u = 0;
            if (H(a)) return this.each(function(C) {
                e(this).addClass(a.call(this, C, eb(this)))
            });
            if ((b = ob(a)).length)
                for (; c = this[u++];)
                    if (g = eb(c), f = 1 === c.nodeType && " " + db(g) + " ") {
                        for (n = 0; l = b[n++];) 0 > f.indexOf(" " + l + " ") && (f += l + " ");
                        g !== (r = db(f)) && c.setAttribute("class", r)
                    }
            return this
        },
        removeClass: function(a) {
            var b, c, f, g, l, n, r, u = 0;
            if (H(a)) return this.each(function(C) {
                e(this).removeClass(a.call(this, C, eb(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ((b = ob(a)).length)
                for (; c = this[u++];)
                    if (g = eb(c), f = 1 === c.nodeType && " " + db(g) + " ") {
                        for (n = 0; l = b[n++];)
                            for (; - 1 < f.indexOf(" " + l + " ");) f = f.replace(" " + l + " ", " ");
                        g !== (r = db(f)) && c.setAttribute("class", r)
                    }
            return this
        },
        toggleClass: function(a, b) {
            var c = typeof a,
                f = "string" === c || Array.isArray(a);
            return "boolean" == typeof b && f ? b ? this.addClass(a) : this.removeClass(a) : H(a) ? this.each(function(g) {
                e(this).toggleClass(a.call(this, g, eb(this), b), b)
            }) : this.each(function() {
                var g,
                    l;
                if (f) {
                    var n = 0;
                    var r = e(this);
                    for (l = ob(a); g = l[n++];) r.hasClass(g) ? r.removeClass(g) : r.addClass(g)
                } else void 0 !== a && "boolean" !== c || ((g = eb(this)) && V.set(this, "__className__", g), this.setAttribute && this.setAttribute("class", g || !1 === a ? "" : V.get(this, "__className__") || ""))
            })
        },
        hasClass: function(a) {
            var b, c = 0;
            for (a = " " + a + " "; b = this[c++];)
                if (1 === b.nodeType && -1 < (" " + db(eb(b)) + " ").indexOf(a)) return !0;
            return !1
        }
    });
    var Cd = /\r/g;
    e.fn.extend({
        val: function(a) {
            var b, c, f, g = this[0];
            if (arguments.length) return f = H(a), this.each(function(l) {
                var n;
                1 === this.nodeType && (null == (n = f ? a.call(this, l, e(this).val()) : a) ? n = "" : "number" == typeof n ? n += "" : Array.isArray(n) && (n = e.map(n, function(r) {
                    return null == r ? "" : r + ""
                })), (b = e.valHooks[this.type] || e.valHooks[this.nodeName.toLowerCase()]) && "set" in b && void 0 !== b.set(this, n, "value") || (this.value = n))
            });
            if (g) return (b = e.valHooks[g.type] || e.valHooks[g.nodeName.toLowerCase()]) && "get" in b && void 0 !== (c = b.get(g, "value")) ? c : "string" == typeof(c = g.value) ? c.replace(Cd, "") : null == c ? "" : c
        }
    });
    e.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b =
                        e.find.attr(a, "value");
                    return null != b ? b : db(e.text(a))
                }
            },
            select: {
                get: function(a) {
                    var b, c, f = a.options,
                        g = a.selectedIndex,
                        l = "select-one" === a.type,
                        n = l ? null : [],
                        r = l ? g + 1 : f.length;
                    for (c = 0 > g ? r : l ? g : 0; c < r; c++)
                        if (!(!(b = f[c]).selected && c !== g || b.disabled || b.parentNode.disabled && va(b.parentNode, "optgroup"))) {
                            if (a = e(b).val(), l) return a;
                            n.push(a)
                        }
                    return n
                },
                set: function(a, b) {
                    var c, f, g = a.options;
                    b = e.makeArray(b);
                    for (var l = g.length; l--;)((f = g[l]).selected = -1 < e.inArray(e.valHooks.option.get(f), b)) && (c = !0);
                    return c || (a.selectedIndex = -1), b
                }
            }
        }
    });
    e.each(["radio", "checkbox"], function() {
        e.valHooks[this] = {
            set: function(a, b) {
                if (Array.isArray(b)) return a.checked = -1 < e.inArray(e(a).val(), b)
            }
        };
        G.checkOn || (e.valHooks[this].get = function(a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    G.focusin = "onfocusin" in L;
    var Kc = /^(?:focusinfocus|focusoutblur)$/,
        Lc = function(a) {
            a.stopPropagation()
        };
    e.extend(e.event, {
        trigger: function(a, b, c, f) {
            var g, l, n, r, u, C, y = [c || X],
                I = h.call(a, "type") ? a.type : a;
            var O = h.call(a, "namespace") ? a.namespace.split(".") : [];
            if (g = C = l = c = c || X, 3 !== c.nodeType && 8 !== c.nodeType && !Kc.test(I + e.event.triggered) && (-1 < I.indexOf(".") && (I = (O = I.split(".")).shift(), O.sort()), n = 0 > I.indexOf(":") && "on" + I, a = a[e.expando] ? a : new e.Event(I, "object" == typeof a && a), a.isTrigger = f ? 2 : 3, a.namespace = O.join("."), a.rnamespace = a.namespace ? new RegExp("(^|\\.)" + O.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, a.result = void 0, a.target || (a.target = c), b = null == b ? [a] : e.makeArray(b, [a]), u = e.event.special[I] || {}, f || !u.trigger || !1 !== u.trigger.apply(c, b))) {
                if (!f && !u.noBubble &&
                    !ia(c)) {
                    var W = u.delegateType || I;
                    for (Kc.test(W + I) || (g = g.parentNode); g; g = g.parentNode) y.push(g), l = g;
                    l === (c.ownerDocument || X) && y.push(l.defaultView || l.parentWindow || L)
                }
                for (O = 0;
                    (g = y[O++]) && !a.isPropagationStopped();) C = g, a.type = 1 < O ? W : u.bindType || I, (r = (V.get(g, "events") || {})[a.type] && V.get(g, "handle")) && r.apply(g, b), (r = n && g[n]) && r.apply && gc(g) && (a.result = r.apply(g, b), !1 === a.result && a.preventDefault());
                return a.type = I, f || a.isDefaultPrevented() || u._default && !1 !== u._default.apply(y.pop(), b) || !gc(c) || n && H(c[I]) &&
                    !ia(c) && ((l = c[n]) && (c[n] = null), e.event.triggered = I, a.isPropagationStopped() && C.addEventListener(I, Lc), c[I](), a.isPropagationStopped() && C.removeEventListener(I, Lc), e.event.triggered = void 0, l && (c[n] = l)), a.result
            }
        },
        simulate: function(a, b, c) {
            a = e.extend(new e.Event, c, {
                type: a,
                isSimulated: !0
            });
            e.event.trigger(a, null, b)
        }
    });
    e.fn.extend({
        trigger: function(a, b) {
            return this.each(function() {
                e.event.trigger(a, b, this)
            })
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            if (c) return e.event.trigger(a, b, c, !0)
        }
    });
    G.focusin ||
        e.each({
            focus: "focusin",
            blur: "focusout"
        }, function(a, b) {
            var c = function(f) {
                e.event.simulate(b, f.target, e.event.fix(f))
            };
            e.event.special[b] = {
                setup: function() {
                    var f = this.ownerDocument || this,
                        g = V.access(f, b);
                    g || f.addEventListener(a, c, !0);
                    V.access(f, b, (g || 0) + 1)
                },
                teardown: function() {
                    var f = this.ownerDocument || this,
                        g = V.access(f, b) - 1;
                    g ? V.access(f, b, g) : (f.removeEventListener(a, c, !0), V.remove(f, b))
                }
            }
        });
    var Nb = L.location,
        Mc = Date.now(),
        sc = /\?/;
    e.parseXML = function(a) {
        if (!a || "string" != typeof a) return null;
        try {
            var b =
                (new L.DOMParser).parseFromString(a, "text/xml")
        } catch (c) {
            b = void 0
        }
        return b && !b.getElementsByTagName("parsererror").length || e.error("Invalid XML: " + a), b
    };
    var Zc = /\[\]$/,
        Nc = /\r?\n/g,
        Dd = /^(?:submit|button|image|reset|file)$/i,
        Ed = /^(?:input|select|textarea|keygen)/i;
    e.param = function(a, b) {
        var c, f = [],
            g = function(l, n) {
                n = H(n) ? n() : n;
                f[f.length] = encodeURIComponent(l) + "=" + encodeURIComponent(null == n ? "" : n)
            };
        if (Array.isArray(a) || a.jquery && !e.isPlainObject(a)) e.each(a, function() {
            g(this.name, this.value)
        });
        else
            for (c in a) Jb(c,
                a[c], b, g);
        return f.join("&")
    };
    e.fn.extend({
        serialize: function() {
            return e.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var a = e.prop(this, "elements");
                return a ? e.makeArray(a) : this
            }).filter(function() {
                var a = this.type;
                return this.name && !e(this).is(":disabled") && Ed.test(this.nodeName) && !Dd.test(a) && (this.checked || !Gc.test(a))
            }).map(function(a, b) {
                a = e(this).val();
                return null == a ? null : Array.isArray(a) ? e.map(a, function(c) {
                    return {
                        name: b.name,
                        value: c.replace(Nc, "\r\n")
                    }
                }) : {
                    name: b.name,
                    value: a.replace(Nc, "\r\n")
                }
            }).get()
        }
    });
    var Fd = /%20/g,
        Gd = /#.*$/,
        Hd = /([?&])_=[^&]*/,
        Id = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Jd = /^(?:GET|HEAD)$/,
        Kd = /^\/\//,
        Oc = {},
        lc = {},
        Pc = "*/".concat("*"),
        tc = X.createElement("a");
    tc.href = Nb.href;
    e.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Nb.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Nb.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Pc,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": e.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(a, b) {
            return b ? fb(fb(a, e.ajaxSettings), b) : fb(e.ajaxSettings, a)
        },
        ajaxPrefilter: Kb(Oc),
        ajaxTransport: Kb(lc),
        ajax: function(a,
            b) {
            function c(aa, wa, sa, Da) {
                var $a, ba, Ea, qa = wa;
                if (!U) {
                    U = !0;
                    n && L.clearTimeout(n);
                    f = void 0;
                    g = Da || "";
                    Z.readyState = 0 < aa ? 4 : 0;
                    Da = 200 <= aa && 300 > aa || 304 === aa;
                    if (sa) {
                        var ca = y;
                        for (var La = Z, Aa, Ga, ea, ka, Pa = ca.contents, Wa = ca.dataTypes;
                            "*" === Wa[0];) Wa.shift(), void 0 === Aa && (Aa = ca.mimeType || La.getResponseHeader("Content-Type"));
                        if (Aa)
                            for (Ga in Pa)
                                if (Pa[Ga] && Pa[Ga].test(Aa)) {
                                    Wa.unshift(Ga);
                                    break
                                }
                        if (Wa[0] in sa) ea = Wa[0];
                        else {
                            for (Ga in sa) {
                                if (!Wa[0] || ca.converters[Ga + " " + Wa[0]]) {
                                    ea = Ga;
                                    break
                                }
                                ka || (ka = Ga)
                            }
                            ea = ea || ka
                        }
                        sa = ea ?
                            (ea !== Wa[0] && Wa.unshift(ea), sa[ea]) : void 0;
                        ca = sa
                    }
                    a: {
                        sa = y;Aa = ca;Ga = Z;ea = Da;
                        var Ia, sb, Oa;ca = {};La = sa.dataTypes.slice();
                        if (La[1])
                            for (Ia in sa.converters) ca[Ia.toLowerCase()] = sa.converters[Ia];
                        for (ka = La.shift(); ka;)
                            if (sa.responseFields[ka] && (Ga[sa.responseFields[ka]] = Aa), !Oa && ea && sa.dataFilter && (Aa = sa.dataFilter(Aa, sa.dataType)), Oa = ka, ka = La.shift())
                                if ("*" === ka) ka = Oa;
                                else if ("*" !== Oa && Oa !== ka) {
                            if (!(Ia = ca[Oa + " " + ka] || ca["* " + ka]))
                                for (ab in ca)
                                    if ((sb = ab.split(" "))[1] === ka && (Ia = ca[Oa + " " + sb[0]] || ca["* " +
                                            sb[0]])) {
                                        !0 === Ia ? Ia = ca[ab] : !0 !== ca[ab] && (ka = sb[0], La.unshift(sb[1]));
                                        break
                                    }
                            if (!0 !== Ia)
                                if (Ia && sa["throws"]) Aa = Ia(Aa);
                                else try {
                                    Aa = Ia(Aa)
                                } catch (pc) {
                                    var ab = {
                                        state: "parsererror",
                                        error: Ia ? pc : "No conversion from " + Oa + " to " + ka
                                    };
                                    break a
                                }
                        }
                        ab = {
                            state: "success",
                            data: Aa
                        }
                    }
                    ca = ab;
                    Da ? (y.ifModified && ((Ea = Z.getResponseHeader("Last-Modified")) && (e.lastModified[ua] = Ea), (Ea = Z.getResponseHeader("etag")) && (e.etag[ua] = Ea)), 204 === aa || "HEAD" === y.type ? qa = "nocontent" : 304 === aa ? qa = "notmodified" : (qa = ca.state, $a = ca.data, Da = !(ba =
                        ca.error))) : (ba = qa, !aa && qa || (qa = "error", 0 > aa && (aa = 0)));
                    Z.status = aa;
                    Z.statusText = (wa || qa) + "";
                    Da ? W.resolveWith(I, [$a, qa, Z]) : W.rejectWith(I, [Z, qa, ba]);
                    Z.statusCode(da);
                    da = void 0;
                    r && O.trigger(Da ? "ajaxSuccess" : "ajaxError", [Z, y, Da ? $a : ba]);
                    Q.fireWith(I, [Z, qa]);
                    r && (O.trigger("ajaxComplete", [Z, y]), --e.active || e.event.trigger("ajaxStop"))
                }
            }
            "object" == typeof a && (b = a, a = void 0);
            b = b || {};
            var f, g, l, n, r, u, C, y = e.ajaxSetup({}, b),
                I = y.context || y,
                O = y.context && (I.nodeType || I.jquery) ? e(I) : e.event,
                W = e.Deferred(),
                Q = e.Callbacks("once memory"),
                da = y.statusCode || {},
                ya = {},
                Xa = {},
                za = "canceled",
                Z = {
                    readyState: 0,
                    getResponseHeader: function(aa) {
                        var wa;
                        if (U) {
                            if (!l)
                                for (l = {}; wa = Id.exec(g);) l[wa[1].toLowerCase()] = wa[2];
                            wa = l[aa.toLowerCase()]
                        }
                        return null == wa ? null : wa
                    },
                    getAllResponseHeaders: function() {
                        return U ? g : null
                    },
                    setRequestHeader: function(aa, wa) {
                        return null == U && (aa = Xa[aa.toLowerCase()] = Xa[aa.toLowerCase()] || aa, ya[aa] = wa), this
                    },
                    overrideMimeType: function(aa) {
                        return null == U && (y.mimeType = aa), this
                    },
                    statusCode: function(aa) {
                        var wa;
                        if (aa)
                            if (U) Z.always(aa[Z.status]);
                            else
                                for (wa in aa) da[wa] = [da[wa], aa[wa]];
                        return this
                    },
                    abort: function(aa) {
                        aa = aa || za;
                        return f && f.abort(aa), c(0, aa), this
                    }
                };
            if (W.promise(Z), y.url = ((a || y.url || Nb.href) + "").replace(Kd, Nb.protocol + "//"), y.type = b.method || b.type || y.method || y.type, y.dataTypes = (y.dataType || "*").toLowerCase().match(Ya) || [""], null == y.crossDomain) {
                a = X.createElement("a");
                try {
                    a.href = y.url, a.href = a.href, y.crossDomain = tc.protocol + "//" + tc.host != a.protocol + "//" + a.host
                } catch (aa) {
                    y.crossDomain = !0
                }
            }
            if (y.data && y.processData && "string" != typeof y.data &&
                (y.data = e.param(y.data, y.traditional)), ac(Oc, y, b, Z), U) return Z;
            (r = e.event && y.global) && 0 == e.active++ && e.event.trigger("ajaxStart");
            y.type = y.type.toUpperCase();
            y.hasContent = !Jd.test(y.type);
            var ua = y.url.replace(Gd, "");
            y.hasContent ? y.data && y.processData && 0 === (y.contentType || "").indexOf("application/x-www-form-urlencoded") && (y.data = y.data.replace(Fd, "+")) : (C = y.url.slice(ua.length), y.data && (y.processData || "string" == typeof y.data) && (ua += (sc.test(ua) ? "&" : "?") + y.data, delete y.data), !1 === y.cache && (ua = ua.replace(Hd,
                "$1"), C = (sc.test(ua) ? "&" : "?") + "_=" + Mc++ + C), y.url = ua + C);
            y.ifModified && (e.lastModified[ua] && Z.setRequestHeader("If-Modified-Since", e.lastModified[ua]), e.etag[ua] && Z.setRequestHeader("If-None-Match", e.etag[ua]));
            (y.data && y.hasContent && !1 !== y.contentType || b.contentType) && Z.setRequestHeader("Content-Type", y.contentType);
            Z.setRequestHeader("Accept", y.dataTypes[0] && y.accepts[y.dataTypes[0]] ? y.accepts[y.dataTypes[0]] + ("*" !== y.dataTypes[0] ? ", " + Pc + "; q=0.01" : "") : y.accepts["*"]);
            for (u in y.headers) Z.setRequestHeader(u,
                y.headers[u]);
            if (y.beforeSend && (!1 === y.beforeSend.call(I, Z, y) || U)) return Z.abort();
            if (za = "abort", Q.add(y.complete), Z.done(y.success), Z.fail(y.error), f = ac(lc, y, b, Z)) {
                if (Z.readyState = 1, r && O.trigger("ajaxSend", [Z, y]), U) return Z;
                y.async && 0 < y.timeout && (n = L.setTimeout(function() {
                    Z.abort("timeout")
                }, y.timeout));
                try {
                    var U = !1;
                    f.send(ya, c)
                } catch (aa) {
                    if (U) throw aa;
                    c(-1, aa)
                }
            } else c(-1, "No Transport");
            return Z
        },
        getJSON: function(a, b, c) {
            return e.get(a, b, c, "json")
        },
        getScript: function(a, b) {
            return e.get(a, void 0, b,
                "script")
        }
    });
    e.each(["get", "post"], function(a, b) {
        e[b] = function(c, f, g, l) {
            return H(f) && (l = l || g, g = f, f = void 0), e.ajax(e.extend({
                url: c,
                type: b,
                dataType: l,
                data: f,
                success: g
            }, e.isPlainObject(c) && c))
        }
    });
    e._evalUrl = function(a) {
        return e.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    };
    e.fn.extend({
        wrapAll: function(a) {
            var b;
            return this[0] && (H(a) && (a = a.call(this[0])), b = e(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
                for (var c =
                        this; c.firstElementChild;) c = c.firstElementChild;
                return c
            }).append(this)), this
        },
        wrapInner: function(a) {
            return H(a) ? this.each(function(b) {
                e(this).wrapInner(a.call(this, b))
            }) : this.each(function() {
                var b = e(this),
                    c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        },
        wrap: function(a) {
            var b = H(a);
            return this.each(function(c) {
                e(this).wrapAll(b ? a.call(this, c) : a)
            })
        },
        unwrap: function(a) {
            return this.parent(a).not("body").each(function() {
                e(this).replaceWith(this.childNodes)
            }), this
        }
    });
    e.expr.pseudos.hidden = function(a) {
        return !e.expr.pseudos.visible(a)
    };
    e.expr.pseudos.visible = function(a) {
        return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length)
    };
    e.ajaxSettings.xhr = function() {
        try {
            return new L.XMLHttpRequest
        } catch (a) {}
    };
    var Ld = {
            0: 200,
            1223: 204
        },
        Ob = e.ajaxSettings.xhr();
    G.cors = !!Ob && "withCredentials" in Ob;
    G.ajax = Ob = !!Ob;
    e.ajaxTransport(function(a) {
        var b, c;
        if (G.cors || Ob && !a.crossDomain) return {
            send: function(f, g) {
                var l, n = a.xhr();
                if (n.open(a.type, a.url, a.async, a.username, a.password), a.xhrFields)
                    for (l in a.xhrFields) n[l] = a.xhrFields[l];
                a.mimeType &&
                    n.overrideMimeType && n.overrideMimeType(a.mimeType);
                a.crossDomain || f["X-Requested-With"] || (f["X-Requested-With"] = "XMLHttpRequest");
                for (l in f) n.setRequestHeader(l, f[l]);
                b = function(r) {
                    return function() {
                        b && (b = c = n.onload = n.onerror = n.onabort = n.ontimeout = n.onreadystatechange = null, "abort" === r ? n.abort() : "error" === r ? "number" != typeof n.status ? g(0, "error") : g(n.status, n.statusText) : g(Ld[n.status] || n.status, n.statusText, "text" !== (n.responseType || "text") || "string" != typeof n.responseText ? {
                                binary: n.response
                            } : {
                                text: n.responseText
                            },
                            n.getAllResponseHeaders()))
                    }
                };
                n.onload = b();
                c = n.onerror = n.ontimeout = b("error");
                void 0 !== n.onabort ? n.onabort = c : n.onreadystatechange = function() {
                    4 === n.readyState && L.setTimeout(function() {
                        b && c()
                    })
                };
                b = b("abort");
                try {
                    n.send(a.hasContent && a.data || null)
                } catch (r) {
                    if (b) throw r;
                }
            },
            abort: function() {
                b && b()
            }
        }
    });
    e.ajaxPrefilter(function(a) {
        a.crossDomain && (a.contents.script = !1)
    });
    e.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(a) {
                return e.globalEval(a), a
            }
        }
    });
    e.ajaxPrefilter("script", function(a) {
        void 0 === a.cache && (a.cache = !1);
        a.crossDomain && (a.type = "GET")
    });
    e.ajaxTransport("script", function(a) {
        if (a.crossDomain) {
            var b, c;
            return {
                send: function(f, g) {
                    b = e("<script>").prop({
                        charset: a.scriptCharset,
                        src: a.url
                    }).on("load error", c = function(l) {
                        b.remove();
                        c = null;
                        l && g("error" === l.type ? 404 : 200, l.type)
                    });
                    X.head.appendChild(b[0])
                },
                abort: function() {
                    c && c()
                }
            }
        }
    });
    var Qc = [],
        uc = /(=)\?(?=&|$)|\?\?/;
    e.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = Qc.pop() || e.expando + "_" + Mc++;
            return this[a] = !0, a
        }
    });
    e.ajaxPrefilter("json jsonp", function(a, b, c) {
        var f, g, l, n = !1 !== a.jsonp && (uc.test(a.url) ? "url" : "string" == typeof a.data && 0 === (a.contentType || "").indexOf("application/x-www-form-urlencoded") && uc.test(a.data) && "data");
        if (n || "jsonp" === a.dataTypes[0]) return f = a.jsonpCallback = H(a.jsonpCallback) ? a.jsonpCallback() : a.jsonpCallback, n ? a[n] = a[n].replace(uc, "$1" + f) : !1 !== a.jsonp && (a.url += (sc.test(a.url) ? "&" : "?") + a.jsonp + "=" + f), a.converters["script json"] =
            function() {
                return l || e.error(f + " was not called"), l[0]
            }, a.dataTypes[0] = "json", g = L[f], L[f] = function() {
                l = arguments
            }, c.always(function() {
                void 0 === g ? e(L).removeProp(f) : L[f] = g;
                a[f] && (a.jsonpCallback = b.jsonpCallback, Qc.push(f));
                l && H(g) && g(l[0]);
                l = g = void 0
            }), "script"
    });
    G.createHTMLDocument = function() {
        var a = X.implementation.createHTMLDocument("").body;
        return a.innerHTML = "<form></form><form></form>", 2 === a.childNodes.length
    }();
    e.parseHTML = function(a, b, c) {
        if ("string" != typeof a) return [];
        "boolean" == typeof b &&
            (c = b, b = !1);
        var f, g, l;
        return b || (G.createHTMLDocument ? ((f = (b = X.implementation.createHTMLDocument("")).createElement("base")).href = X.location.href, b.head.appendChild(f)) : b = X), g = fc.exec(a), l = !c && [], g ? [b.createElement(g[1])] : (g = mb([a], b, l), l && l.length && e(l).remove(), e.merge([], g.childNodes))
    };
    e.fn.load = function(a, b, c) {
        var f, g, l, n = this,
            r = a.indexOf(" ");
        return -1 < r && (f = db(a.slice(r)), a = a.slice(0, r)), H(b) ? (c = b, b = void 0) : b && "object" == typeof b && (g = "POST"), 0 < n.length && e.ajax({
            url: a,
            type: g || "GET",
            dataType: "html",
            data: b
        }).done(function(u) {
            l = arguments;
            n.html(f ? e("<div>").append(e.parseHTML(u)).find(f) : u)
        }).always(c && function(u, C) {
            n.each(function() {
                c.apply(this, l || [u.responseText, C, u])
            })
        }), this
    };
    e.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function(a, b) {
        e.fn[b] = function(c) {
            return this.on(b, c)
        }
    });
    e.expr.pseudos.animated = function(a) {
        return e.grep(e.timers, function(b) {
            return a === b.elem
        }).length
    };
    e.offset = {
        setOffset: function(a, b, c) {
            var f, g, l, n = e.css(a, "position"),
                r = e(a),
                u = {};
            "static" === n && (a.style.position = "relative");
            var C = r.offset();
            var y = e.css(a, "top");
            var I = e.css(a, "left");
            ("absolute" === n || "fixed" === n) && -1 < (y + I).indexOf("auto") ? (l = (f = r.position()).top, g = f.left) : (l = parseFloat(y) || 0, g = parseFloat(I) || 0);
            H(b) && (b = b.call(a, c, e.extend({}, C)));
            null != b.top && (u.top = b.top - C.top + l);
            null != b.left && (u.left = b.left - C.left + g);
            "using" in b ? b.using.call(a, u) : r.css(u)
        }
    };
    e.fn.extend({
        offset: function(a) {
            if (arguments.length) return void 0 === a ? this : this.each(function(g) {
                e.offset.setOffset(this,
                    a, g)
            });
            var b, c, f = this[0];
            if (f) return f.getClientRects().length ? (b = f.getBoundingClientRect(), c = f.ownerDocument.defaultView, {
                top: b.top + c.pageYOffset,
                left: b.left + c.pageXOffset
            }) : {
                top: 0,
                left: 0
            }
        },
        position: function() {
            if (this[0]) {
                var a, b = this[0],
                    c = {
                        top: 0,
                        left: 0
                    };
                if ("fixed" === e.css(b, "position")) var f = b.getBoundingClientRect();
                else {
                    f = this.offset();
                    var g = b.ownerDocument;
                    for (a = b.offsetParent || g.documentElement; a && (a === g.body || a === g.documentElement) && "static" === e.css(a, "position");) a = a.parentNode;
                    a && a !== b &&
                        1 === a.nodeType && ((c = e(a).offset()).top += e.css(a, "borderTopWidth", !0), c.left += e.css(a, "borderLeftWidth", !0))
                }
                return {
                    top: f.top - c.top - e.css(b, "marginTop", !0),
                    left: f.left - c.left - e.css(b, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var a = this.offsetParent; a && "static" === e.css(a, "position");) a = a.offsetParent;
                return a || hc
            })
        }
    });
    e.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(a, b) {
        var c = "pageYOffset" === b;
        e.fn[a] = function(f) {
            return kb(this, function(g, l, n) {
                var r;
                if (ia(g) ? r = g : 9 === g.nodeType && (r = g.defaultView), void 0 === n) return r ? r[b] : g[l];
                r ? r.scrollTo(c ? r.pageXOffset : n, c ? n : r.pageYOffset) : g[l] = n
            }, a, f, arguments.length)
        }
    });
    e.each(["top", "left"], function(a, b) {
        e.cssHooks[b] = Ub(G.pixelPosition, function(c, f) {
            if (f) return f = ub(c, b), jc.test(f) ? e(c).position()[b] + "px" : f
        })
    });
    e.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        e.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(c, f) {
            e.fn[f] = function(g, l) {
                var n = arguments.length && (c || "boolean" != typeof g),
                    r = c || (!0 ===
                        g || !0 === l ? "margin" : "border");
                return kb(this, function(u, C, y) {
                    var I;
                    return ia(u) ? 0 === f.indexOf("outer") ? u["inner" + a] : u.document.documentElement["client" + a] : 9 === u.nodeType ? (I = u.documentElement, Math.max(u.body["scroll" + a], I["scroll" + a], u.body["offset" + a], I["offset" + a], I["client" + a])) : void 0 === y ? e.css(u, C, r) : e.style(u, C, y, r)
                }, b, n ? g : void 0, n)
            }
        })
    });
    e.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),
        function(a, b) {
            e.fn[b] = function(c, f) {
                return 0 < arguments.length ? this.on(b, null, c, f) : this.trigger(b)
            }
        });
    e.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        }
    });
    e.fn.extend({
        bind: function(a, b, c) {
            return this.on(a, null, b, c)
        },
        unbind: function(a, b) {
            return this.off(a, null, b)
        },
        delegate: function(a, b, c, f) {
            return this.on(b, a, c, f)
        },
        undelegate: function(a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    });
    e.proxy = function(a, b) {
        var c, f, g;
        if ("string" == typeof b && (c = a[b],
                b = a, a = c), H(a)) return f = hb.call(arguments, 2), g = function() {
            return a.apply(b || this, f.concat(hb.call(arguments)))
        }, g.guid = a.guid = a.guid || e.guid++, g
    };
    e.holdReady = function(a) {
        a ? e.readyWait++ : e.ready(!0)
    };
    e.isArray = Array.isArray;
    e.parseJSON = JSON.parse;
    e.nodeName = va;
    e.isFunction = H;
    e.isWindow = ia;
    e.camelCase = ja;
    e.type = Ja;
    e.now = Date.now;
    e.isNumeric = function(a) {
        var b = e.type(a);
        return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a))
    };
    "function" == typeof define && define.amd && define("jquery", [], function() {
        return e
    });
    var Md = L.jQuery,
        Nd = L.$;
    return e.noConflict = function(a) {
        return L.$ === e && (L.$ = Nd), a && L.jQuery === e && (L.jQuery = Md), e
    }, k || (L.jQuery = L.$ = e), e
});
! function(L) {
    function k(z, K, F) {
        this.settings = F;
        this.$element = z;
        this.$sourceElement = K;
        this.inDrag = this.inTapAndHold = this.inTap = !1;
        this.timerTapAndHold = this.timerTap = this.dragStart = this.tapStart = null;
        this.mouseDown = !1;
        this.eyStart = this.exStart = this.yStart = this.xStart = this.ey = this.ex = this.y = this.x = null;
        this.taps = 0;
        this.ended = this.started = !1
    }

    function oa(z, K, F) {
        var A = z[0];
        return "undefined" == typeof A._touch && (A._touch = new k(z, K, F)), A._touch
    }

    function Ja(z, K, F) {
        var A, N, fa, ja, Ba;
        return A = z.$element.offset(),
            N = z.$element.width(), fa = z.$element.height(), ja = Math.min(Math.max(K, A.left), A.left + N), Ba = Math.min(Math.max(F, A.top), A.top + fa), {
                x: ja,
                y: Ba
            }
    }
    var ta = L(document),
        va = null,
        Y = null,
        tb = {
            useTouch: !0,
            useMouse: !0,
            trackDocument: !1,
            trackDocumentNormalize: !1,
            noClick: !1,
            dragThreshold: 10,
            dragDelay: 200,
            swipeThreshold: 30,
            tapDelay: 250,
            tapAndHoldDelay: 500,
            delegateSelector: null,
            dropFilter: !1,
            dropFilterTraversal: !0,
            coordinates: "page",
            preventDefault: {
                drag: !1,
                swipe: !1,
                tap: !1
            }
        };
    k.prototype.uses = function(z) {
        var K = L._data(this.$sourceElement[0],
            "events");
        switch (z) {
            case "swipe":
                return K.hasOwnProperty(z) || K.hasOwnProperty("swipeUp") || K.hasOwnProperty("swipeDown") || K.hasOwnProperty("swipeLeft") || K.hasOwnProperty("swipeRight");
            case "drag":
                return K.hasOwnProperty(z) || K.hasOwnProperty("dragStart") || K.hasOwnProperty("dragEnd");
            case "tapAndHold":
            case "doubleTap":
                return K.hasOwnProperty(z);
            case "tap":
                return K.hasOwnProperty(z) || K.hasOwnProperty("doubleTap") || K.hasOwnProperty("tapAndHold")
        }
        return !1
    };
    k.prototype.cancel = function(z) {
        this.taps = 0;
        this.inDrag =
            this.inTapAndHold = this.inTap = !1;
        this.eyStart = this.exStart = this.yStart = this.xStart = this.dragStart = this.tapStart = null;
        z && (this.mouseDown = !1)
    };
    k.prototype.doStart = function(z, K, F) {
        var A = this,
            N = A.$element.offset();
        z.stopPropagation();
        (A.uses("drag") && A.settings.preventDefault.drag(A) || A.uses("swipe") && A.settings.preventDefault.swipe(A) || A.uses("tap") && A.settings.preventDefault.tap(A)) && z.preventDefault();
        A.uses("tapAndHold") && A.$element.css("-webkit-tap-highlight-color", "rgba(0,0,0,0)").css("-webkit-touch-callout",
            "none").css("-webkit-user-select", "none");
        A.x = K;
        A.y = F;
        A.ex = K - N.left;
        A.ey = F - N.top;
        A.tapStart = Date.now();
        clearTimeout(A.timerTap);
        A.timerTap = setTimeout(function() {
            A.inTap && 0 < A.taps && (A.$element.trigger(2 == A.taps ? "doubleTap" : "tap", {
                taps: A.taps,
                x: A.x,
                y: A.y,
                ex: A.ex,
                ey: A.ey,
                duration: Date.now() - A.tapStart,
                event: z
            }), A.cancel());
            A.timerTap = null
        }, A.settings.tapDelay);
        A.uses("tapAndHold") && (clearTimeout(A.timerTapAndHold), A.timerTapAndHold = setTimeout(function() {
            A.inTap && (A.$element.trigger("tapAndHold", {
                x: A.x,
                y: A.y,
                ex: A.ex,
                ey: A.ey,
                duration: Date.now() - A.tapStart,
                event: z
            }), A.cancel());
            A.timerTapAndHold = null;
            A.inTapAndHold = !0
        }, A.settings.tapAndHoldDelay));
        A.inTap = !0
    };
    k.prototype.doMove = function(z, K, F) {
        var A, N = this.$element.offset(),
            fa = (Math.abs(this.x - K) + Math.abs(this.y - F)) / 2;
        if (z.stopPropagation(), (this.uses("swipe") && this.settings.preventDefault.swipe(this) || this.uses("drag") && this.settings.preventDefault.drag(this)) && z.preventDefault(), 2 < fa && clearTimeout(this.timerTapAndHold), this.inDrag && va == this) {
            if (this.$element.trigger("drag", {
                    x: K,
                    y: F,
                    ex: K - N.left,
                    ey: F - N.top,
                    start: {
                        x: this.xStart,
                        y: this.yStart,
                        ex: this.exStart,
                        ey: this.eyStart
                    },
                    event: z,
                    exStart: this.exStart,
                    eyStart: this.eyStart
                }), this.$element.css("pointer-events", "none"), A = "fixed" == this.$element.css("position") ? document.elementFromPoint(K - ta.scrollLeft(), F - ta.scrollTop()) : document.elementFromPoint(K, F), this.$element.css("pointer-events", ""), A) {
                if (!1 !== this.settings.dropFilter) switch (typeof this.settings.dropFilter) {
                    case "string":
                        if (this.settings.dropFilterTraversal)
                            for (; A &&
                                !L(A).is(this.settings.dropFilter);) A = A.parentElement;
                        else L(A).is(this.settings.dropFilter) || (A = null);
                        break;
                    case "function":
                        if (this.settings.dropFilterTraversal)
                            for (; A && !0 !== this.settings.dropFilter(this.$element[0], A);) A = A.parentElement;
                        else !1 === this.settings.dropFilter(this.$element[0], A) && (A = null);
                        break;
                    default:
                    case "boolean":
                        if (!0 === this.settings.dropFilter)
                            for (; A.parentElement != this.$element[0].parentElement;)
                                if (A = A.parentElement, !A) {
                                    A = null;
                                    break
                                }
                }
                A === this.$element[0] && (A = null)
            }
            Y && Y !== A && (this.$element.trigger("dragLeave", {
                element: Y,
                event: z
            }), Y = null);
            !Y && A && (Y = A, this.$element.trigger("dragEnter", {
                element: Y,
                event: z
            }));
            Y && (N = L(Y).offset(), this.$element.trigger("dragOver", {
                element: Y,
                event: z,
                x: K,
                y: F,
                ex: K - N.left,
                ey: F - N.top
            }))
        } else if (fa > this.settings.dragThreshold) {
            if (Date.now() - this.tapStart < this.settings.dragDelay) return void this.cancel();
            this.cancel();
            this.inDrag = !0;
            this.dragStart = Date.now();
            this.xStart = K;
            this.yStart = F;
            this.exStart = K - N.left;
            this.eyStart = F - N.top;
            this.uses("drag") && this.settings.preventDefault.drag(this) &&
                z.preventDefault();
            this.$element.trigger("dragStart", {
                x: this.xStart,
                y: this.yStart,
                ex: this.exStart,
                ey: this.eyStart,
                event: z
            });
            va = this
        }
    };
    k.prototype.doEnd = function(z, K, F) {
        var A, N, fa, ja = this.$element.offset(),
            Ba = Math.abs(this.x - K),
            Ra = Math.abs(this.y - F);
        z.stopPropagation();
        this.inTap ? (clearTimeout(this.timerTapAndHold), this.taps++, (!this.timerTap || 1 == this.taps && !this.uses("doubleTap") || 2 == this.taps && this.uses("doubleTap")) && (this.$element.trigger(2 == this.taps ? "doubleTap" : "tap", {
            taps: this.taps,
            x: this.x,
            y: this.y,
            ex: this.ex,
            ey: this.ey,
            duration: Date.now() - this.tapStart,
            event: z
        }), this.cancel())) : this.inDrag ? (Y && (ja = L(Y).offset(), this.$element.trigger("drop", {
                element: Y,
                event: z,
                x: K,
                y: F,
                ex: K - ja.left,
                ey: F - ja.top
            }), Y = null), fa = Date.now() - this.dragStart, A = Math.sqrt(Math.pow(Math.abs(this.x - K), 2) + Math.pow(Math.abs(this.y - F), 2)), N = A / fa, this.$element.trigger("dragEnd", {
                start: {
                    x: this.x,
                    y: this.y,
                    ex: this.ex,
                    ey: this.ey
                },
                end: {
                    x: K,
                    y: F,
                    ex: K - ja.left,
                    ey: F - ja.top
                },
                distance: A,
                duration: fa,
                velocity: N,
                event: z
            }), va = null, (Ba >
                this.settings.swipeThreshold || Ra > this.settings.swipeThreshold) && (this.$element.trigger("swipe", {
                distance: A,
                duration: fa,
                velocity: N,
                event: z
            }), Ba > Ra ? (N = Ba / fa, K < this.x ? this.$element.trigger("swipeLeft", {
                distance: Ba,
                duration: fa,
                velocity: N,
                event: z
            }) : this.$element.trigger("swipeRight", {
                distance: Ba,
                duration: fa,
                velocity: N,
                event: z
            })) : Ra > Ba && (N = Ra / fa, F < this.y ? this.$element.trigger("swipeUp", {
                distance: Ra,
                duration: fa,
                velocity: N,
                event: z
            }) : this.$element.trigger("swipeDown", {
                distance: Ra,
                duration: fa,
                velocity: N,
                event: z
            }))),
            this.inDrag = !1) : this.inTapAndHold && (clearTimeout(this.timerTapAndHold), this.$element.trigger("tapAndHoldEnd", {
            x: this.x,
            y: this.y,
            event: z
        }), this.inTapAndHold = !1)
    };
    L.fn.touch = function(z) {
        var K = L(this);
        if (1 < this.length)
            for (var F = 0; F < this.length; F++) L.touch(L(this[F]), z);
        else 1 == this.length && L.touch(K, z);
        return K
    };
    L.fn.enableTouch = function(z) {
        return L(this).touch(z)
    };
    L.touch = function(z, K) {
        var F = {};
        if (F = L.extend(F, tb), F = L.extend(F, K), "function" != typeof F.preventDefault.drag && (F.preventDefault.drag = !0 ===
                F.preventDefault.drag ? function(A) {
                    return !0
                } : function(A) {
                    return !1
                }), "function" != typeof F.preventDefault.swipe && (F.preventDefault.swipe = !0 === F.preventDefault.swipe ? function(A) {
                return !0
            } : function(A) {
                return !1
            }), "function" != typeof F.preventDefault.tap && (F.preventDefault.tap = !0 === F.preventDefault.tap ? function(A) {
                return !0
            } : function(A) {
                return !1
            }), F.noClick && z.on("click", function(A) {
                A.preventDefault()
            }), F.useTouch) K = function(A) {
            var N = L(this),
                fa = oa(N, z, F);
            fa.started = !0;
            fa.doStart(A, A.originalEvent.touches[0][F.coordinates +
                "X"
            ], A.originalEvent.touches[0][F.coordinates + "Y"]);
            setTimeout(function() {
                fa.started = !1
            }, 1E3)
        }, z.on("touchstart", K), F.delegateSelector && z.on("touchstart", F.delegateSelector, K), K = function(A) {
            var N = L(this);
            N = oa(N, z, F);
            var fa = A.originalEvent.touches[0][F.coordinates + "X"],
                ja = A.originalEvent.touches[0][F.coordinates + "Y"];
            N.settings.trackDocument && N.settings.trackDocumentNormalize && (ja = Ja(N, fa, ja), fa = ja.x, ja = ja.y);
            N.doMove(A, fa, ja)
        }, z.on("touchmove", K), F.delegateSelector && z.on("touchmove", F.delegateSelector,
            K), K = function(A) {
            var N = L(this),
                fa = oa(N, z, F);
            fa.ended = !0;
            N = Ja(fa, A.originalEvent.changedTouches[0][F.coordinates + "X"], A.originalEvent.changedTouches[0][F.coordinates + "Y"]);
            fa.doEnd(A, N.x, N.y);
            setTimeout(function() {
                fa.ended = !1
            }, 1E3)
        }, z.on("touchend", K), F.delegateSelector && z.on("touchend", F.delegateSelector, K);
        F.useMouse && (K = function(A) {
            var N = L(this);
            N = oa(N, z, F);
            return !N.started && (N.mouseDown = !0, void N.doStart(A, A[F.coordinates + "X"], A[F.coordinates + "Y"]))
        }, z.on("mousedown", K), F.delegateSelector && z.on("mousedown",
            F.delegateSelector, K), K = function(A) {
            var N = L(this);
            N = oa(N, z, F);
            N.mouseDown && N.doMove(A, A[F.coordinates + "X"], A[F.coordinates + "Y"])
        }, z.on("mousemove", K), F.delegateSelector && z.on("mousemove", F.delegateSelector, K), K = function(A) {
            var N = L(this);
            N = oa(N, z, F);
            return !N.ended && (ta.triggerHandler("mouseup", A), N.doEnd(A, A[F.coordinates + "X"], A[F.coordinates + "Y"]), void(N.mouseDown = !1))
        }, z.on("mouseup", K), F.delegateSelector && z.on("mouseup", F.delegateSelector, K));
        F.trackDocument || z.on("mouseleave", function(A) {
            var N =
                L(this);
            N = oa(N, z, F);
            N.doEnd(A, A[F.coordinates + "X"], A[F.coordinates + "Y"]);
            N.mouseDown = !1
        })
    };
    ta.on("mousemove", function(z) {
        var K = va;
        if (K && K.settings.useMouse && K.mouseDown && K.settings.trackDocument) {
            var F = z[K.settings.coordinates + "X"],
                A = z[K.settings.coordinates + "Y"];
            K.settings.trackDocumentNormalize && (A = Ja(K, F, A), F = A.x, A = A.y);
            K.doMove(z, F, A)
        }
    }).on("mouseup", function(z, K) {
        var F = va;
        if (F && F.settings.useMouse && F.settings.trackDocument && ("undefined" != typeof K && (z = K), F.settings.coordinates + "X" in z)) {
            K = z[F.settings.coordinates +
                "X"];
            var A = z[F.settings.coordinates + "Y"];
            F.settings.trackDocumentNormalize && (A = Ja(F, K, A), K = A.x, A = A.y);
            F.doEnd(z, K, A);
            F.mouseDown = !1
        }
    })
}(jQuery);
! function(L, k) {
    "object" == typeof exports && "undefined" != typeof module ? k(exports, require("jquery"), require("popper.js")) : "function" == typeof define && define.amd ? define(["exports", "jquery", "popper.js"], k) : k((L = L || self).bootstrap = {}, L.jQuery, L.Popper)
}(this, function(L, k, oa) {
    function Ja(w, t) {
        for (var d = 0; d < t.length; d++) {
            var h = t[d];
            h.enumerable = h.enumerable || !1;
            h.configurable = !0;
            "value" in h && (h.writable = !0);
            Object.defineProperty(w, h.key, h)
        }
    }

    function ta(w, t, d) {
        return t && Ja(w.prototype, t), d && Ja(w, d), w
    }

    function va(w,
        t) {
        var d = Object.keys(w);
        if (Object.getOwnPropertySymbols) {
            var h = Object.getOwnPropertySymbols(w);
            t && (h = h.filter(function(p) {
                return Object.getOwnPropertyDescriptor(w, p).enumerable
            }));
            d.push.apply(d, h)
        }
        return d
    }

    function Y(w) {
        for (var t = 1; t < arguments.length; t++) {
            var d = null != arguments[t] ? arguments[t] : {};
            t % 2 ? va(Object(d), !0).forEach(function(h) {
                var p = d[h];
                h in w ? Object.defineProperty(w, h, {
                    value: p,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : w[h] = p
            }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(w,
                Object.getOwnPropertyDescriptors(d)) : va(Object(d)).forEach(function(h) {
                Object.defineProperty(w, h, Object.getOwnPropertyDescriptor(d, h))
            })
        }
        return w
    }

    function tb(w, t, d) {
        if (0 === w.length) return w;
        if (d && "function" == typeof d) return d(w);
        w = (new window.DOMParser).parseFromString(w, "text/html");
        var h = Object.keys(t),
            p = [].slice.call(w.body.querySelectorAll("*"));
        d = function(H) {
            var ia = p[H];
            H = ia.nodeName.toLowerCase();
            if (-1 === h.indexOf(ia.nodeName.toLowerCase())) return ia.parentNode.removeChild(ia), "continue";
            var pa = [].slice.call(ia.attributes),
                e = [].concat(t["*"] || [], t[H] || []);
            pa.forEach(function(vb) {
                ! function(Ma, Qa) {
                    var Bb = Ma.nodeName.toLowerCase();
                    if (-1 !== Qa.indexOf(Bb)) return -1 === Sb.indexOf(Bb) || !(!Ma.nodeValue.match(ub) && !Ma.nodeValue.match(Ub));
                    Ma = Qa.filter(function(fc) {
                        return fc instanceof RegExp
                    });
                    Qa = 0;
                    for (var ec = Ma.length; Qa < ec; Qa++)
                        if (Bb.match(Ma[Qa])) return !0;
                    return !1
                }(vb, e) && ia.removeAttribute(vb.nodeName)
            })
        };
        for (var D = 0, G = p.length; D < G; D++) d(D);
        return w.body.innerHTML
    }
    k = k && k.hasOwnProperty("default") ?
        k.default : k;
    oa = oa && oa.hasOwnProperty("default") ? oa.default : oa;
    var z = {
        TRANSITION_END: "bsTransitionEnd",
        getUID: function(w) {
            for (; w += ~~(1E6 * Math.random()), document.getElementById(w););
            return w
        },
        getSelectorFromElement: function(w) {
            var t = w.getAttribute("data-target");
            t && "#" !== t || (t = (w = w.getAttribute("href")) && "#" !== w ? w.trim() : "");
            try {
                return document.querySelector(t) ? t : null
            } catch (d) {
                return null
            }
        },
        getTransitionDurationFromElement: function(w) {
            if (!w) return 0;
            var t = k(w).css("transition-duration");
            w = k(w).css("transition-delay");
            var d = parseFloat(t),
                h = parseFloat(w);
            return d || h ? (t = t.split(",")[0], w = w.split(",")[0], 1E3 * (parseFloat(t) + parseFloat(w))) : 0
        },
        reflow: function(w) {
            return w.offsetHeight
        },
        triggerTransitionEnd: function(w) {
            k(w).trigger("transitionend")
        },
        supportsTransitionEnd: function() {
            return !0
        },
        isElement: function(w) {
            return (w[0] || w).nodeType
        },
        typeCheckConfig: function(w, t, d) {
            for (var h in d)
                if (Object.prototype.hasOwnProperty.call(d, h)) {
                    var p = d[h],
                        D = t[h];
                    D = D && z.isElement(D) ? "element" : (G = D, {}.toString.call(G).match(/\s([a-z]+)/i)[1].toLowerCase());
                    if (!(new RegExp(p)).test(D)) throw Error(w.toUpperCase() + ': Option "' + h + '" provided type "' + D + '" but expected type "' + p + '".');
                }
            var G
        },
        findShadowRoot: function(w) {
            if (!document.documentElement.attachShadow) return null;
            if ("function" != typeof w.getRootNode) return w instanceof ShadowRoot ? w : w.parentNode ? z.findShadowRoot(w.parentNode) : null;
            w = w.getRootNode();
            return w instanceof ShadowRoot ? w : null
        },
        jQueryDetection: function() {
            if ("undefined" == typeof k) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
            var w = k.fn.jquery.split(" ")[0].split(".");
            if (2 > w[0] && 9 > w[1] || 1 === w[0] && 9 === w[1] && 1 > w[2] || 4 <= w[0]) throw Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
        }
    };
    z.jQueryDetection();
    k.fn.emulateTransitionEnd = function(w) {
        var t = this,
            d = !1;
        return k(this).one(z.TRANSITION_END, function() {
            d = !0
        }), setTimeout(function() {
            d || z.triggerTransitionEnd(t)
        }, w), this
    };
    k.event.special[z.TRANSITION_END] = {
        bindType: "transitionend",
        delegateType: "transitionend",
        handle: function(w) {
            if (k(w.target).is(this)) return w.handleObj.handler.apply(this,
                arguments)
        }
    };
    var K = k.fn.alert,
        F = function() {
            function w(d) {
                this._element = d
            }
            var t = w.prototype;
            return t.close = function(d) {
                var h = this._element;
                d && (h = this._getRootElement(d));
                this._triggerCloseEvent(h).isDefaultPrevented() || this._removeElement(h)
            }, t.dispose = function() {
                k.removeData(this._element, "bs.alert");
                this._element = null
            }, t._getRootElement = function(d) {
                var h = z.getSelectorFromElement(d),
                    p = !1;
                return h && (p = document.querySelector(h)), p || k(d).closest(".alert")[0]
            }, t._triggerCloseEvent = function(d) {
                var h = k.Event("close.bs.alert");
                return k(d).trigger(h), h
            }, t._removeElement = function(d) {
                var h = this;
                if (k(d).removeClass("show"), k(d).hasClass("fade")) {
                    var p = z.getTransitionDurationFromElement(d);
                    k(d).one(z.TRANSITION_END, function(D) {
                        return h._destroyElement(d, D)
                    }).emulateTransitionEnd(p)
                } else this._destroyElement(d)
            }, t._destroyElement = function(d) {
                k(d).detach().trigger("closed.bs.alert").remove()
            }, w._jQueryInterface = function(d) {
                return this.each(function() {
                    var h = k(this),
                        p = h.data("bs.alert");
                    p || (p = new w(this), h.data("bs.alert", p));
                    "close" ===
                    d && p[d](this)
                })
            }, w._handleDismiss = function(d) {
                return function(h) {
                    h && h.preventDefault();
                    d.close(this)
                }
            }, ta(w, null, [{
                key: "VERSION",
                get: function() {
                    return "4.4.1"
                }
            }]), w
        }();
    k(document).on("click.bs.alert.data-api", '[data-dismiss="alert"]', F._handleDismiss(new F));
    k.fn.alert = F._jQueryInterface;
    k.fn.alert.Constructor = F;
    k.fn.alert.noConflict = function() {
        return k.fn.alert = K, F._jQueryInterface
    };
    var A = k.fn.button,
        N = function() {
            function w(d) {
                this._element = d
            }
            var t = w.prototype;
            return t.toggle = function() {
                var d = !0,
                    h = !0,
                    p = k(this._element).closest('[data-toggle="buttons"]')[0];
                if (p) {
                    var D = this._element.querySelector('input:not([type="hidden"])');
                    D && ("radio" === D.type ? D.checked && this._element.classList.contains("active") ? d = !1 : (h = p.querySelector(".active")) && k(h).removeClass("active") : "checkbox" === D.type ? "LABEL" === this._element.tagName && D.checked === this._element.classList.contains("active") && (d = !1) : d = !1, d && (D.checked = !this._element.classList.contains("active"), k(D).trigger("change")), D.focus(), h = !1)
                }
                this._element.hasAttribute("disabled") ||
                    this._element.classList.contains("disabled") || (h && this._element.setAttribute("aria-pressed", !this._element.classList.contains("active")), d && k(this._element).toggleClass("active"))
            }, t.dispose = function() {
                k.removeData(this._element, "bs.button");
                this._element = null
            }, w._jQueryInterface = function(d) {
                return this.each(function() {
                    var h = k(this).data("bs.button");
                    h || (h = new w(this), k(this).data("bs.button", h));
                    "toggle" === d && h[d]()
                })
            }, ta(w, null, [{
                key: "VERSION",
                get: function() {
                    return "4.4.1"
                }
            }]), w
        }();
    k(document).on("click.bs.button.data-api",
        '[data-toggle^="button"]',
        function(w) {
            var t = w.target;
            if (k(t).hasClass("btn") || (t = k(t).closest(".btn")[0]), !t || t.hasAttribute("disabled") || t.classList.contains("disabled")) w.preventDefault();
            else {
                var d = t.querySelector('input:not([type="hidden"])');
                if (d && (d.hasAttribute("disabled") || d.classList.contains("disabled"))) return void w.preventDefault();
                N._jQueryInterface.call(k(t), "toggle")
            }
        }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(w) {
        var t = k(w.target).closest(".btn")[0];
        k(t).toggleClass("focus", /^focus(in)?$/.test(w.type))
    });
    k(window).on("load.bs.button.data-api", function() {
        for (var w = [].slice.call(document.querySelectorAll('[data-toggle="buttons"] .btn')), t = 0, d = w.length; t < d; t++) {
            var h = w[t],
                p = h.querySelector('input:not([type="hidden"])');
            p.checked || p.hasAttribute("checked") ? h.classList.add("active") : h.classList.remove("active")
        }
        t = 0;
        for (d = (w = [].slice.call(document.querySelectorAll('[data-toggle="button"]'))).length; t < d; t++) h = w[t], "true" === h.getAttribute("aria-pressed") ?
            h.classList.add("active") : h.classList.remove("active")
    });
    k.fn.button = N._jQueryInterface;
    k.fn.button.Constructor = N;
    k.fn.button.noConflict = function() {
        return k.fn.button = A, N._jQueryInterface
    };
    var fa = k.fn.carousel,
        ja = {
            interval: 5E3,
            keyboard: !0,
            slide: !1,
            pause: "hover",
            wrap: !0,
            touch: !0
        },
        Ba = {
            interval: "(number|boolean)",
            keyboard: "boolean",
            slide: "(boolean|string)",
            pause: "(string|boolean)",
            wrap: "boolean",
            touch: "boolean"
        },
        Ra = {
            TOUCH: "touch",
            PEN: "pen"
        },
        bb = function() {
            function w(d, h) {
                this._activeElement = this._interval =
                    this._items = null;
                this._isSliding = this._isPaused = !1;
                this.touchTimeout = null;
                this.touchDeltaX = this.touchStartX = 0;
                this._config = this._getConfig(h);
                this._element = d;
                this._indicatorsElement = this._element.querySelector(".carousel-indicators");
                this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints;
                this._pointerEvent = !(!window.PointerEvent && !window.MSPointerEvent);
                this._addEventListeners()
            }
            var t = w.prototype;
            return t.next = function() {
                    this._isSliding || this._slide("next")
                }, t.nextWhenVisible =
                function() {
                    !document.hidden && k(this._element).is(":visible") && "hidden" !== k(this._element).css("visibility") && this.next()
                }, t.prev = function() {
                    this._isSliding || this._slide("prev")
                }, t.pause = function(d) {
                    d || (this._isPaused = !0);
                    this._element.querySelector(".carousel-item-next, .carousel-item-prev") && (z.triggerTransitionEnd(this._element), this.cycle(!0));
                    clearInterval(this._interval);
                    this._interval = null
                }, t.cycle = function(d) {
                    d || (this._isPaused = !1);
                    this._interval && (clearInterval(this._interval), this._interval =
                        null);
                    this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
                }, t.to = function(d) {
                    var h = this;
                    this._activeElement = this._element.querySelector(".active.carousel-item");
                    var p = this._getItemIndex(this._activeElement);
                    if (!(d > this._items.length - 1 || 0 > d))
                        if (this._isSliding) k(this._element).one("slid.bs.carousel", function() {
                            return h.to(d)
                        });
                        else {
                            if (p === d) return this.pause(), void this.cycle();
                            this._slide(p <
                                d ? "next" : "prev", this._items[d])
                        }
                }, t.dispose = function() {
                    k(this._element).off(".bs.carousel");
                    k.removeData(this._element, "bs.carousel");
                    this._indicatorsElement = this._activeElement = this._isSliding = this._isPaused = this._interval = this._element = this._config = this._items = null
                }, t._getConfig = function(d) {
                    return d = Y({}, ja, {}, d), z.typeCheckConfig("carousel", d, Ba), d
                }, t._handleSwipe = function() {
                    var d = Math.abs(this.touchDeltaX);
                    40 >= d || (d /= this.touchDeltaX, (this.touchDeltaX = 0) < d && this.prev(), 0 > d && this.next())
                }, t._addEventListeners =
                function() {
                    var d = this;
                    this._config.keyboard && k(this._element).on("keydown.bs.carousel", function(h) {
                        return d._keydown(h)
                    });
                    "hover" === this._config.pause && k(this._element).on("mouseenter.bs.carousel", function(h) {
                        return d.pause(h)
                    }).on("mouseleave.bs.carousel", function(h) {
                        return d.cycle(h)
                    });
                    this._config.touch && this._addTouchEventListeners()
                }, t._addTouchEventListeners = function() {
                    var d = this;
                    if (this._touchSupported) {
                        var h = function(D) {
                                d._pointerEvent && Ra[D.originalEvent.pointerType.toUpperCase()] ? d.touchStartX =
                                    D.originalEvent.clientX : d._pointerEvent || (d.touchStartX = D.originalEvent.touches[0].clientX)
                            },
                            p = function(D) {
                                d._pointerEvent && Ra[D.originalEvent.pointerType.toUpperCase()] && (d.touchDeltaX = D.originalEvent.clientX - d.touchStartX);
                                d._handleSwipe();
                                "hover" === d._config.pause && (d.pause(), d.touchTimeout && clearTimeout(d.touchTimeout), d.touchTimeout = setTimeout(function(G) {
                                    return d.cycle(G)
                                }, 500 + d._config.interval))
                            };
                        k(this._element.querySelectorAll(".carousel-item img")).on("dragstart.bs.carousel", function(D) {
                            return D.preventDefault()
                        });
                        this._pointerEvent ? (k(this._element).on("pointerdown.bs.carousel", function(D) {
                            return h(D)
                        }), k(this._element).on("pointerup.bs.carousel", function(D) {
                            return p(D)
                        }), this._element.classList.add("pointer-event")) : (k(this._element).on("touchstart.bs.carousel", function(D) {
                            return h(D)
                        }), k(this._element).on("touchmove.bs.carousel", function(D) {
                            D.originalEvent.touches && 1 < D.originalEvent.touches.length ? d.touchDeltaX = 0 : d.touchDeltaX = D.originalEvent.touches[0].clientX - d.touchStartX
                        }), k(this._element).on("touchend.bs.carousel",
                            function(D) {
                                return p(D)
                            }))
                    }
                }, t._keydown = function(d) {
                    if (!/input|textarea/i.test(d.target.tagName)) switch (d.which) {
                        case 37:
                            d.preventDefault();
                            this.prev();
                            break;
                        case 39:
                            d.preventDefault(), this.next()
                    }
                }, t._getItemIndex = function(d) {
                    return this._items = d && d.parentNode ? [].slice.call(d.parentNode.querySelectorAll(".carousel-item")) : [], this._items.indexOf(d)
                }, t._getItemByDirection = function(d, h) {
                    var p = "next" === d,
                        D = "prev" === d,
                        G = this._getItemIndex(h),
                        H = this._items.length - 1;
                    if ((D && 0 === G || p && G === H) && !this._config.wrap) return h;
                    d = (G + ("prev" === d ? -1 : 1)) % this._items.length;
                    return -1 == d ? this._items[this._items.length - 1] : this._items[d]
                }, t._triggerSlideEvent = function(d, h) {
                    var p = this._getItemIndex(d),
                        D = this._getItemIndex(this._element.querySelector(".active.carousel-item"));
                    d = k.Event("slide.bs.carousel", {
                        relatedTarget: d,
                        direction: h,
                        from: D,
                        to: p
                    });
                    return k(this._element).trigger(d), d
                }, t._setActiveIndicatorElement = function(d) {
                    if (this._indicatorsElement) {
                        var h = [].slice.call(this._indicatorsElement.querySelectorAll(".active"));
                        k(h).removeClass("active");
                        (d = this._indicatorsElement.children[this._getItemIndex(d)]) && k(d).addClass("active")
                    }
                }, t._slide = function(d, h) {
                    var p, D, G = this,
                        H = this._element.querySelector(".active.carousel-item"),
                        ia = this._getItemIndex(H),
                        pa = h || H && this._getItemByDirection(d, H),
                        e = this._getItemIndex(pa);
                    h = !!this._interval;
                    if (d = "next" === d ? (p = "carousel-item-left", D = "carousel-item-next", "left") : (p = "carousel-item-right", D = "carousel-item-prev", "right"), pa && k(pa).hasClass("active")) this._isSliding = !1;
                    else if (!this._triggerSlideEvent(pa, d).isDefaultPrevented() &&
                        H && pa) {
                        this._isSliding = !0;
                        h && this.pause();
                        this._setActiveIndicatorElement(pa);
                        var vb = k.Event("slid.bs.carousel", {
                            relatedTarget: pa,
                            direction: d,
                            from: ia,
                            to: e
                        });
                        k(this._element).hasClass("slide") ? (k(pa).addClass(D), z.reflow(pa), k(H).addClass(p), k(pa).addClass(p), (ia = parseInt(pa.getAttribute("data-interval"), 10)) ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, this._config.interval = ia) : this._config.interval = this._config.defaultInterval || this._config.interval, ia = z.getTransitionDurationFromElement(H),
                            k(H).one(z.TRANSITION_END, function() {
                                k(pa).removeClass(p + " " + D).addClass("active");
                                k(H).removeClass("active " + D + " " + p);
                                G._isSliding = !1;
                                setTimeout(function() {
                                    return k(G._element).trigger(vb)
                                }, 0)
                            }).emulateTransitionEnd(ia)) : (k(H).removeClass("active"), k(pa).addClass("active"), this._isSliding = !1, k(this._element).trigger(vb));
                        h && this.cycle()
                    }
                }, w._jQueryInterface = function(d) {
                    return this.each(function() {
                        var h = k(this).data("bs.carousel"),
                            p = Y({}, ja, {}, k(this).data());
                        "object" == typeof d && (p = Y({}, p, {}, d));
                        var D = "string" == typeof d ? d : p.slide;
                        if (h || (h = new w(this, p), k(this).data("bs.carousel", h)), "number" == typeof d) h.to(d);
                        else if ("string" == typeof D) {
                            if ("undefined" == typeof h[D]) throw new TypeError('No method named "' + D + '"');
                            h[D]()
                        } else p.interval && p.ride && (h.pause(), h.cycle())
                    })
                }, w._dataApiClickHandler = function(d) {
                    var h = z.getSelectorFromElement(this);
                    if (h && (h = k(h)[0]) && k(h).hasClass("carousel")) {
                        var p = Y({}, k(h).data(), {}, k(this).data()),
                            D = this.getAttribute("data-slide-to");
                        D && (p.interval = !1);
                        w._jQueryInterface.call(k(h),
                            p);
                        D && k(h).data("bs.carousel").to(D);
                        d.preventDefault()
                    }
                }, ta(w, null, [{
                    key: "VERSION",
                    get: function() {
                        return "4.4.1"
                    }
                }, {
                    key: "Default",
                    get: function() {
                        return ja
                    }
                }]), w
        }();
    k(document).on("click.bs.carousel.data-api", "[data-slide], [data-slide-to]", bb._dataApiClickHandler);
    k(window).on("load.bs.carousel.data-api", function() {
        for (var w = [].slice.call(document.querySelectorAll('[data-ride="carousel"]')), t = 0, d = w.length; t < d; t++) {
            var h = k(w[t]);
            bb._jQueryInterface.call(h, h.data())
        }
    });
    k.fn.carousel = bb._jQueryInterface;
    k.fn.carousel.Constructor = bb;
    k.fn.carousel.noConflict = function() {
        return k.fn.carousel = fa, bb._jQueryInterface
    };
    var lb = k.fn.collapse,
        xa = {
            toggle: !0,
            parent: ""
        },
        Db = {
            toggle: "boolean",
            parent: "(string|element)"
        },
        mb = function() {
            function w(d, h) {
                this._isTransitioning = !1;
                this._element = d;
                this._config = this._getConfig(h);
                this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + d.id + '"],[data-toggle="collapse"][data-target="#' + d.id + '"]'));
                h = [].slice.call(document.querySelectorAll('[data-toggle="collapse"]'));
                for (var p = 0, D = h.length; p < D; p++) {
                    var G = h[p],
                        H = z.getSelectorFromElement(G),
                        ia = [].slice.call(document.querySelectorAll(H)).filter(function(pa) {
                            return pa === d
                        });
                    null !== H && 0 < ia.length && (this._selector = H, this._triggerArray.push(G))
                }
                this._parent = this._config.parent ? this._getParent() : null;
                this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray);
                this._config.toggle && this.toggle()
            }
            var t = w.prototype;
            return t.toggle = function() {
                    k(this._element).hasClass("show") ? this.hide() : this.show()
                },
                t.show = function() {
                    var d, h, p = this;
                    if (!(this._isTransitioning || k(this._element).hasClass("show") || (this._parent && 0 === (d = [].slice.call(this._parent.querySelectorAll(".show, .collapsing")).filter(function(H) {
                            return "string" == typeof p._config.parent ? H.getAttribute("data-parent") === p._config.parent : H.classList.contains("collapse")
                        })).length && (d = null), d && (h = k(d).not(this._selector).data("bs.collapse")) && h._isTransitioning))) {
                        var D = k.Event("show.bs.collapse");
                        if (k(this._element).trigger(D), !D.isDefaultPrevented()) {
                            d &&
                                (w._jQueryInterface.call(k(d).not(this._selector), "hide"), h || k(d).data("bs.collapse", null));
                            var G = this._getDimension();
                            k(this._element).removeClass("collapse").addClass("collapsing");
                            this._element.style[G] = 0;
                            this._triggerArray.length && k(this._triggerArray).removeClass("collapsed").attr("aria-expanded", !0);
                            this.setTransitioning(!0);
                            d = "scroll" + (G[0].toUpperCase() + G.slice(1));
                            h = z.getTransitionDurationFromElement(this._element);
                            k(this._element).one(z.TRANSITION_END, function() {
                                k(p._element).removeClass("collapsing").addClass("collapse").addClass("show");
                                p._element.style[G] = "";
                                p.setTransitioning(!1);
                                k(p._element).trigger("shown.bs.collapse")
                            }).emulateTransitionEnd(h);
                            this._element.style[G] = this._element[d] + "px"
                        }
                    }
                }, t.hide = function() {
                    var d = this;
                    if (!this._isTransitioning && k(this._element).hasClass("show")) {
                        var h = k.Event("hide.bs.collapse");
                        if (k(this._element).trigger(h), !h.isDefaultPrevented()) {
                            h = this._getDimension();
                            this._element.style[h] = this._element.getBoundingClientRect()[h] + "px";
                            z.reflow(this._element);
                            k(this._element).addClass("collapsing").removeClass("collapse").removeClass("show");
                            var p = this._triggerArray.length;
                            if (0 < p)
                                for (var D = 0; D < p; D++) {
                                    var G = this._triggerArray[D],
                                        H = z.getSelectorFromElement(G);
                                    null !== H && (k([].slice.call(document.querySelectorAll(H))).hasClass("show") || k(G).addClass("collapsed").attr("aria-expanded", !1))
                                }
                            this.setTransitioning(!0);
                            this._element.style[h] = "";
                            h = z.getTransitionDurationFromElement(this._element);
                            k(this._element).one(z.TRANSITION_END, function() {
                                d.setTransitioning(!1);
                                k(d._element).removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                            }).emulateTransitionEnd(h)
                        }
                    }
                },
                t.setTransitioning = function(d) {
                    this._isTransitioning = d
                }, t.dispose = function() {
                    k.removeData(this._element, "bs.collapse");
                    this._isTransitioning = this._triggerArray = this._element = this._parent = this._config = null
                }, t._getConfig = function(d) {
                    return (d = Y({}, xa, {}, d)).toggle = !!d.toggle, z.typeCheckConfig("collapse", d, Db), d
                }, t._getDimension = function() {
                    return k(this._element).hasClass("width") ? "width" : "height"
                }, t._getParent = function() {
                    var d, h = this;
                    z.isElement(this._config.parent) ? (d = this._config.parent, "undefined" !=
                        typeof this._config.parent.jquery && (d = this._config.parent[0])) : d = document.querySelector(this._config.parent);
                    var p = [].slice.call(d.querySelectorAll('[data-toggle="collapse"][data-parent="' + this._config.parent + '"]'));
                    return k(p).each(function(D, G) {
                        h._addAriaAndCollapsedClass(w._getTargetFromElement(G), [G])
                    }), d
                }, t._addAriaAndCollapsedClass = function(d, h) {
                    d = k(d).hasClass("show");
                    h.length && k(h).toggleClass("collapsed", !d).attr("aria-expanded", d)
                }, w._getTargetFromElement = function(d) {
                    return (d = z.getSelectorFromElement(d)) ?
                        document.querySelector(d) : null
                }, w._jQueryInterface = function(d) {
                    return this.each(function() {
                        var h = k(this),
                            p = h.data("bs.collapse"),
                            D = Y({}, xa, {}, h.data(), {}, "object" == typeof d && d ? d : {});
                        if (!p && D.toggle && /show|hide/.test(d) && (D.toggle = !1), p || (p = new w(this, D), h.data("bs.collapse", p)), "string" == typeof d) {
                            if ("undefined" == typeof p[d]) throw new TypeError('No method named "' + d + '"');
                            p[d]()
                        }
                    })
                }, ta(w, null, [{
                    key: "VERSION",
                    get: function() {
                        return "4.4.1"
                    }
                }, {
                    key: "Default",
                    get: function() {
                        return xa
                    }
                }]), w
        }();
    k(document).on("click.bs.collapse.data-api",
        '[data-toggle="collapse"]',
        function(w) {
            "A" === w.currentTarget.tagName && w.preventDefault();
            var t = k(this);
            w = z.getSelectorFromElement(this);
            w = [].slice.call(document.querySelectorAll(w));
            k(w).each(function() {
                var d = k(this),
                    h = d.data("bs.collapse") ? "toggle" : t.data();
                mb._jQueryInterface.call(d, h)
            })
        });
    k.fn.collapse = mb._jQueryInterface;
    k.fn.collapse.Constructor = mb;
    k.fn.collapse.noConflict = function() {
        return k.fn.collapse = lb, mb._jQueryInterface
    };
    var zb = k.fn.dropdown,
        nb = RegExp("38|40|27"),
        Qb = {
            offset: 0,
            flip: !0,
            boundary: "scrollParent",
            reference: "toggle",
            display: "dynamic",
            popperConfig: null
        },
        Eb = {
            offset: "(number|string|function)",
            flip: "boolean",
            boundary: "(string|element)",
            reference: "(string|element)",
            display: "string",
            popperConfig: "(null|object)"
        },
        Va = function() {
            function w(d, h) {
                this._element = d;
                this._popper = null;
                this._config = this._getConfig(h);
                this._menu = this._getMenuElement();
                this._inNavbar = this._detectNavbar();
                this._addEventListeners()
            }
            var t = w.prototype;
            return t.toggle = function() {
                if (!this._element.disabled &&
                    !k(this._element).hasClass("disabled")) {
                    var d = k(this._menu).hasClass("show");
                    w._clearMenus();
                    d || this.show(!0)
                }
            }, t.show = function(d) {
                if (void 0 === d && (d = !1), !(this._element.disabled || k(this._element).hasClass("disabled") || k(this._menu).hasClass("show"))) {
                    var h = {
                            relatedTarget: this._element
                        },
                        p = k.Event("show.bs.dropdown", h),
                        D = w._getParentFromElement(this._element);
                    if (k(D).trigger(p), !p.isDefaultPrevented()) {
                        if (!this._inNavbar && d) {
                            if ("undefined" == typeof oa) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
                            d = this._element;
                            "parent" === this._config.reference ? d = D : z.isElement(this._config.reference) && (d = this._config.reference, "undefined" != typeof this._config.reference.jquery && (d = this._config.reference[0]));
                            "scrollParent" !== this._config.boundary && k(D).addClass("position-static");
                            this._popper = new oa(d, this._menu, this._getPopperConfig())
                        }
                        "ontouchstart" in document.documentElement && 0 === k(D).closest(".navbar-nav").length && k(document.body).children().on("mouseover", null, k.noop);
                        this._element.focus();
                        this._element.setAttribute("aria-expanded", !0);
                        k(this._menu).toggleClass("show");
                        k(D).toggleClass("show").trigger(k.Event("shown.bs.dropdown", h))
                    }
                }
            }, t.hide = function() {
                if (!this._element.disabled && !k(this._element).hasClass("disabled") && k(this._menu).hasClass("show")) {
                    var d = {
                            relatedTarget: this._element
                        },
                        h = k.Event("hide.bs.dropdown", d),
                        p = w._getParentFromElement(this._element);
                    k(p).trigger(h);
                    h.isDefaultPrevented() || (this._popper && this._popper.destroy(), k(this._menu).toggleClass("show"), k(p).toggleClass("show").trigger(k.Event("hidden.bs.dropdown",
                        d)))
                }
            }, t.dispose = function() {
                k.removeData(this._element, "bs.dropdown");
                k(this._element).off(".bs.dropdown");
                this._element = null;
                (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null)
            }, t.update = function() {
                this._inNavbar = this._detectNavbar();
                null !== this._popper && this._popper.scheduleUpdate()
            }, t._addEventListeners = function() {
                var d = this;
                k(this._element).on("click.bs.dropdown", function(h) {
                    h.preventDefault();
                    h.stopPropagation();
                    d.toggle()
                })
            }, t._getConfig = function(d) {
                return d = Y({}, this.constructor.Default, {}, k(this._element).data(), {}, d), z.typeCheckConfig("dropdown", d, this.constructor.DefaultType), d
            }, t._getMenuElement = function() {
                if (!this._menu) {
                    var d = w._getParentFromElement(this._element);
                    d && (this._menu = d.querySelector(".dropdown-menu"))
                }
                return this._menu
            }, t._getPlacement = function() {
                var d = k(this._element.parentNode),
                    h = "bottom-start";
                return d.hasClass("dropup") ? (h = "top-start", k(this._menu).hasClass("dropdown-menu-right") && (h = "top-end")) : d.hasClass("dropright") ? h = "right-start" : d.hasClass("dropleft") ?
                    h = "left-start" : k(this._menu).hasClass("dropdown-menu-right") && (h = "bottom-end"), h
            }, t._detectNavbar = function() {
                return 0 < k(this._element).closest(".navbar").length
            }, t._getOffset = function() {
                var d = this,
                    h = {};
                return "function" == typeof this._config.offset ? h.fn = function(p) {
                    return p.offsets = Y({}, p.offsets, {}, d._config.offset(p.offsets, d._element) || {}), p
                } : h.offset = this._config.offset, h
            }, t._getPopperConfig = function() {
                var d = {
                    placement: this._getPlacement(),
                    modifiers: {
                        offset: this._getOffset(),
                        flip: {
                            enabled: this._config.flip
                        },
                        preventOverflow: {
                            boundariesElement: this._config.boundary
                        }
                    }
                };
                return "static" === this._config.display && (d.modifiers.applyStyle = {
                    enabled: !1
                }), Y({}, d, {}, this._config.popperConfig)
            }, w._jQueryInterface = function(d) {
                return this.each(function() {
                    var h = k(this).data("bs.dropdown");
                    if (h || (h = new w(this, "object" == typeof d ? d : null), k(this).data("bs.dropdown", h)), "string" == typeof d) {
                        if ("undefined" == typeof h[d]) throw new TypeError('No method named "' + d + '"');
                        h[d]()
                    }
                })
            }, w._clearMenus = function(d) {
                if (!d || 3 !== d.which && ("keyup" !==
                        d.type || 9 === d.which))
                    for (var h = [].slice.call(document.querySelectorAll('[data-toggle="dropdown"]')), p = 0, D = h.length; p < D; p++) {
                        var G = w._getParentFromElement(h[p]),
                            H = k(h[p]).data("bs.dropdown"),
                            ia = {
                                relatedTarget: h[p]
                            };
                        if (d && "click" === d.type && (ia.clickEvent = d), H) {
                            var pa = H._menu;
                            if (k(G).hasClass("show") && !(d && ("click" === d.type && /input|textarea/i.test(d.target.tagName) || "keyup" === d.type && 9 === d.which) && k.contains(G, d.target))) {
                                var e = k.Event("hide.bs.dropdown", ia);
                                k(G).trigger(e);
                                e.isDefaultPrevented() || ("ontouchstart" in
                                    document.documentElement && k(document.body).children().off("mouseover", null, k.noop), h[p].setAttribute("aria-expanded", "false"), H._popper && H._popper.destroy(), k(pa).removeClass("show"), k(G).removeClass("show").trigger(k.Event("hidden.bs.dropdown", ia)))
                            }
                        }
                    }
            }, w._getParentFromElement = function(d) {
                var h, p = z.getSelectorFromElement(d);
                return p && (h = document.querySelector(p)), h || d.parentNode
            }, w._dataApiKeydownHandler = function(d) {
                if (!((/input|textarea/i.test(d.target.tagName) ? 32 === d.which || 27 !== d.which && (40 !==
                        d.which && 38 !== d.which || k(d.target).closest(".dropdown-menu").length) : !nb.test(d.which)) || (d.preventDefault(), d.stopPropagation(), this.disabled || k(this).hasClass("disabled")))) {
                    var h = w._getParentFromElement(this),
                        p = k(h).hasClass("show");
                    if (p || 27 !== d.which) p && (!p || 27 !== d.which && 32 !== d.which) ? (h = [].slice.call(h.querySelectorAll(".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)")).filter(function(D) {
                        return k(D).is(":visible")
                    }), 0 !== h.length && (p = h.indexOf(d.target), 38 === d.which && 0 < p && p--, 40 ===
                        d.which && p < h.length - 1 && p++, 0 > p && (p = 0), h[p].focus())) : (27 === d.which && (d = h.querySelector('[data-toggle="dropdown"]'), k(d).trigger("focus")), k(this).trigger("click"))
                }
            }, ta(w, null, [{
                key: "VERSION",
                get: function() {
                    return "4.4.1"
                }
            }, {
                key: "Default",
                get: function() {
                    return Qb
                }
            }, {
                key: "DefaultType",
                get: function() {
                    return Eb
                }
            }]), w
        }();
    k(document).on("keydown.bs.dropdown.data-api", '[data-toggle="dropdown"]', Va._dataApiKeydownHandler).on("keydown.bs.dropdown.data-api", ".dropdown-menu", Va._dataApiKeydownHandler).on("click.bs.dropdown.data-api keyup.bs.dropdown.data-api",
        Va._clearMenus).on("click.bs.dropdown.data-api", '[data-toggle="dropdown"]', function(w) {
        w.preventDefault();
        w.stopPropagation();
        Va._jQueryInterface.call(k(this), "toggle")
    }).on("click.bs.dropdown.data-api", ".dropdown form", function(w) {
        w.stopPropagation()
    });
    k.fn.dropdown = Va._jQueryInterface;
    k.fn.dropdown.Constructor = Va;
    k.fn.dropdown.noConflict = function() {
        return k.fn.dropdown = zb, Va._jQueryInterface
    };
    var ic = k.fn.modal,
        Fb = {
            backdrop: !0,
            keyboard: !0,
            focus: !0,
            show: !0
        },
        Rb = {
            backdrop: "(boolean|string)",
            keyboard: "boolean",
            focus: "boolean",
            show: "boolean"
        },
        Ta = function() {
            function w(d, h) {
                this._config = this._getConfig(h);
                this._element = d;
                this._dialog = d.querySelector(".modal-dialog");
                this._backdrop = null;
                this._isTransitioning = this._ignoreBackdropClick = this._isBodyOverflowing = this._isShown = !1;
                this._scrollbarWidth = 0
            }
            var t = w.prototype;
            return t.toggle = function(d) {
                    return this._isShown ? this.hide() : this.show(d)
                }, t.show = function(d) {
                    var h = this;
                    if (!this._isShown && !this._isTransitioning) {
                        k(this._element).hasClass("fade") && (this._isTransitioning = !0);
                        var p = k.Event("show.bs.modal", {
                            relatedTarget: d
                        });
                        k(this._element).trigger(p);
                        this._isShown || p.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), k(this._element).on("click.dismiss.bs.modal", '[data-dismiss="modal"]', function(D) {
                            return h.hide(D)
                        }), k(this._dialog).on("mousedown.dismiss.bs.modal", function() {
                            k(h._element).one("mouseup.dismiss.bs.modal", function(D) {
                                k(D.target).is(h._element) && (h._ignoreBackdropClick = !0)
                            })
                        }), this._showBackdrop(function() {
                            return h._showElement(d)
                        }))
                    }
                }, t.hide = function(d) {
                    var h = this;
                    if (d && d.preventDefault(), this._isShown && !this._isTransitioning)
                        if (d = k.Event("hide.bs.modal"), k(this._element).trigger(d), this._isShown && !d.isDefaultPrevented()) this._isShown = !1, d = k(this._element).hasClass("fade"), (d && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), k(document).off("focusin.bs.modal"), k(this._element).removeClass("show"), k(this._element).off("click.dismiss.bs.modal"),
                            k(this._dialog).off("mousedown.dismiss.bs.modal"), d) ? (d = z.getTransitionDurationFromElement(this._element), k(this._element).one(z.TRANSITION_END, function(p) {
                            return h._hideModal(p)
                        }).emulateTransitionEnd(d)) : this._hideModal()
                }, t.dispose = function() {
                    [window, this._element, this._dialog].forEach(function(d) {
                        return k(d).off(".bs.modal")
                    });
                    k(document).off("focusin.bs.modal");
                    k.removeData(this._element, "bs.modal");
                    this._scrollbarWidth = this._isTransitioning = this._ignoreBackdropClick = this._isBodyOverflowing =
                        this._isShown = this._backdrop = this._dialog = this._element = this._config = null
                }, t.handleUpdate = function() {
                    this._adjustDialog()
                }, t._getConfig = function(d) {
                    return d = Y({}, Fb, {}, d), z.typeCheckConfig("modal", d, Rb), d
                }, t._triggerBackdropTransition = function() {
                    var d = this;
                    if ("static" === this._config.backdrop) {
                        var h = k.Event("hidePrevented.bs.modal");
                        (k(this._element).trigger(h), h.defaultPrevented) || (this._element.classList.add("modal-static"), h = z.getTransitionDurationFromElement(this._element), k(this._element).one(z.TRANSITION_END,
                            function() {
                                d._element.classList.remove("modal-static")
                            }).emulateTransitionEnd(h), this._element.focus())
                    } else this.hide()
                }, t._showElement = function(d) {
                    function h() {
                        p._config.focus && p._element.focus();
                        p._isTransitioning = !1;
                        k(p._element).trigger(H)
                    }
                    var p = this,
                        D = k(this._element).hasClass("fade"),
                        G = this._dialog ? this._dialog.querySelector(".modal-body") : null;
                    this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element);
                    this._element.style.display =
                        "block";
                    this._element.removeAttribute("aria-hidden");
                    this._element.setAttribute("aria-modal", !0);
                    k(this._dialog).hasClass("modal-dialog-scrollable") && G ? G.scrollTop = 0 : this._element.scrollTop = 0;
                    D && z.reflow(this._element);
                    k(this._element).addClass("show");
                    this._config.focus && this._enforceFocus();
                    var H = k.Event("shown.bs.modal", {
                        relatedTarget: d
                    });
                    D ? (d = z.getTransitionDurationFromElement(this._dialog), k(this._dialog).one(z.TRANSITION_END, h).emulateTransitionEnd(d)) : h()
                }, t._enforceFocus = function() {
                    var d =
                        this;
                    k(document).off("focusin.bs.modal").on("focusin.bs.modal", function(h) {
                        document !== h.target && d._element !== h.target && 0 === k(d._element).has(h.target).length && d._element.focus()
                    })
                }, t._setEscapeEvent = function() {
                    var d = this;
                    this._isShown && this._config.keyboard ? k(this._element).on("keydown.dismiss.bs.modal", function(h) {
                        27 === h.which && d._triggerBackdropTransition()
                    }) : this._isShown || k(this._element).off("keydown.dismiss.bs.modal")
                }, t._setResizeEvent = function() {
                    var d = this;
                    this._isShown ? k(window).on("resize.bs.modal",
                        function(h) {
                            return d.handleUpdate(h)
                        }) : k(window).off("resize.bs.modal")
                }, t._hideModal = function() {
                    var d = this;
                    this._element.style.display = "none";
                    this._element.setAttribute("aria-hidden", !0);
                    this._element.removeAttribute("aria-modal");
                    this._isTransitioning = !1;
                    this._showBackdrop(function() {
                        k(document.body).removeClass("modal-open");
                        d._resetAdjustments();
                        d._resetScrollbar();
                        k(d._element).trigger("hidden.bs.modal")
                    })
                }, t._removeBackdrop = function() {
                    this._backdrop && (k(this._backdrop).remove(), this._backdrop =
                        null)
                }, t._showBackdrop = function(d) {
                    var h = this,
                        p = k(this._element).hasClass("fade") ? "fade" : "";
                    if (this._isShown && this._config.backdrop) {
                        if (this._backdrop = document.createElement("div"), this._backdrop.className = "modal-backdrop", p && this._backdrop.classList.add(p), k(this._backdrop).appendTo(document.body), k(this._element).on("click.dismiss.bs.modal", function(G) {
                                h._ignoreBackdropClick ? h._ignoreBackdropClick = !1 : G.target === G.currentTarget && h._triggerBackdropTransition()
                            }), p && z.reflow(this._backdrop), k(this._backdrop).addClass("show"),
                            d) {
                            if (!p) return void d();
                            p = z.getTransitionDurationFromElement(this._backdrop);
                            k(this._backdrop).one(z.TRANSITION_END, d).emulateTransitionEnd(p)
                        }
                    } else if (!this._isShown && this._backdrop)
                        if (k(this._backdrop).removeClass("show"), p = function() {
                                h._removeBackdrop();
                                d && d()
                            }, k(this._element).hasClass("fade")) {
                            var D = z.getTransitionDurationFromElement(this._backdrop);
                            k(this._backdrop).one(z.TRANSITION_END, p).emulateTransitionEnd(D)
                        } else p();
                    else d && d()
                }, t._adjustDialog = function() {
                    var d = this._element.scrollHeight >
                        document.documentElement.clientHeight;
                    !this._isBodyOverflowing && d && (this._element.style.paddingLeft = this._scrollbarWidth + "px");
                    this._isBodyOverflowing && !d && (this._element.style.paddingRight = this._scrollbarWidth + "px")
                }, t._resetAdjustments = function() {
                    this._element.style.paddingLeft = "";
                    this._element.style.paddingRight = ""
                }, t._checkScrollbar = function() {
                    var d = document.body.getBoundingClientRect();
                    this._isBodyOverflowing = d.left + d.right < window.innerWidth;
                    this._scrollbarWidth = this._getScrollbarWidth()
                }, t._setScrollbar =
                function() {
                    var d = this;
                    if (this._isBodyOverflowing) {
                        var h = [].slice.call(document.querySelectorAll(".fixed-top, .fixed-bottom, .is-fixed, .sticky-top")),
                            p = [].slice.call(document.querySelectorAll(".sticky-top"));
                        k(h).each(function(D, G) {
                            D = G.style.paddingRight;
                            var H = k(G).css("padding-right");
                            k(G).data("padding-right", D).css("padding-right", parseFloat(H) + d._scrollbarWidth + "px")
                        });
                        k(p).each(function(D, G) {
                            D = G.style.marginRight;
                            var H = k(G).css("margin-right");
                            k(G).data("margin-right", D).css("margin-right", parseFloat(H) -
                                d._scrollbarWidth + "px")
                        });
                        h = document.body.style.paddingRight;
                        p = k(document.body).css("padding-right");
                        k(document.body).data("padding-right", h).css("padding-right", parseFloat(p) + this._scrollbarWidth + "px")
                    }
                    k(document.body).addClass("modal-open")
                }, t._resetScrollbar = function() {
                    var d = [].slice.call(document.querySelectorAll(".fixed-top, .fixed-bottom, .is-fixed, .sticky-top"));
                    k(d).each(function(h, p) {
                        h = k(p).data("padding-right");
                        k(p).removeData("padding-right");
                        p.style.paddingRight = h || ""
                    });
                    d = [].slice.call(document.querySelectorAll(".sticky-top"));
                    k(d).each(function(h, p) {
                        h = k(p).data("margin-right");
                        "undefined" != typeof h && k(p).css("margin-right", h).removeData("margin-right")
                    });
                    d = k(document.body).data("padding-right");
                    k(document.body).removeData("padding-right");
                    document.body.style.paddingRight = d || ""
                }, t._getScrollbarWidth = function() {
                    var d = document.createElement("div");
                    d.className = "modal-scrollbar-measure";
                    document.body.appendChild(d);
                    var h = d.getBoundingClientRect().width - d.clientWidth;
                    return document.body.removeChild(d), h
                }, w._jQueryInterface =
                function(d, h) {
                    return this.each(function() {
                        var p = k(this).data("bs.modal"),
                            D = Y({}, Fb, {}, k(this).data(), {}, "object" == typeof d && d ? d : {});
                        if (p || (p = new w(this, D), k(this).data("bs.modal", p)), "string" == typeof d) {
                            if ("undefined" == typeof p[d]) throw new TypeError('No method named "' + d + '"');
                            p[d](h)
                        } else D.show && p.show(h)
                    })
                }, ta(w, null, [{
                    key: "VERSION",
                    get: function() {
                        return "4.4.1"
                    }
                }, {
                    key: "Default",
                    get: function() {
                        return Fb
                    }
                }]), w
        }();
    k(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(w) {
        var t,
            d = this,
            h = z.getSelectorFromElement(this);
        h && (t = document.querySelector(h));
        h = k(t).data("bs.modal") ? "toggle" : Y({}, k(t).data(), {}, k(this).data());
        "A" !== this.tagName && "AREA" !== this.tagName || w.preventDefault();
        var p = k(t).one("show.bs.modal", function(D) {
            D.isDefaultPrevented() || p.one("hidden.bs.modal", function() {
                k(d).is(":visible") && d.focus()
            })
        });
        Ta._jQueryInterface.call(k(t), h, this)
    });
    k.fn.modal = Ta._jQueryInterface;
    k.fn.modal.Constructor = Ta;
    k.fn.modal.noConflict = function() {
        return k.fn.modal = ic, Ta._jQueryInterface
    };
    var Sb = "background cite href itemtype longdesc poster src xlink:href".split(" "),
        ub = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
        Ub = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i,
        Vb = k.fn.tooltip,
        Wb = RegExp("(^|\\s)bs-tooltip\\S+", "g"),
        Hb = ["sanitize", "whiteList", "sanitizeFn"],
        Xb = {
            animation: "boolean",
            template: "string",
            title: "(string|element|function)",
            trigger: "string",
            delay: "(number|object)",
            html: "boolean",
            selector: "(string|boolean)",
            placement: "(string|function)",
            offset: "(number|string|function)",
            container: "(string|element|boolean)",
            fallbackPlacement: "(string|array)",
            boundary: "(string|element)",
            sanitize: "boolean",
            sanitizeFn: "(null|function)",
            whiteList: "object",
            popperConfig: "(null|object)"
        },
        Fa = {
            AUTO: "auto",
            TOP: "top",
            RIGHT: "right",
            BOTTOM: "bottom",
            LEFT: "left"
        },
        Ib = {
            animation: !0,
            template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: "hover focus",
            title: "",
            delay: 0,
            html: !1,
            selector: !1,
            placement: "top",
            offset: 0,
            container: !1,
            fallbackPlacement: "flip",
            boundary: "scrollParent",
            sanitize: !0,
            sanitizeFn: null,
            whiteList: {
                "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
                a: ["target", "href", "title", "rel"],
                area: [],
                b: [],
                br: [],
                col: [],
                code: [],
                div: [],
                em: [],
                hr: [],
                h1: [],
                h2: [],
                h3: [],
                h4: [],
                h5: [],
                h6: [],
                i: [],
                img: ["src", "alt", "title", "width", "height"],
                li: [],
                ol: [],
                p: [],
                pre: [],
                s: [],
                small: [],
                span: [],
                sub: [],
                sup: [],
                strong: [],
                u: [],
                ul: []
            },
            popperConfig: null
        },
        Zb = {
            HIDE: "hide.bs.tooltip",
            HIDDEN: "hidden.bs.tooltip",
            SHOW: "show.bs.tooltip",
            SHOWN: "shown.bs.tooltip",
            INSERTED: "inserted.bs.tooltip",
            CLICK: "click.bs.tooltip",
            FOCUSIN: "focusin.bs.tooltip",
            FOCUSOUT: "focusout.bs.tooltip",
            MOUSEENTER: "mouseenter.bs.tooltip",
            MOUSELEAVE: "mouseleave.bs.tooltip"
        },
        Ua = function() {
            function w(d, h) {
                if ("undefined" == typeof oa) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
                this._isEnabled = !0;
                this._timeout = 0;
                this._hoverState = "";
                this._activeTrigger = {};
                this._popper = null;
                this.element = d;
                this.config = this._getConfig(h);
                this.tip = null;
                this._setListeners()
            }
            var t = w.prototype;
            return t.enable = function() {
                    this._isEnabled = !0
                }, t.disable = function() {
                    this._isEnabled = !1
                }, t.toggleEnabled = function() {
                    this._isEnabled = !this._isEnabled
                }, t.toggle = function(d) {
                    if (this._isEnabled)
                        if (d) {
                            var h = this.constructor.DATA_KEY,
                                p = k(d.currentTarget).data(h);
                            p || (p = new this.constructor(d.currentTarget, this._getDelegateConfig()), k(d.currentTarget).data(h, p));
                            p._activeTrigger.click = !p._activeTrigger.click;
                            p._isWithActiveTrigger() ? p._enter(null, p) : p._leave(null, p)
                        } else {
                            if (k(this.getTipElement()).hasClass("show")) return void this._leave(null, this);
                            this._enter(null, this)
                        }
                }, t.dispose = function() {
                    clearTimeout(this._timeout);
                    k.removeData(this.element, this.constructor.DATA_KEY);
                    k(this.element).off(this.constructor.EVENT_KEY);
                    k(this.element).closest(".modal").off("hide.bs.modal", this._hideModalHandler);
                    this.tip && k(this.tip).remove();
                    this._activeTrigger = this._hoverState = this._timeout = this._isEnabled = null;
                    this._popper &&
                        this._popper.destroy();
                    this.tip = this.config = this.element = this._popper = null
                }, t.show = function() {
                    var d = this;
                    if ("none" === k(this.element).css("display")) throw Error("Please use show on visible elements");
                    var h = k.Event(this.constructor.Event.SHOW);
                    if (this.isWithContent() && this._isEnabled) {
                        k(this.element).trigger(h);
                        var p = z.findShadowRoot(this.element);
                        p = k.contains(null !== p ? p : this.element.ownerDocument.documentElement, this.element);
                        if (!h.isDefaultPrevented() && p) {
                            h = this.getTipElement();
                            p = z.getUID(this.constructor.NAME);
                            h.setAttribute("id", p);
                            this.element.setAttribute("aria-describedby", p);
                            this.setContent();
                            this.config.animation && k(h).addClass("fade");
                            p = "function" == typeof this.config.placement ? this.config.placement.call(this, h, this.element) : this.config.placement;
                            p = this._getAttachment(p);
                            this.addAttachmentClass(p);
                            var D = this._getContainer();
                            k(h).data(this.constructor.DATA_KEY, this);
                            k.contains(this.element.ownerDocument.documentElement, this.tip) || k(h).appendTo(D);
                            k(this.element).trigger(this.constructor.Event.INSERTED);
                            this._popper = new oa(this.element, h, this._getPopperConfig(p));
                            k(h).addClass("show");
                            "ontouchstart" in document.documentElement && k(document.body).children().on("mouseover", null, k.noop);
                            h = function() {
                                d.config.animation && d._fixTransition();
                                var G = d._hoverState;
                                d._hoverState = null;
                                k(d.element).trigger(d.constructor.Event.SHOWN);
                                "out" === G && d._leave(null, d)
                            };
                            k(this.tip).hasClass("fade") ? (p = z.getTransitionDurationFromElement(this.tip), k(this.tip).one(z.TRANSITION_END, h).emulateTransitionEnd(p)) : h()
                        }
                    }
                }, t.hide =
                function(d) {
                    function h() {
                        "show" !== p._hoverState && D.parentNode && D.parentNode.removeChild(D);
                        p._cleanTipClass();
                        p.element.removeAttribute("aria-describedby");
                        k(p.element).trigger(p.constructor.Event.HIDDEN);
                        null !== p._popper && p._popper.destroy();
                        d && d()
                    }
                    var p = this,
                        D = this.getTipElement(),
                        G = k.Event(this.constructor.Event.HIDE);
                    (k(this.element).trigger(G), G.isDefaultPrevented()) || ((k(D).removeClass("show"), "ontouchstart" in document.documentElement && k(document.body).children().off("mouseover", null, k.noop),
                        this._activeTrigger.click = !1, this._activeTrigger.focus = !1, this._activeTrigger.hover = !1, k(this.tip).hasClass("fade")) ? (G = z.getTransitionDurationFromElement(D), k(D).one(z.TRANSITION_END, h).emulateTransitionEnd(G)) : h(), this._hoverState = "")
                }, t.update = function() {
                    null !== this._popper && this._popper.scheduleUpdate()
                }, t.isWithContent = function() {
                    return !!this.getTitle()
                }, t.addAttachmentClass = function(d) {
                    k(this.getTipElement()).addClass("bs-tooltip-" + d)
                }, t.getTipElement = function() {
                    return this.tip = this.tip || k(this.config.template)[0],
                        this.tip
                }, t.setContent = function() {
                    var d = this.getTipElement();
                    this.setElementContent(k(d.querySelectorAll(".tooltip-inner")), this.getTitle());
                    k(d).removeClass("fade show")
                }, t.setElementContent = function(d, h) {
                    "object" != typeof h || !h.nodeType && !h.jquery ? this.config.html ? (this.config.sanitize && (h = tb(h, this.config.whiteList, this.config.sanitizeFn)), d.html(h)) : d.text(h) : this.config.html ? k(h).parent().is(d) || d.empty().append(h) : d.text(k(h).text())
                }, t.getTitle = function() {
                    return this.element.getAttribute("data-original-title") ||
                        ("function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title)
                }, t._getPopperConfig = function(d) {
                    var h = this;
                    return Y({}, {
                        placement: d,
                        modifiers: {
                            offset: this._getOffset(),
                            flip: {
                                behavior: this.config.fallbackPlacement
                            },
                            arrow: {
                                element: ".arrow"
                            },
                            preventOverflow: {
                                boundariesElement: this.config.boundary
                            }
                        },
                        onCreate: function(p) {
                            p.originalPlacement !== p.placement && h._handlePopperPlacementChange(p)
                        },
                        onUpdate: function(p) {
                            return h._handlePopperPlacementChange(p)
                        }
                    }, {}, this.config.popperConfig)
                },
                t._getOffset = function() {
                    var d = this,
                        h = {};
                    return "function" == typeof this.config.offset ? h.fn = function(p) {
                        return p.offsets = Y({}, p.offsets, {}, d.config.offset(p.offsets, d.element) || {}), p
                    } : h.offset = this.config.offset, h
                }, t._getContainer = function() {
                    return !1 === this.config.container ? document.body : z.isElement(this.config.container) ? k(this.config.container) : k(document).find(this.config.container)
                }, t._getAttachment = function(d) {
                    return Fa[d.toUpperCase()]
                }, t._setListeners = function() {
                    var d = this;
                    this.config.trigger.split(" ").forEach(function(h) {
                        if ("click" ===
                            h) k(d.element).on(d.constructor.Event.CLICK, d.config.selector, function(D) {
                            return d.toggle(D)
                        });
                        else if ("manual" !== h) {
                            var p = "hover" === h ? d.constructor.Event.MOUSEENTER : d.constructor.Event.FOCUSIN;
                            h = "hover" === h ? d.constructor.Event.MOUSELEAVE : d.constructor.Event.FOCUSOUT;
                            k(d.element).on(p, d.config.selector, function(D) {
                                return d._enter(D)
                            }).on(h, d.config.selector, function(D) {
                                return d._leave(D)
                            })
                        }
                    });
                    this._hideModalHandler = function() {
                        d.element && d.hide()
                    };
                    k(this.element).closest(".modal").on("hide.bs.modal",
                        this._hideModalHandler);
                    this.config.selector ? this.config = Y({}, this.config, {
                        trigger: "manual",
                        selector: ""
                    }) : this._fixTitle()
                }, t._fixTitle = function() {
                    var d = typeof this.element.getAttribute("data-original-title");
                    !this.element.getAttribute("title") && "string" == d || (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
                }, t._enter = function(d, h) {
                    var p = this.constructor.DATA_KEY;
                    (h = h || k(d.currentTarget).data(p)) || (h = new this.constructor(d.currentTarget,
                        this._getDelegateConfig()), k(d.currentTarget).data(p, h));
                    d && (h._activeTrigger["focusin" === d.type ? "focus" : "hover"] = !0);
                    k(h.getTipElement()).hasClass("show") || "show" === h._hoverState ? h._hoverState = "show" : (clearTimeout(h._timeout), h._hoverState = "show", h.config.delay && h.config.delay.show ? h._timeout = setTimeout(function() {
                        "show" === h._hoverState && h.show()
                    }, h.config.delay.show) : h.show())
                }, t._leave = function(d, h) {
                    var p = this.constructor.DATA_KEY;
                    (h = h || k(d.currentTarget).data(p)) || (h = new this.constructor(d.currentTarget,
                        this._getDelegateConfig()), k(d.currentTarget).data(p, h));
                    d && (h._activeTrigger["focusout" === d.type ? "focus" : "hover"] = !1);
                    h._isWithActiveTrigger() || (clearTimeout(h._timeout), h._hoverState = "out", h.config.delay && h.config.delay.hide ? h._timeout = setTimeout(function() {
                        "out" === h._hoverState && h.hide()
                    }, h.config.delay.hide) : h.hide())
                }, t._isWithActiveTrigger = function() {
                    for (var d in this._activeTrigger)
                        if (this._activeTrigger[d]) return !0;
                    return !1
                }, t._getConfig = function(d) {
                    var h = k(this.element).data();
                    return Object.keys(h).forEach(function(p) {
                        -1 !==
                            Hb.indexOf(p) && delete h[p]
                    }), "number" == typeof(d = Y({}, this.constructor.Default, {}, h, {}, "object" == typeof d && d ? d : {})).delay && (d.delay = {
                        show: d.delay,
                        hide: d.delay
                    }), "number" == typeof d.title && (d.title = d.title.toString()), "number" == typeof d.content && (d.content = d.content.toString()), z.typeCheckConfig("tooltip", d, this.constructor.DefaultType), d.sanitize && (d.template = tb(d.template, d.whiteList, d.sanitizeFn)), d
                }, t._getDelegateConfig = function() {
                    var d = {};
                    if (this.config)
                        for (var h in this.config) this.constructor.Default[h] !==
                            this.config[h] && (d[h] = this.config[h]);
                    return d
                }, t._cleanTipClass = function() {
                    var d = k(this.getTipElement()),
                        h = d.attr("class").match(Wb);
                    null !== h && h.length && d.removeClass(h.join(""))
                }, t._handlePopperPlacementChange = function(d) {
                    this.tip = d.instance.popper;
                    this._cleanTipClass();
                    this.addAttachmentClass(this._getAttachment(d.placement))
                }, t._fixTransition = function() {
                    var d = this.getTipElement(),
                        h = this.config.animation;
                    null === d.getAttribute("x-placement") && (k(d).removeClass("fade"), this.config.animation = !1, this.hide(),
                        this.show(), this.config.animation = h)
                }, w._jQueryInterface = function(d) {
                    return this.each(function() {
                        var h = k(this).data("bs.tooltip"),
                            p = "object" == typeof d && d;
                        if ((h || !/dispose|hide/.test(d)) && (h || (h = new w(this, p), k(this).data("bs.tooltip", h)), "string" == typeof d)) {
                            if ("undefined" == typeof h[d]) throw new TypeError('No method named "' + d + '"');
                            h[d]()
                        }
                    })
                }, ta(w, null, [{
                    key: "VERSION",
                    get: function() {
                        return "4.4.1"
                    }
                }, {
                    key: "Default",
                    get: function() {
                        return Ib
                    }
                }, {
                    key: "NAME",
                    get: function() {
                        return "tooltip"
                    }
                }, {
                    key: "DATA_KEY",
                    get: function() {
                        return "bs.tooltip"
                    }
                }, {
                    key: "Event",
                    get: function() {
                        return Zb
                    }
                }, {
                    key: "EVENT_KEY",
                    get: function() {
                        return ".bs.tooltip"
                    }
                }, {
                    key: "DefaultType",
                    get: function() {
                        return Xb
                    }
                }]), w
        }();
    k.fn.tooltip = Ua._jQueryInterface;
    k.fn.tooltip.Constructor = Ua;
    k.fn.tooltip.noConflict = function() {
        return k.fn.tooltip = Vb, Ua._jQueryInterface
    };
    var $b = k.fn.popover,
        kc = RegExp("(^|\\s)bs-popover\\S+", "g"),
        Na = Y({}, Ua.Default, {
            placement: "right",
            trigger: "click",
            content: "",
            template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        }),
        db = Y({}, Ua.DefaultType, {
            content: "(string|element|function)"
        }),
        eb = {
            HIDE: "hide.bs.popover",
            HIDDEN: "hidden.bs.popover",
            SHOW: "show.bs.popover",
            SHOWN: "shown.bs.popover",
            INSERTED: "inserted.bs.popover",
            CLICK: "click.bs.popover",
            FOCUSIN: "focusin.bs.popover",
            FOCUSOUT: "focusout.bs.popover",
            MOUSEENTER: "mouseenter.bs.popover",
            MOUSELEAVE: "mouseleave.bs.popover"
        },
        ob = function(w) {
            function t() {
                return w.apply(this, arguments) || this
            }! function(h, p) {
                h.prototype = Object.create(p.prototype);
                (h.prototype.constructor = h).__proto__ =
                    p
            }(t, w);
            var d = t.prototype;
            return d.isWithContent = function() {
                return this.getTitle() || this._getContent()
            }, d.addAttachmentClass = function(h) {
                k(this.getTipElement()).addClass("bs-popover-" + h)
            }, d.getTipElement = function() {
                return this.tip = this.tip || k(this.config.template)[0], this.tip
            }, d.setContent = function() {
                var h = k(this.getTipElement());
                this.setElementContent(h.find(".popover-header"), this.getTitle());
                var p = this._getContent();
                "function" == typeof p && (p = p.call(this.element));
                this.setElementContent(h.find(".popover-body"),
                    p);
                h.removeClass("fade show")
            }, d._getContent = function() {
                return this.element.getAttribute("data-content") || this.config.content
            }, d._cleanTipClass = function() {
                var h = k(this.getTipElement()),
                    p = h.attr("class").match(kc);
                null !== p && 0 < p.length && h.removeClass(p.join(""))
            }, t._jQueryInterface = function(h) {
                return this.each(function() {
                    var p = k(this).data("bs.popover"),
                        D = "object" == typeof h ? h : null;
                    if ((p || !/dispose|hide/.test(h)) && (p || (p = new t(this, D), k(this).data("bs.popover", p)), "string" == typeof h)) {
                        if ("undefined" ==
                            typeof p[h]) throw new TypeError('No method named "' + h + '"');
                        p[h]()
                    }
                })
            }, ta(t, null, [{
                key: "VERSION",
                get: function() {
                    return "4.4.1"
                }
            }, {
                key: "Default",
                get: function() {
                    return Na
                }
            }, {
                key: "NAME",
                get: function() {
                    return "popover"
                }
            }, {
                key: "DATA_KEY",
                get: function() {
                    return "bs.popover"
                }
            }, {
                key: "Event",
                get: function() {
                    return eb
                }
            }, {
                key: "EVENT_KEY",
                get: function() {
                    return ".bs.popover"
                }
            }, {
                key: "DefaultType",
                get: function() {
                    return db
                }
            }]), t
        }(Ua);
    k.fn.popover = ob._jQueryInterface;
    k.fn.popover.Constructor = ob;
    k.fn.popover.noConflict = function() {
        return k.fn.popover =
            $b, ob._jQueryInterface
    };
    var Jb = k.fn.scrollspy,
        Kb = {
            offset: 10,
            method: "auto",
            target: ""
        },
        ac = {
            offset: "number",
            method: "string",
            target: "(string|element)"
        },
        fb = function() {
            function w(d, h) {
                var p = this;
                this._element = d;
                this._scrollElement = "BODY" === d.tagName ? window : d;
                this._config = this._getConfig(h);
                this._selector = this._config.target + " .nav-link," + this._config.target + " .list-group-item," + this._config.target + " .dropdown-item";
                this._offsets = [];
                this._targets = [];
                this._activeTarget = null;
                this._scrollHeight = 0;
                k(this._scrollElement).on("scroll.bs.scrollspy",
                    function(D) {
                        return p._process(D)
                    });
                this.refresh();
                this._process()
            }
            var t = w.prototype;
            return t.refresh = function() {
                var d = this,
                    h = this._scrollElement === this._scrollElement.window ? "offset" : "position",
                    p = "auto" === this._config.method ? h : this._config.method,
                    D = "position" === p ? this._getScrollTop() : 0;
                this._offsets = [];
                this._targets = [];
                this._scrollHeight = this._getScrollHeight();
                [].slice.call(document.querySelectorAll(this._selector)).map(function(G) {
                    var H;
                    G = z.getSelectorFromElement(G);
                    if (G && (H = document.querySelector(G)),
                        H) {
                        var ia = H.getBoundingClientRect();
                        if (ia.width || ia.height) return [k(H)[p]().top + D, G]
                    }
                    return null
                }).filter(function(G) {
                    return G
                }).sort(function(G, H) {
                    return G[0] - H[0]
                }).forEach(function(G) {
                    d._offsets.push(G[0]);
                    d._targets.push(G[1])
                })
            }, t.dispose = function() {
                k.removeData(this._element, "bs.scrollspy");
                k(this._scrollElement).off(".bs.scrollspy");
                this._scrollHeight = this._activeTarget = this._targets = this._offsets = this._selector = this._config = this._scrollElement = this._element = null
            }, t._getConfig = function(d) {
                if ("string" !=
                    typeof(d = Y({}, Kb, {}, "object" == typeof d && d ? d : {})).target) {
                    var h = k(d.target).attr("id");
                    h || (h = z.getUID("scrollspy"), k(d.target).attr("id", h));
                    d.target = "#" + h
                }
                return z.typeCheckConfig("scrollspy", d, ac), d
            }, t._getScrollTop = function() {
                return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
            }, t._getScrollHeight = function() {
                return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
            }, t._getOffsetHeight = function() {
                return this._scrollElement ===
                    window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
            }, t._process = function() {
                var d = this._getScrollTop() + this._config.offset,
                    h = this._getScrollHeight(),
                    p = this._config.offset + h - this._getOffsetHeight();
                if (this._scrollHeight !== h && this.refresh(), p <= d) d = this._targets[this._targets.length - 1], this._activeTarget !== d && this._activate(d);
                else {
                    if (this._activeTarget && d < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();
                    for (h = this._offsets.length; h--;) this._activeTarget !==
                        this._targets[h] && d >= this._offsets[h] && ("undefined" == typeof this._offsets[h + 1] || d < this._offsets[h + 1]) && this._activate(this._targets[h])
                }
            }, t._activate = function(d) {
                this._activeTarget = d;
                this._clear();
                var h = this._selector.split(",").map(function(p) {
                    return p + '[data-target="' + d + '"],' + p + '[href="' + d + '"]'
                });
                h = k([].slice.call(document.querySelectorAll(h.join(","))));
                h.hasClass("dropdown-item") ? (h.closest(".dropdown").find(".dropdown-toggle").addClass("active"), h.addClass("active")) : (h.addClass("active"), h.parents(".nav, .list-group").prev(".nav-link, .list-group-item").addClass("active"),
                    h.parents(".nav, .list-group").prev(".nav-item").children(".nav-link").addClass("active"));
                k(this._scrollElement).trigger("activate.bs.scrollspy", {
                    relatedTarget: d
                })
            }, t._clear = function() {
                [].slice.call(document.querySelectorAll(this._selector)).filter(function(d) {
                    return d.classList.contains("active")
                }).forEach(function(d) {
                    return d.classList.remove("active")
                })
            }, w._jQueryInterface = function(d) {
                return this.each(function() {
                    var h = k(this).data("bs.scrollspy");
                    if (h || (h = new w(this, "object" == typeof d && d), k(this).data("bs.scrollspy",
                            h)), "string" == typeof d) {
                        if ("undefined" == typeof h[d]) throw new TypeError('No method named "' + d + '"');
                        h[d]()
                    }
                })
            }, ta(w, null, [{
                key: "VERSION",
                get: function() {
                    return "4.4.1"
                }
            }, {
                key: "Default",
                get: function() {
                    return Kb
                }
            }]), w
        }();
    k(window).on("load.bs.scrollspy.data-api", function() {
        for (var w = [].slice.call(document.querySelectorAll('[data-spy="scroll"]')), t = w.length; t--;) {
            var d = k(w[t]);
            fb._jQueryInterface.call(d, d.data())
        }
    });
    k.fn.scrollspy = fb._jQueryInterface;
    k.fn.scrollspy.Constructor = fb;
    k.fn.scrollspy.noConflict =
        function() {
            return k.fn.scrollspy = Jb, fb._jQueryInterface
        };
    var gb = k.fn.tab,
        X = function() {
            function w(d) {
                this._element = d
            }
            var t = w.prototype;
            return t.show = function() {
                var d = this;
                if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && k(this._element).hasClass("active") || k(this._element).hasClass("disabled"))) {
                    var h, p = k(this._element).closest(".nav, .list-group")[0],
                        D = z.getSelectorFromElement(this._element);
                    if (p) {
                        var G = "UL" === p.nodeName || "OL" === p.nodeName ? "> li > .active" : ".active";
                        var H = (H = k.makeArray(k(p).find(G)))[H.length - 1]
                    }
                    G = k.Event("hide.bs.tab", {
                        relatedTarget: this._element
                    });
                    var ia = k.Event("show.bs.tab", {
                        relatedTarget: H
                    });
                    (H && k(H).trigger(G), k(this._element).trigger(ia), ia.isDefaultPrevented() || G.isDefaultPrevented()) || (D && (h = document.querySelector(D)), this._activate(this._element, p), p = function() {
                        var pa = k.Event("hidden.bs.tab", {
                                relatedTarget: d._element
                            }),
                            e = k.Event("shown.bs.tab", {
                                relatedTarget: H
                            });
                        k(H).trigger(pa);
                        k(d._element).trigger(e)
                    }, h ? this._activate(h, h.parentNode,
                        p) : p())
                }
            }, t.dispose = function() {
                k.removeData(this._element, "bs.tab");
                this._element = null
            }, t._activate = function(d, h, p) {
                function D() {
                    return G._transitionComplete(d, H, p)
                }
                var G = this,
                    H = (!h || "UL" !== h.nodeName && "OL" !== h.nodeName ? k(h).children(".active") : k(h).find("> li > .active"))[0];
                h = p && H && k(H).hasClass("fade");
                H && h ? (h = z.getTransitionDurationFromElement(H), k(H).removeClass("show").one(z.TRANSITION_END, D).emulateTransitionEnd(h)) : D()
            }, t._transitionComplete = function(d, h, p) {
                if (h) {
                    k(h).removeClass("active");
                    var D = k(h.parentNode).find("> .dropdown-menu .active")[0];
                    D && k(D).removeClass("active");
                    "tab" === h.getAttribute("role") && h.setAttribute("aria-selected", !1)
                }
                if (k(d).addClass("active"), "tab" === d.getAttribute("role") && d.setAttribute("aria-selected", !0), z.reflow(d), d.classList.contains("fade") && d.classList.add("show"), d.parentNode && k(d.parentNode).hasClass("dropdown-menu")) {
                    if (h = k(d).closest(".dropdown")[0]) h = [].slice.call(h.querySelectorAll(".dropdown-toggle")), k(h).addClass("active");
                    d.setAttribute("aria-expanded", !0)
                }
                p && p()
            }, w._jQueryInterface = function(d) {
                return this.each(function() {
                    var h = k(this),
                        p = h.data("bs.tab");
                    if (p || (p = new w(this), h.data("bs.tab", p)), "string" == typeof d) {
                        if ("undefined" == typeof p[d]) throw new TypeError('No method named "' + d + '"');
                        p[d]()
                    }
                })
            }, ta(w, null, [{
                key: "VERSION",
                get: function() {
                    return "4.4.1"
                }
            }]), w
        }();
    k(document).on("click.bs.tab.data-api", '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', function(w) {
        w.preventDefault();
        X._jQueryInterface.call(k(this), "show")
    });
    k.fn.tab =
        X._jQueryInterface;
    k.fn.tab.Constructor = X;
    k.fn.tab.noConflict = function() {
        return k.fn.tab = gb, X._jQueryInterface
    };
    var mc = k.fn.toast,
        hb = {
            animation: "boolean",
            autohide: "boolean",
            delay: "number"
        },
        Gb = {
            animation: !0,
            autohide: !0,
            delay: 500
        },
        pb = function() {
            function w(d, h) {
                this._element = d;
                this._config = this._getConfig(h);
                this._timeout = null;
                this._setListeners()
            }
            var t = w.prototype;
            return t.show = function() {
                    var d = this,
                        h = k.Event("show.bs.toast");
                    if (k(this._element).trigger(h), !h.isDefaultPrevented())
                        if (this._config.animation &&
                            this._element.classList.add("fade"), h = function() {
                                d._element.classList.remove("showing");
                                d._element.classList.add("show");
                                k(d._element).trigger("shown.bs.toast");
                                d._config.autohide && (d._timeout = setTimeout(function() {
                                    d.hide()
                                }, d._config.delay))
                            }, this._element.classList.remove("hide"), z.reflow(this._element), this._element.classList.add("showing"), this._config.animation) {
                            var p = z.getTransitionDurationFromElement(this._element);
                            k(this._element).one(z.TRANSITION_END, h).emulateTransitionEnd(p)
                        } else h()
                },
                t.hide = function() {
                    if (this._element.classList.contains("show")) {
                        var d = k.Event("hide.bs.toast");
                        k(this._element).trigger(d);
                        d.isDefaultPrevented() || this._close()
                    }
                }, t.dispose = function() {
                    clearTimeout(this._timeout);
                    this._timeout = null;
                    this._element.classList.contains("show") && this._element.classList.remove("show");
                    k(this._element).off("click.dismiss.bs.toast");
                    k.removeData(this._element, "bs.toast");
                    this._config = this._element = null
                }, t._getConfig = function(d) {
                    return d = Y({}, Gb, {}, k(this._element).data(), {},
                        "object" == typeof d && d ? d : {}), z.typeCheckConfig("toast", d, this.constructor.DefaultType), d
                }, t._setListeners = function() {
                    var d = this;
                    k(this._element).on("click.dismiss.bs.toast", '[data-dismiss="toast"]', function() {
                        return d.hide()
                    })
                }, t._close = function() {
                    function d() {
                        h._element.classList.add("hide");
                        k(h._element).trigger("hidden.bs.toast")
                    }
                    var h = this;
                    if (this._element.classList.remove("show"), this._config.animation) {
                        var p = z.getTransitionDurationFromElement(this._element);
                        k(this._element).one(z.TRANSITION_END,
                            d).emulateTransitionEnd(p)
                    } else d()
                }, w._jQueryInterface = function(d) {
                    return this.each(function() {
                        var h = k(this),
                            p = h.data("bs.toast");
                        if (p || (p = new w(this, "object" == typeof d && d), h.data("bs.toast", p)), "string" == typeof d) {
                            if ("undefined" == typeof p[d]) throw new TypeError('No method named "' + d + '"');
                            p[d](this)
                        }
                    })
                }, ta(w, null, [{
                    key: "VERSION",
                    get: function() {
                        return "4.4.1"
                    }
                }, {
                    key: "DefaultType",
                    get: function() {
                        return hb
                    }
                }, {
                    key: "Default",
                    get: function() {
                        return Gb
                    }
                }]), w
        }();
    k.fn.toast = pb._jQueryInterface;
    k.fn.toast.Constructor =
        pb;
    k.fn.toast.noConflict = function() {
        return k.fn.toast = mc, pb._jQueryInterface
    };
    L.Alert = F;
    L.Button = N;
    L.Carousel = bb;
    L.Collapse = mb;
    L.Dropdown = Va;
    L.Modal = Ta;
    L.Popover = ob;
    L.Scrollspy = fb;
    L.Tab = X;
    L.Toast = pb;
    L.Tooltip = Ua;
    L.Util = z;
    Object.defineProperty(L, "__esModule", {
        value: !0
    })
});